$(function() {

	$("#feeTable").on("click",function(i) {
	    if (i.class == ".totalfee"){
		    displayFormTotal( $(this) );
		} else if (i.class == ".feeitem"){
		    displayFormItem( $(this) );
		}	
	});
});

function displayFormTotal( cell ) {

    var schoolperiodid = cell.attr("id"),
	    schoollevelid = $("selectedlevel").innerHTML;
		cellWidth = cell.css("width"),
		prevContent = cell.text(),
		form = '<form th:object="${schoolLevelPeriodTotalForm}" th:action="@{/addschoollevelperiodtotal}"><input type="text" size="4" th:field="*{amount}" name="amount" value="'+
			   prevContent+'" /><input type="hidden" th:field="*{schoolperiodid}" name="schoolperiodid" value="'+ schoolperiodid +'" />'+
			   '<input type="hidden" th:field="*{schoollevelid}" name="schoollevelid" value="'+schoollevelid+'"/></form>';
	
	cell.html(form).find('input[type=text]')
		.focus()
		.css('width',cellWidth);
			   
    cell.on('click', function(){return false});
	
	cell.on('keydown',function(e) {
		if (e.keyCode == 13) {
			changeFieldTotal(cell, prevContent);
		} else if (e.keyCode == 27) {
			cell.text(prevContent);
			cell.off('click');
		}
	});
}

function displayFormItem( cell ) {

    var schoolperiodid = cell.attr('id'),
	    feeitemid = cell.closest.('tr').attr('id');
	    schoollevelid = $('selectedlevel').innerHTML;
		cellWidth = cell.css('width'),
		prevContent = cell.text(),
		form = '<form th:object="${feeItemAmountForm}" th:action="@{/addschoollevelperiodtotal}"><input type="text" size="4" th:field="*{amount}" name="amount" value="'+
			   prevContent+'" /><input type="hidden" th:field="*{schoolperiodid}" name="schoolperiodid" value="'+ schoolperiodid +'" />'+
			   '<input type="hidden" th:field="*{schoollevelid}" name="schoollevelid" value="'+schoollevelid+'"/><input type="hidden"  th:field="*{feeitemid}" name="feeitemid" value="'+ feeitemid +'" /></form>';
	
	cell.html(form).find('input[type=text]')
		.focus()
		.css('width',cellWidth);
			   
    cell.on('click', function(){return false});
	
	cell.on('keydown',function(e) {
		if (e.keyCode == 13) {
			changeFieldTotal(cell, prevContent);
		} else if (e.keyCode == 27) {
			cell.text(prevContent);
			cell.off('click');
		}
	});
}

function changeFieldTotal( cell, prevContent ) {
     
	cell.off('keydown');

    var input = cell.find('form').serialize();
        url = cell.find('form').attr('action');

    $.ajax({
	    type: "POST",
	    url: url,
		data: input,
		dataType: "json",
		success: function(response) {
            f (data.success)
			cell.html(data.value);
		    else {
			alert("There was a problem updating the data.  Please try again.");
			cell.html(prevContent);
		    }
      }
    });	
	
	cell.off('click');
}



