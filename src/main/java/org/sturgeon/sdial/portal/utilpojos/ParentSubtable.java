package org.sturgeon.sdial.portal.utilpojos;

import org.sturgeon.sdial.common.model.SubscriberStudent;

public class ParentSubtable {
    private Long studentid;
    private Long subscriberid;
    private String phoneNumber;
    private String subscriberName;

    public ParentSubtable(SubscriberStudent subscriberStudent){
        this.studentid = subscriberStudent.getStudentid();
        this.subscriberid = subscriberStudent.getSubscriberid();
        this.phoneNumber = subscriberStudent.getSubscriber().getPhoneNumber();
        this.subscriberName = subscriberStudent.getSubscriber().getName();
    }
}
