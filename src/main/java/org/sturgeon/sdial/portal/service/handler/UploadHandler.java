package org.sturgeon.sdial.portal.service.handler;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.sturgeon.sdial.common.model.*;
import org.sturgeon.sdial.common.model.forms.*;
import org.sturgeon.sdial.common.service.school.*;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

/**
 * Created by User on 9/26/2015.
 */
@Service
public class UploadHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(UploadHandler.class);
    ExamResultsService examResultsService;
    StudentService studentService;
    SchoolService schoolService;
    ExamService examService;
    SchoolFeesService schoolFeesService;
    SmsContactService smsContactService;

    @Autowired
    public UploadHandler(ExamResultsService examResultsService, StudentService studentService, SchoolService schoolService, ExamService examService, SchoolFeesService schoolFeesService, SmsContactService smsContactService) {
        this.examResultsService = examResultsService;
        this.studentService = studentService;
        this.schoolService = schoolService;
        this.examService = examService;
        this.schoolFeesService = schoolFeesService;
        this.smsContactService = smsContactService;
    }

    public void UploadStudents(Sheet sheet, Long schoolid) {
        sheet.removeRow(sheet.getRow(0));

        for (Row row : sheet) {
            Cell cell1 = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
            Cell cell2 = row.getCell(1, Row.RETURN_BLANK_AS_NULL);

            String cell1Content;
            String cell2Content = "";

            if (cell1 != null){
                cell1.setCellType(Cell.CELL_TYPE_STRING);
                cell1Content = cell1.getStringCellValue();
                if(cell2 != null){
                    cell2.setCellType(Cell.CELL_TYPE_STRING);
                    cell2Content = cell2.getStringCellValue();
                }

                StudentCreateForm studentForm = new StudentCreateForm();
                studentForm.setSchoolid(schoolid);
                studentForm.setAdmission(cell1Content);
                studentForm.setStudentName(cell2Content);


                Student student = studentService.create(studentForm);

            } else{

            }

        }
    }

    public void UploadStudentstoGroup(Sheet sheet, Long schoolid, String studentgroupid) {
        sheet.removeRow(sheet.getRow(0));

        for (Row row : sheet) {

            Cell cell1 = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
            Cell cell2 = row.getCell(1, Row.RETURN_BLANK_AS_NULL);

            String cell1Content;
            String cell2Content = "";

            if (cell1 != null) {
                cell1.setCellType(Cell.CELL_TYPE_STRING);
                cell1Content = cell1.getStringCellValue();
                if (cell2 != null) {
                    cell2.setCellType(Cell.CELL_TYPE_STRING);
                    cell2Content = cell2.getStringCellValue();
                }
                StudentCreateForm studentForm = new StudentCreateForm();

                    studentForm.setSchoolid(schoolid);
                    studentForm.setStudentName(cell2Content);
                    studentForm.setAdmission(cell1Content);
                    studentForm.setStudentgroupid(studentgroupid);

                    studentService.AddStudentWithGroup(studentForm);
            } else {

            }

        }
    }

    public Map<Integer, Map<Integer, String>> GetOrder(Row headerRow) {
        Map<Integer, String> orderedHeader = new HashMap<Integer, String>();
        Map<Integer, String> unknownHeader = new HashMap<Integer, String>();
        Integer count = 0;

        Iterator cellIterator = headerRow.cellIterator();
        while (cellIterator.hasNext()) {
            Cell cell = (Cell) cellIterator.next();
            cell.setCellType(Cell.CELL_TYPE_STRING);
            String headerCell = cell.getStringCellValue();

            if (StringUtils.containsIgnoreCase(headerCell, "adm")) {
                orderedHeader.put(count, "admission");
            } else if (StringUtils.containsIgnoreCase(headerCell, "name")) {
                orderedHeader.put(count, "name");
            } else if (StringUtils.containsIgnoreCase(headerCell, "eng")) {
                orderedHeader.put(count, "english");
            } else if (StringUtils.containsIgnoreCase(headerCell, "kis")) {
                orderedHeader.put(count, "kiswahili");
            } else if (StringUtils.containsIgnoreCase(headerCell, "math")) {
                orderedHeader.put(count, "math");
            } else if (StringUtils.containsIgnoreCase(headerCell, "sci") && !StringUtils.containsIgnoreCase(headerCell, "home")) {
                orderedHeader.put(count, "science");
            } else if (StringUtils.containsIgnoreCase(headerCell, "bio")) {
                orderedHeader.put(count, "biology");
            } else if (StringUtils.containsIgnoreCase(headerCell, "phy") && !StringUtils.containsIgnoreCase(headerCell, "geo")) {
                orderedHeader.put(count, "physics");
            } else if (StringUtils.containsIgnoreCase(headerCell, "che")) {
                orderedHeader.put(count, "chemistry");
            } else if (StringUtils.containsIgnoreCase(headerCell, "his")) {
                orderedHeader.put(count, "history");
            } else if (StringUtils.containsIgnoreCase(headerCell, "geo")) {
                orderedHeader.put(count, "geography");
            } else if (StringUtils.containsIgnoreCase(headerCell, "cre")) {
                orderedHeader.put(count, "cre");
            } else if (StringUtils.containsIgnoreCase(headerCell, "ire")) {
                orderedHeader.put(count, "ire");
            } else if (StringUtils.containsIgnoreCase(headerCell, "hre")) {
                orderedHeader.put(count, "hre");
            } else if (StringUtils.containsIgnoreCase(headerCell, "sci") || StringUtils.containsIgnoreCase(headerCell, "home") || StringUtils.containsIgnoreCase(headerCell, "hsc")) {
                orderedHeader.put(count, "homescience");
            } else if (StringUtils.containsIgnoreCase(headerCell, "art")) {
                orderedHeader.put(count, "art");
            } else if (StringUtils.containsIgnoreCase(headerCell, "agri")) {
                orderedHeader.put(count, "agriculture");
            } else if (StringUtils.containsIgnoreCase(headerCell, "comp")) {
                orderedHeader.put(count, "computer");
            } else if (StringUtils.containsIgnoreCase(headerCell, "avi")) {
                orderedHeader.put(count, "aviation");
            } else if (StringUtils.containsIgnoreCase(headerCell, "fre")) {
                orderedHeader.put(count, "french");
            } else if (StringUtils.containsIgnoreCase(headerCell, "ger")) {
                orderedHeader.put(count, "german");
            } else if (StringUtils.containsIgnoreCase(headerCell, "ara")) {
                orderedHeader.put(count, "arabic");
            } else if (StringUtils.containsIgnoreCase(headerCell, "mus")) {
                orderedHeader.put(count, "music");
            } else if (StringUtils.containsIgnoreCase(headerCell, "bus")) {
                orderedHeader.put(count, "business");
            } else if (StringUtils.containsIgnoreCase(headerCell, "social") || StringUtils.containsIgnoreCase(headerCell, "ss") && !StringUtils.containsIgnoreCase(headerCell, "re")) {
                orderedHeader.put(count, "socialstudies");
            } else if (StringUtils.containsIgnoreCase(headerCell, "ssre")) {
                orderedHeader.put(count, "ssre");
            } else if (StringUtils.containsIgnoreCase(headerCell, "tot")) {
                orderedHeader.put(count, "total");
            } else if (StringUtils.containsIgnoreCase(headerCell, "ave")) {
                orderedHeader.put(count, "average");
            } else if (StringUtils.containsIgnoreCase(headerCell, "gra")) {
                orderedHeader.put(count, "grade");
            } else if (StringUtils.containsIgnoreCase(headerCell, "pos")) {
                orderedHeader.put(count, "position");
            } else {
                unknownHeader.put(count, headerCell);
            }

            count++;
        }
        Map<Integer, Map<Integer, String>> orderResults = new HashMap<Integer, Map<Integer, String>>();
        orderResults.put(1, orderedHeader);
        orderResults.put(2, unknownHeader);

        return orderResults;

    }



    public void SaveResults(Sheet sheet, Map<Integer, String> orderedHeader, Long examid) {
        Exam exam = examService.getExambyId(examid).get();

        sheet.removeRow(sheet.getRow(0));

        for (Row row : sheet) {

            ExamResultsForm resultsform = new ExamResultsForm();
            StudentCreateForm studentForm = new StudentCreateForm();
            Student student;


            for (int i = 0; i < orderedHeader.size(); i++) {
                String cellContent;
                Cell cell = row.getCell(i, Row.RETURN_BLANK_AS_NULL);
                if (cell == null){
                    cellContent = "";
                } else {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    cellContent = cell.getStringCellValue();
                }

                if (orderedHeader.get(i).equals("admission")) {
                    studentForm.setAdmission(cellContent);
                } else if (orderedHeader.get(i).equals("name")) {
                    studentForm.setStudentName(cellContent);
                } else if (orderedHeader.get(i).equals("english")) {
                    resultsform.setEnglish(cellContent);
                } else if (orderedHeader.get(i).equals("kiswahili")) {
                    resultsform.setKiswahili(cellContent);
                } else if (orderedHeader.get(i).equals("math")) {
                    resultsform.setMaths(cellContent);
                } else if (orderedHeader.get(i).equals("science")) {
                    resultsform.setScience(cellContent);
                } else if (orderedHeader.get(i).equals("biology")) {
                    resultsform.setBiology(cellContent);
                } else if (orderedHeader.get(i).equals("physics")) {
                    resultsform.setPhysics(cellContent);
                } else if (orderedHeader.get(i).equals("chemistry")) {
                    resultsform.setChemistry(cellContent);
                } else if (orderedHeader.get(i).equals("history")) {
                    resultsform.setHistory(cellContent);
                } else if (orderedHeader.get(i).equals("geography")) {
                    resultsform.setGeography(cellContent);
                } else if (orderedHeader.get(i).equals("cre")) {
                    resultsform.setCre(cellContent);
                } else if (orderedHeader.get(i).equals("ire")) {
                    resultsform.setIre(cellContent);
                } else if (orderedHeader.get(i).equals("hre")) {
                    resultsform.setHre(cellContent);
                } else if (orderedHeader.get(i).equals("homescience")) {
                    resultsform.setHomescience(cellContent);
                } else if (orderedHeader.get(i).equals("art")) {
                    resultsform.setArt(cellContent);
                } else if (orderedHeader.get(i).equals("agriculture")) {
                    resultsform.setAgriculture(cellContent);
                } else if (orderedHeader.get(i).equals("computer")) {
                    resultsform.setCommputer(cellContent);
                } else if (orderedHeader.get(i).equals("aviation")) {
                    resultsform.setAviation(cellContent);
                } else if (orderedHeader.get(i).equals("french")) {
                    resultsform.setFrench(cellContent);
                } else if (orderedHeader.get(i).equals("german")) {
                    resultsform.setGerman(cellContent);
                } else if (orderedHeader.get(i).equals("arabic")) {
                    resultsform.setArabic(cellContent);
                } else if (orderedHeader.get(i).equals("music")) {
                    resultsform.setMusic(cellContent);
                } else if (orderedHeader.get(i).equals("business")) {
                    resultsform.setBusinessstudies(cellContent);
                } else if (orderedHeader.get(i).equals("socialstudies")) {
                    resultsform.setSocialstudies(cellContent);
                } else if (orderedHeader.get(i).equals("ssre")) {
                    resultsform.setSsre(cellContent);
                } else if (orderedHeader.get(i).equals("total")) {
                    resultsform.setTotal(cellContent);
                } else if (orderedHeader.get(i).equals("average")) {
                    resultsform.setAverage(cellContent);
                } else if (orderedHeader.get(i).equals("grade")) {
                    resultsform.setGrade(cellContent);
                } else if (orderedHeader.get(i).equals("position")) {
                    resultsform.setPosition(cellContent);
                }

            }

            Optional<Student> studentOptional = studentService.getStudentbyAdmission(exam.getSchoolid(), studentForm.getAdmission());

            if (studentOptional.isPresent()) {
                student = studentOptional.get();
            } else {
                studentForm.setSchoolid(exam.getSchoolid());
                student = studentService.create(studentForm);
            }

            resultsform.setStudentid(student.getStudentid());
            resultsform.setExamid(exam.getExamid());

            ExamResults examResults = examResultsService.create(resultsform);
        }
    }

    public void UploadFeeBalance(Sheet sheet, Long schoolid) {
        sheet.removeRow(sheet.getRow(0));

        for (Row row : sheet) {
            Cell cell1 = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
            Cell cell2 = row.getCell(1, Row.RETURN_BLANK_AS_NULL);
            Cell cell3 = row.getCell(2, Row.RETURN_BLANK_AS_NULL);
            Cell cell4 = row.getCell(3, Row.RETURN_BLANK_AS_NULL);

            String cell1Content;
            String cell2Content = null;
            String cell3Content = null;
            String cell4Content = null;

            if (cell1 != null) {
                cell1.setCellType(Cell.CELL_TYPE_STRING);
                cell1Content = cell1.getStringCellValue();

                if (cell2 != null) {
                    cell2.setCellType(Cell.CELL_TYPE_STRING);
                    cell2Content = cell2.getStringCellValue();
                }
                if (cell3 != null) {
                    cell3.setCellType(Cell.CELL_TYPE_STRING);
                    cell3Content = cell3.getStringCellValue();
                }
                if (cell4 != null) {
                    cell4.setCellType(Cell.CELL_TYPE_STRING);
                    cell4Content = cell4.getStringCellValue();
                }

                StudentCreateForm studentForm = new StudentCreateForm();
                studentForm.setSchoolid(schoolid);
                Student student;
                studentForm.setStudentName(cell2Content);
                studentForm.setAdmission(cell1Content);
                Optional<Student> studentOptional = studentService.getStudentbyAdmission(studentForm.getSchoolid(), studentForm.getAdmission());

                if (!studentOptional.isPresent()) {
                    student = studentService.create(studentForm);
                } else {
                    student = studentOptional.get();
                }

                FeeBalanceForm feeBalanceForm = new FeeBalanceForm();
                feeBalanceForm.setStudentid(student.getStudentid());
                feeBalanceForm.setBalance(cell3Content);
                feeBalanceForm.setPaid(cell4Content);

                FeeBalance feeBalance = schoolFeesService.addFeeBalance(feeBalanceForm);
            } else {

            }
        }
    }

    public void UploadParents(Sheet sheet, Long schoolid) {
        sheet.removeRow(sheet.getRow(0));

        for (Row row : sheet) {

            Cell cell1 = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
            Cell cell2 = row.getCell(1, Row.RETURN_BLANK_AS_NULL);
            Cell cell3 = row.getCell(2, Row.RETURN_BLANK_AS_NULL);
            Cell cell4 = row.getCell(3, Row.RETURN_BLANK_AS_NULL);
            Cell cell5 = row.getCell(4, Row.RETURN_BLANK_AS_NULL);

            String cell1Content;
            String cell2Content = null;
            String cell3Content;
            String cell4Content = null;
            String cell5Content = null;

            if (cell1 != null && cell3 != null) {
                cell1.setCellType(Cell.CELL_TYPE_STRING);
                cell1Content = cell1.getStringCellValue();

                cell3.setCellType(Cell.CELL_TYPE_STRING);
                cell3Content = cell3.getStringCellValue();

                if (cell2 != null) {
                    cell2.setCellType(Cell.CELL_TYPE_STRING);
                    cell2Content = cell2.getStringCellValue();
                }
                if (cell4 != null) {
                    cell4.setCellType(Cell.CELL_TYPE_STRING);
                    cell4Content = cell4.getStringCellValue();
                }
                if (cell5 != null) {
                    cell5.setCellType(Cell.CELL_TYPE_STRING);
                    cell5Content = cell5.getStringCellValue();
                }

                ParentAddForm parentAddForm = new ParentAddForm();
                parentAddForm.setAdmission(cell1Content);
                parentAddForm.setStudentName(cell2Content);
                parentAddForm.setPhoneNumber(cell3Content);
                parentAddForm.setParentName(cell4Content);

                parentAddForm.setSchoolid(schoolid);
                if (StringUtils.containsIgnoreCase(cell5Content, "pri")) {
                    parentAddForm.setIsPrimary(true);
                } else {
                    parentAddForm.setIsPrimary(false);
                }

                Subscriber subscriber = studentService.createSubscriber(parentAddForm);

            } else {

            }
        }
    }


    public void UploadSmsContact(Sheet sheet, Long schoolid, Long smscontactgroupid) {
        sheet.removeRow(sheet.getRow(0));

        for (Row row : sheet) {
            Cell cell1 = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
            Cell cell2 = row.getCell(1, Row.RETURN_BLANK_AS_NULL);

            String cell1Content;
            String cell2Content = "";

            if (cell1 != null){
                cell1.setCellType(Cell.CELL_TYPE_STRING);
                cell1Content = cell1.getStringCellValue();
                if(cell2 != null){
                    cell2.setCellType(Cell.CELL_TYPE_STRING);
                    cell2Content = cell2.getStringCellValue();
                }

                SmsContactCreateForm form = new SmsContactCreateForm();
                form.setSchoolid(schoolid);
                form.setSmscontactgroupid(smscontactgroupid);
                form.setPhoneNumber(cell1Content);
                form.setName(cell2Content);

                SmsContact smsContact = smsContactService.createSmsContact(form);

            } else{

            }

        }
    }

}
