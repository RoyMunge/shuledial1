package org.sturgeon.sdial.portal.service.user;


import org.sturgeon.sdial.common.model.PortalUser;
import org.sturgeon.sdial.common.model.forms.UserCreateForm;

import java.util.Collection;
import java.util.Optional;

public interface UserService {

    Optional<PortalUser> getUserById(long id);

    Optional<PortalUser> getUserByEmail(String email);

    Collection<PortalUser> getAllUsers();

    PortalUser create(UserCreateForm userform, Long schoolId);

}
