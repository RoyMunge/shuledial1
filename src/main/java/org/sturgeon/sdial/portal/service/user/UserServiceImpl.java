package org.sturgeon.sdial.portal.service.user;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.model.PortalUser;
import org.sturgeon.sdial.common.model.forms.UserCreateForm;
import org.sturgeon.sdial.common.repository.UserRepository;

import java.util.Collection;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<PortalUser> getUserById(long id) {
        LOGGER.debug("Getting user={}", id);
        return Optional.ofNullable(userRepository.findOne(id));
    }

    @Override
    public Optional<PortalUser> getUserByEmail(String email) {
        LOGGER.debug("Getting user by email={}", email.replaceFirst("@.*", "@***"));
        return userRepository.findOneByEmail(email);
    }

    @Override
    public Collection<PortalUser> getAllUsers() {
        LOGGER.debug("Getting all users");
        return userRepository.findAll(new Sort("email"));
    }

    @Override
    public PortalUser create(UserCreateForm form, Long schoolid) {
        PortalUser user = new PortalUser();
        user.setEmail(form.getUserEmail());
        user.setPasswordHash(new BCryptPasswordEncoder().encode(form.getPassword()));
        user.setFirstName(form.getFirstName().toUpperCase());
        user.setOtherNames(form.getOtherNames().toUpperCase());
        user.setPhone(form.getUserPhone());
        user.setSchoolid(schoolid);
        return userRepository.save(user);
    }

}
