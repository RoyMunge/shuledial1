package org.sturgeon.sdial.portal.controller;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.sturgeon.sdial.common.model.Exam;
import org.sturgeon.sdial.common.model.StudentGroup;
import org.sturgeon.sdial.common.model.forms.SmsContactCreateForm;
import org.sturgeon.sdial.common.model.forms.StudentCreateForm;
import org.sturgeon.sdial.common.model.forms.StudentGroupCreateForm;
import org.sturgeon.sdial.common.service.school.SchoolFeesService;
import org.sturgeon.sdial.portal.service.handler.UploadHandler;
import org.sturgeon.sdial.common.service.school.ExamService;
import org.sturgeon.sdial.common.service.school.SchoolService;
import org.sturgeon.sdial.common.service.school.StudentService;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by User on 9/28/2015.
 */
@Controller
public class UploadController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PortalController.class);
    UploadHandler uploadHandler;
    StudentService studentService;
    ExamService examService;
    SchoolService schoolService;
    SchoolFeesService schoolFeesService;

    public ConcurrentMap<Integer, Sheet> sheetMap = new ConcurrentHashMap();
    public ConcurrentMap<Integer, Map<Integer, String>> orderedHeaderMap = new ConcurrentHashMap();

    @Autowired
    public UploadController(UploadHandler uploadHandler, StudentService studentService, ExamService examService, SchoolService schoolService, SchoolFeesService schoolFeesService) {
        this.uploadHandler = uploadHandler;
        this.studentService = studentService;
        this.examService = examService;
        this.schoolService = schoolService;
        this.schoolFeesService = schoolFeesService;
    }

    @RequestMapping(value = "/uploadstudents", headers = ("content-type=multipart/*"), method = RequestMethod.POST)
    public String UploadStudent(@RequestParam("file") MultipartFile file, @RequestParam("schoolid") Long schoolid, @ModelAttribute("studentForm") StudentCreateForm studentForm, RedirectAttributes redirectAttributes, Model model) {
        if (!file.isEmpty()) {
            try {
                Workbook workbook = WorkbookFactory.create(file.getInputStream());
                Sheet sheet = workbook.getSheetAt(0);

                if (studentForm.getStudentgroupid() == null && studentForm.getStudentGroupName().isEmpty()) {
                    uploadHandler.UploadStudents(sheet, schoolid);
                } else if (!studentForm.getStudentGroupName().isEmpty() && studentForm.getStudentgroupid() == null){
                    StudentGroupCreateForm form = new StudentGroupCreateForm();
                    form.setName(studentForm.getStudentGroupName());
                    form.setSchoolid(schoolid);
                    form.setDescription("");
                    StudentGroup studentGroup = studentService.createStudentGroup(form);

                    if (studentGroup != null){
                        uploadHandler.UploadStudentstoGroup(sheet, schoolid, String.valueOf(studentGroup.getStudentgroupid()));
                    }

                }  else if (studentForm.getStudentGroupName().isEmpty() && studentForm.getStudentgroupid() != null)  {
                    uploadHandler.UploadStudentstoGroup(sheet, schoolid, studentForm.getStudentgroupid());
                } else {
                    redirectAttributes.addFlashAttribute("bindinguploaderrors", "Error! please only create new group or upload to existing groups. Not both");
                    redirectAttributes.addAttribute("selected", "upload");
                    return "redirect:/newstudent";
                }
            } catch (Exception e) {
                LOGGER.error("Exception occurred when trying to upload the students", e);
                redirectAttributes.addFlashAttribute("bindinguploaderrors", "Error! Please confirm the file and upload again.");
                redirectAttributes.addAttribute("selected", "upload");
                return "redirect:/newstudent";
            }


        } else {
            LOGGER.error("Empty file was uploaded");
            redirectAttributes.addFlashAttribute("bindingadderrors", "No file was uploaded");
            redirectAttributes.addAttribute("selected", "upload");
            return "redirect:/newstudent";
        }

        redirectAttributes.addFlashAttribute("success", "Students have been uploaded successfully");
        redirectAttributes.addAttribute("selected", "upload");
        return "redirect:/newstudent";
    }

   /* @RequestMapping(value = "/uploadstudentstogroup", headers = ("content-type=multipart/*"), method = RequestMethod.POST)
    public String UploadStudentToGroup(@RequestParam("file") MultipartFile file, @RequestParam("schoolid") Long schoolid, @RequestParam("studentgroupid") Long studentgroupid, RedirectAttributes redirectAttributes, Model model) {
        if (!file.isEmpty()) {
            try {
                XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
                XSSFSheet sheet = workbook.getSheetAt(0);
                if (studentgroupid.equals(null)) {
                    uploadHandler.UploadStudents(sheet, schoolid);
                } else {
                    uploadHandler.UploadStudentstoGroup(sheet, schoolid, studentgroupid);
                }
            } catch (Exception e) {
                LOGGER.error("Exception occurred when trying to upload the students", e);
                model.addAttribute("bindinguploaderrors", "Error! Please confirm the file and upload again.");
                model.addAttribute("studentCount", studentService.count(schoolid));
                model.addAttribute("studentForm", new StudentCreateForm());
                return "students";

            }
        } else {
            LOGGER.error("Empty file was uploaded");
            model.addAttribute("bindinguploaderrors", "No file was uploaded");
            model.addAttribute("studentCount", studentService.count(schoolid));
            model.addAttribute("studentForm", new StudentCreateForm());
            return "students";
        }

        redirectAttributes.addFlashAttribute("success", "Students have been uploaded successfully");
        return "redirect:/students";
    }


    @RequestMapping(value = "/uploadresults", method = RequestMethod.POST)
    public String UploadResult(@RequestParam("file") MultipartFile file, @RequestParam("schoolid") Long schoolid, @RequestParam("examid") Long examid, RedirectAttributes redirectAttributes, Model model){
        Exam exam = examService.getExambyId(examid).get();
        if (!file.isEmpty()){
            try {
                XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
                XSSFSheet sheet = workbook.getSheetAt(0);
                Row row = sheet.getRow(0);
                int sheetID = randInt(0,300);
                sheetMap.put(sheetID, sheet);

                if (schoolService.getSchoolById(schoolid).get().getType().equals("PRIMARY")){
                    Map <Integer, Map<Integer, String>> headerOrderMap = uploadHandler.GetOrderPrimary(row);
                    Map<Integer,String> orderedHeader = headerOrderMap.get(1);
                    Map<Integer,String> unknownCells = headerOrderMap.get(2);
                    Integer headerID = randInt(0,300);
                    orderedHeaderMap.put(headerID,orderedHeader);
                    model.addAttribute("orderedHeader",orderedHeader);
                    model.addAttribute("headerID", headerID);
                    model.addAttribute("sheetID", sheetID);
                    model.addAttribute("exam", exam);
                    if (!unknownCells.isEmpty()) {
                        model.addAttribute("unknownCells", unknownCells);
                    }
                    return "addresults";
                } else {
                    Map <Integer, Map<Integer, String>> headerOrderMap = uploadHandler.GetOrderSecondary(row);
                    Map<Integer,String> orderedHeader = headerOrderMap.get(1);
                    Map<Integer,String> unknownCells = headerOrderMap.get(2);
                    Integer headerID = randInt(0,300);
                    orderedHeaderMap.put(headerID,orderedHeader);
                    model.addAttribute("orderedHeader",orderedHeader);
                    model.addAttribute("headerID", headerID);
                    model.addAttribute("sheetID", sheetID);
                    model.addAttribute("exam", exam);
                    if (!unknownCells.isEmpty()) {
                        model.addAttribute("unknownCells", unknownCells);
                    }
                    return "addresults";
                }

            } catch (Exception e){
                LOGGER.error("Exception occurred when trying to upload the students", e);
                redirectAttributes.addFlashAttribute("bindinguploaderrors", "Error! Please confirm the file and upload again.");
                model.addAttribute("exam", exam);
                return "addresults";
        }

        } else {
            LOGGER.info("Empty file was uploaded");
            model.addAttribute("bindinguploaderrors", "No file was uploaded");
            model.addAttribute("exam", exam);
            return "results";
        }
    }

    @RequestMapping(value = "/confirmupload", method = RequestMethod.POST)
    public String ConfirmResult(@RequestParam("schoolid") Long schoolid, @RequestParam("examid") Long examid, @RequestParam("sheetID") Integer sheetID, @RequestParam("headerID") Integer headerID, RedirectAttributes redirectAttributes){
        if (schoolService.getSchoolById(schoolid).get().getType().equals("PRIMARY")){
            uploadHandler.SavePrimary(sheetMap.get(sheetID),orderedHeaderMap.get(headerID),examid);
            orderedHeaderMap.remove(headerID);
            sheetMap.remove(sheetID);
        } else {
            uploadHandler.SaveSecondary(sheetMap.get(sheetID),orderedHeaderMap.get(headerID),examid);
            orderedHeaderMap.remove(headerID);
            sheetMap.remove(sheetID);
        }
        redirectAttributes.addFlashAttribute("success", "Results have been successfully uploaded");
        return "redirect:/results";
    }


    private static Integer randInt(int min, int max) {
        Random rand = new Random();
        Integer randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    */

    @RequestMapping(value = "/addresults", method = {RequestMethod.POST, RequestMethod.GET})
    public String AddResults(@RequestParam("examid") Long examid, Model model) {
        Exam exam = examService.getExambyId(examid).get();
        model.addAttribute("exam", exam);
        return "addresults";
    }


    @RequestMapping(value = "/uploadresults", method = RequestMethod.POST)
    public String UploadResult(@RequestParam("file") MultipartFile file, @RequestParam("schoolid") Long schoolid, @RequestParam("examid") Long examid, RedirectAttributes redirectAttributes, Model model) {
        Exam exam = examService.getExambyId(examid).get();
        if (!file.isEmpty()) {
            try {
                Workbook workbook = WorkbookFactory.create(file.getInputStream());
                Sheet sheet = workbook.getSheetAt(0);
                Row row = sheet.getRow(0);
                int sheetID = randInt(0, 300);
                sheetMap.put(sheetID, sheet);

                Map<Integer, Map<Integer, String>> headerOrderMap = uploadHandler.GetOrder(row);
                Map<Integer, String> orderedHeader = headerOrderMap.get(1);
                Map<Integer, String> unknownCells = headerOrderMap.get(2);
                Integer headerID = randInt(0, 300);
                orderedHeaderMap.put(headerID, orderedHeader);
                model.addAttribute("orderedHeader", orderedHeader);
                model.addAttribute("headerID", headerID);
                model.addAttribute("sheetID", sheetID);
                model.addAttribute("exam", exam);
                if (!unknownCells.isEmpty()) {
                    model.addAttribute("unknownCells", unknownCells);
                }
                return "addresults";


            } catch (Exception e) {
                LOGGER.error("Exception occurred when trying to upload the students", e);
                redirectAttributes.addFlashAttribute("bindinguploaderrors", "Error! Please confirm the file and upload again.");
                model.addAttribute("exam", exam);
                return "addresults";
            }

        } else {
            LOGGER.info("Empty file was uploaded");
            model.addAttribute("bindinguploaderrors", "No file was uploaded");
            model.addAttribute("exam", exam);
            return "results";
        }
    }

    @RequestMapping(value = "/confirmupload", method = RequestMethod.POST)
    public String ConfirmResult(@RequestParam("schoolid") Long schoolid, @RequestParam("examid") Long examid, @RequestParam("sheetID") Integer sheetID, @RequestParam("headerID") Integer headerID, RedirectAttributes redirectAttributes) {
        uploadHandler.SaveResults(sheetMap.get(sheetID), orderedHeaderMap.get(headerID), examid);
        orderedHeaderMap.remove(headerID);
        sheetMap.remove(sheetID);

        redirectAttributes.addFlashAttribute("success", "Results have been successfully uploaded");
        return "redirect:/results";
    }

    private static Integer randInt(int min, int max) {
        Random rand = new Random();
        Integer randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }


    @RequestMapping(value = "/uploadfeebalance", headers = ("content-type=multipart/*"), method = RequestMethod.POST)
    public String UploadFeeBalance(@RequestParam("file") MultipartFile file, @RequestParam("schoolid") Long schoolid, RedirectAttributes redirectAttributes, Model model) {
        if (!file.isEmpty()) {
            try {
                Workbook workbook = WorkbookFactory.create(file.getInputStream());
                Sheet sheet = workbook.getSheetAt(0);
                uploadHandler.UploadFeeBalance(sheet, schoolid);
            } catch (Exception e) {
                LOGGER.error("Exception occurred when trying to upload the students", e);
                //bindingResult.reject("student.error", "An Error Occurred while adding the student");
                redirectAttributes.addFlashAttribute("bindinguploaderrors", "Error! Please confirm the file and upload again.");
                return "redirect:/fees";

            }
        } else {
            LOGGER.error("Empty file was uploaded");
            redirectAttributes.addFlashAttribute("bindinguploaderrors", "No file was uploaded");
            return "redirect:/fees";
        }

        redirectAttributes.addFlashAttribute("success", "Students have been uploaded successfully");
        return "redirect:/fees";
    }

    @RequestMapping(value = "/uploadparents", headers = ("content-type=multipart/*"), method = RequestMethod.POST)
    public String UploadParents(@RequestParam("file") MultipartFile file, @RequestParam("schoolid") Long schoolid, RedirectAttributes redirectAttributes, Model model) {
        if (!file.isEmpty()) {
            try {
                Workbook workbook = WorkbookFactory.create(file.getInputStream());
                Sheet sheet = workbook.getSheetAt(0);
                uploadHandler.UploadParents(sheet, schoolid);
            } catch (Exception e) {
                LOGGER.error("Exception occurred when trying to upload the parents", e);
                //bindingResult.reject("student.error", "An Error Occurred while adding the student");
                redirectAttributes.addFlashAttribute("bindinguploaderrors", "Error! Please confirm the file and upload again.");
                redirectAttributes.addAttribute("selected", "upload");
                return "redirect:/newparent";

            }
        } else {
            LOGGER.error("Empty file was uploaded");
            redirectAttributes.addFlashAttribute("bindinguploaderrors", "No file was uploaded");
            redirectAttributes.addAttribute("selected", "upload");
            return "redirect:/newparent";
        }

        redirectAttributes.addFlashAttribute("success", "Parents have been uploaded successfully");
        redirectAttributes.addAttribute("selected", "upload");
        return "redirect:/newparent";
    }

    @RequestMapping(value = "/uploadsmscontacts", headers = ("content-type=multipart/*"), method = RequestMethod.POST)
    public String UploadSmsContacts(@RequestParam("file") MultipartFile file, @RequestParam("schoolid") Long schoolid, @ModelAttribute("smsContactAddForm") SmsContactCreateForm form, RedirectAttributes redirectAttributes, Model model) {
        if (!file.isEmpty()) {
            try {
                Workbook workbook = WorkbookFactory.create(file.getInputStream());
                Sheet sheet = workbook.getSheetAt(0);
                if (form.getSmscontactgroupid() !=null) {
                    uploadHandler.UploadSmsContact(sheet, schoolid, form.getSmscontactgroupid());
                } else {
                    redirectAttributes.addFlashAttribute("bindinguploaderrors", "Please select a contact group to upload the contacts to");
                    return "redirect:/bulksms";
                }
            } catch (Exception e) {
                LOGGER.error("Exception occurred when trying to upload the students", e);
                redirectAttributes.addFlashAttribute("bindinguploaderrors", "Error! Please confirm the file and upload again.");
                return "redirect:/bulksms";
            }

        } else {
            LOGGER.error("Empty file was uploaded");
            redirectAttributes.addFlashAttribute("bindingadderrors", "No file was uploaded");
            return "redirect:/bulksms";
        }

        redirectAttributes.addFlashAttribute("success", "Students have been uploaded successfully");
        return "redirect:/bulksms";
    }
}
