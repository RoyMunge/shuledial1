package org.sturgeon.sdial.portal.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.sturgeon.sdial.common.model.School;
import org.sturgeon.sdial.common.model.PortalUser;
import org.sturgeon.sdial.common.model.forms.SchoolCreateForm;
import org.sturgeon.sdial.common.model.forms.UserCreateForm;
import org.sturgeon.sdial.common.model.validator.SchoolCreateFormValidator;
import org.sturgeon.sdial.common.model.validator.UserCreateFormValidator;
import org.sturgeon.sdial.common.service.school.SchoolService;
import org.sturgeon.sdial.common.service.school.SchoolTypeService;
import org.sturgeon.sdial.portal.service.user.UserService;

import javax.validation.Valid;

@Controller
public class RegistrationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);
    private final UserService userService;
    private final SchoolService schoolService;
    private final SchoolTypeService schoolTypeService;
    private final UserCreateFormValidator userCreateFormValidator;
    private final SchoolCreateFormValidator schoolCreateFormValidator;
    private SchoolCreateForm schoolCreateForm;

    @Autowired
    public RegistrationController(UserService userService, SchoolService schoolService, SchoolTypeService schoolTypeService, UserCreateFormValidator userCreateFormValidator, SchoolCreateFormValidator schoolCreateFormValidator) {
        this.userService = userService;
        this.schoolService = schoolService;
        this.schoolTypeService = schoolTypeService;
        this.userCreateFormValidator = userCreateFormValidator;
        this.schoolCreateFormValidator = schoolCreateFormValidator;
    }

    @InitBinder("userForm")
    public void initUserBinder(WebDataBinder binder) {
        binder.addValidators(userCreateFormValidator);
    }


    @InitBinder("schoolForm")
    public void initSchoolBinder(WebDataBinder binder) {
        binder.addValidators(schoolCreateFormValidator);
    }


    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String getUserCreatePage(Model model) {
        LOGGER.debug("Getting registration forms");
        model.addAttribute("schoolForm", new SchoolCreateForm());
        model.addAttribute("userForm", new UserCreateForm());
        model.addAttribute("counties", schoolService.getAllCounties());
        model.addAttribute("subcounties", schoolService.getAllSubCounties());
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String handleSchoolCreateForm(@Valid @ModelAttribute("schoolForm") SchoolCreateForm schoolForm, BindingResult bindingSchoolResult, @Valid @ModelAttribute("userForm") UserCreateForm userForm, BindingResult bindingUserResult, Model model) {

        if (bindingSchoolResult.hasErrors() || bindingUserResult.hasErrors()) {
            model.addAttribute("schoolForm", new SchoolCreateForm());
            model.addAttribute("userForm", new UserCreateForm());
            model.addAttribute("schoolTypes", schoolTypeService.getAllSchoolTypes());
            return "register";
        }
        try {
            LOGGER.debug("Processing school create form = {}", schoolForm);

            School school = schoolService.create(schoolForm);

            LOGGER.debug("School created Name = {}, ID = {}", school.getName(), school.getSchoolid());

            try {
                PortalUser user = userService.create(userForm, school.getSchoolid());

                LOGGER.debug("Usr created Name = {}, email = {}, schoolID = {}", user.getFirstName(), user.getEmail(), user.getSchoolid());

            } catch (DataIntegrityViolationException e) {
                // probably email already exists - very rare case when multiple admins are adding same user
                // at the same time and form validation has passed for more than one of them.
                LOGGER.warn("Exception occurred when trying to save the user, assuming duplicate email", e);
                bindingUserResult.reject("email.exists", "Email already exists");
                return "register";
            }
        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            bindingSchoolResult.reject("school.error", "An Error Occurred while Creating the School");
            return "register";
        }
        // ok, redirect
        return "registersucess";
    }
}
