package org.sturgeon.sdial.portal.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.Authenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.sturgeon.sdial.common.model.*;
import org.sturgeon.sdial.common.model.forms.*;
import org.sturgeon.sdial.common.model.validator.StudentCreateFormValidator;
import org.sturgeon.sdial.common.service.school.*;
import org.sturgeon.sdial.portal.utilpojos.ParentSubtable;
import org.sturgeon.sdial.sms.service.SmsService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.*;

@Controller
public class PortalController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PortalController.class);
    private final StudentService studentService;
    private final ExamService examService;
    private final StudentCreateFormValidator studentCreateFormValidator;
    private final EventService eventService;
    private final AnnouncementService announcementService;
    private final SchoolFeesService schoolFeesService;
    private final SchoolLevelService schoolLevelService;
    private final SchoolPeriodService schoolPeriodService;
    private final SchoolService schoolService;
    private final SmsContactService smsContactService;
    private final SmsService smsService;

    @Autowired
    public PortalController(StudentService studentService, ExamService examService, StudentCreateFormValidator studentCreateFormValidator, EventService eventService, AnnouncementService announcementService, SchoolFeesService schoolFeesService, SchoolLevelService schoolLevelService, SchoolPeriodService schoolPeriodService, SchoolService schoolService, SmsContactService smsContactService, SmsService smsService) {
        this.studentService = studentService;
        this.examService = examService;
        this.studentCreateFormValidator = studentCreateFormValidator;
        this.eventService = eventService;
        this.announcementService = announcementService;
        this.schoolFeesService = schoolFeesService;
        this.schoolLevelService = schoolLevelService;
        this.schoolPeriodService = schoolPeriodService;
        this.schoolService = schoolService;
        this.smsContactService = smsContactService;
        this.smsService = smsService;
    }

    @InitBinder("userForm")
    public void initStudentBinder(WebDataBinder binder) {
        binder.addValidators(studentCreateFormValidator);
    }

    @RequestMapping(value = "/")
    public String getPortalHomePage(Model model, Authentication authentication) {
        LOGGER.debug("opening account home page");
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        model.addAttribute("studentCount", studentService.count(currentUser.getUser().getSchoolid()));
        model.addAttribute("examCount", examService.count(currentUser.getUser().getSchoolid()));
        model.addAttribute("eventCount", eventService.count(currentUser.getUser().getSchoolid()));
        model.addAttribute("announcementCount", announcementService.count(currentUser.getUser().getSchoolid()));
        return "school";
    }

    @RequestMapping(value = "/school")
    public String getSchoolPage(Model model, Authentication authentication) {
        LOGGER.debug("opening account home page");
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        model.addAttribute("studentCount", studentService.count(currentUser.getUser().getSchoolid()));
        model.addAttribute("examCount", examService.count(currentUser.getUser().getSchoolid()));
        model.addAttribute("eventCount", eventService.count(currentUser.getUser().getSchoolid()));
        model.addAttribute("announcementCount", announcementService.count(currentUser.getUser().getSchoolid()));
        return "school";
    }

    @RequestMapping(value = "/students", method = RequestMethod.GET)
    public String getStudentsPage(@RequestParam(value = "selectedgroup", required = false) String selectedGroup, Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();

        if (selectedGroup == null) {

            model.addAttribute("students", studentService.getStudentsbySchool(currentUser.getUser().getSchoolid()));
            model.addAttribute("studentCount", studentService.count(currentUser.getUser().getSchoolid()));
            model.addAttribute("studentForm", new StudentCreateForm());
            model.addAttribute("studentEditForm", new StudentEditForm());
            model.addAttribute("studentGroupAddForm", new StudentGroupCreateForm());
            model.addAttribute("parentAddForm", new ParentAddForm());
            model.addAttribute("parentEditForm", new ParentEditForm());
            model.addAttribute("studentGroups", schoolService.getSchoolById(currentUser.getUser().getSchoolid()).get().getStudentGroupSet());
            model.addAttribute("selectedGroup", "all_students");
            model.addAttribute("selectedGroupName", "All Students");
            return "students";
        } else {

            Set<Student> students = studentService.getStudentsbyGroup(Long.parseLong(selectedGroup));
            model.addAttribute("students", students);
            model.addAttribute("studentCount", studentService.count(currentUser.getUser().getSchoolid()));
            model.addAttribute("studentForm", new StudentCreateForm());
            model.addAttribute("studentEditForm", new StudentEditForm());
            model.addAttribute("studentGroupAddForm", new StudentGroupCreateForm());
            model.addAttribute("parentAddForm", new ParentAddForm());
            model.addAttribute("parentEditForm", new ParentEditForm());
            model.addAttribute("studentGroups", schoolService.getSchoolById(currentUser.getUser().getSchoolid()).get().getStudentGroupSet());
            model.addAttribute("selectedGroup", Long.parseLong(selectedGroup));
            model.addAttribute("selectedGroupName", studentService.findStudentGroupById(Long.parseLong(selectedGroup)).get().getName());
            return "students";
        }

    }

    @RequestMapping(value = "/results")
    public String getResultsPage(Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        model.addAttribute("examCount", examService.count(currentUser.getUser().getSchoolid()));
        model.addAttribute("examForm", new ExamCreateForm());
        model.addAttribute("exams", examService.getExamsbySchoolId(currentUser.getUser().getSchoolid()));
        return "results";
    }

    @RequestMapping(value = "/events")
    public String getEventsage(Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        model.addAttribute("eventCount", eventService.count(currentUser.getUser().getSchoolid()));
        model.addAttribute("eventForm", new EventCreateForm());
        model.addAttribute("upcomingEvents", eventService.getUpcomingEvents(currentUser.getUser().getSchoolid()));
        model.addAttribute("studentGroups", schoolService.getSchoolById(currentUser.getUser().getSchoolid()).get().getStudentGroupSet());

        return "events";
    }

    @RequestMapping(value = "/getevents", method = RequestMethod.GET)
    public
    @ResponseBody
    String getEvent(HttpServletResponse response, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        Collection<Event> events = eventService.getEventsbySchoolId(currentUser.getUser().getSchoolid());
        Collection<Map<String, Object>> eventCollection = new ArrayList<Map<String, Object>>();

        Iterator iterator = events.iterator();

        while (iterator.hasNext()) {
            Event iterateEvent = (Event) iterator.next();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("id", iterateEvent.getEventid());
            map.put("title", iterateEvent.getEventName());
            map.put("start", iterateEvent.getEventDate());
            map.put("backgroundColor", "#0073b7");
            map.put("borderColor", "#0073b7");
            eventCollection.add(map);
        }

        // Convert to JSON string.
        String json = new Gson().toJson(eventCollection);

        // Write JSON string.
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        LOGGER.debug("returning events in json {}", json);

        return json;
    }

    @RequestMapping(value = "/events", method = RequestMethod.POST)
    public String CreateEvent(@Valid @ModelAttribute("eventForm") EventCreateForm eventForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("bindingadderrors", "Error in Creating Event! Please confirm the details");
            model.addAttribute("eventCount", eventService.count(eventForm.getSchoolid()));
            model.addAttribute("upcomingEvents", eventService.getUpcomingEvents(eventForm.getSchoolid()));
            return "events";
        }

        try {
            LOGGER.debug("Processing event create form = {}", eventForm);

            eventService.create(eventForm);


        } catch (Exception e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            bindingResult.reject("event.error", "An Error Occurred while creating the event");
            model.addAttribute("bindingadderrors", "Error in creating the Event! Please confirm the details");
            model.addAttribute("eventCount", eventService.count(eventForm.getSchoolid()));
            model.addAttribute("upcomingEvents", eventService.getUpcomingEvents(eventForm.getSchoolid()));
            return "events";
        }

        redirectAttributes.addFlashAttribute("success", "Event has been created successfully");
        return "redirect:/events";
    }

    @RequestMapping(value = "/searchevent", method = RequestMethod.POST)
    public String SearchEvent(@RequestParam("searchName") String searchName, @RequestParam("schoolid") Long schoolid, RedirectAttributes redirectAttributes) {

        Collection<Event> searchEventResults = eventService.getByName(schoolid, searchName);

        if (!searchEventResults.isEmpty()) {
            redirectAttributes.addFlashAttribute("searchEventResults", searchEventResults);
        } else {
            redirectAttributes.addFlashAttribute("noevent", "No Event with the specified Name was found");
        }

        return "redirect:/events";
    }

    @RequestMapping(value = "/announcements")
    public String getAnnouncementsPage(Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        model.addAttribute("announcementCount", announcementService.count(currentUser.getUser().getSchoolid()));
        model.addAttribute("announcementForm", new AnnouncementCreateForm());
        model.addAttribute("announcements", announcementService.getAnnouncementsbySchoolId(currentUser.getUser().getSchoolid()));
        model.addAttribute("studentGroups", schoolService.getSchoolById(currentUser.getUser().getSchoolid()).get().getStudentGroupSet());
        return "announcements";
    }

    @RequestMapping(value = "/addannouncement", method = RequestMethod.POST)
    public String handleAnnouncementCreateForm(@Valid @ModelAttribute("announcementForm") AnnouncementCreateForm announcementForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            model.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            model.addAttribute("announcementCount", announcementService.count(announcementForm.getSchoolid()));
            return "announcements";
        }

        try {
            LOGGER.debug("Processing announcement create form = {}", announcementForm);

            announcementService.create(announcementForm);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the annoucement", e);
            bindingResult.reject("announcement.error", "An Error Occurred while adding the annoucement");
            model.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            model.addAttribute("annoucementCount", announcementService.count(announcementForm.getSchoolid()));
            return "announcements";
        }
        redirectAttributes.addFlashAttribute("success", "Announcement has been added successfully");
        return "redirect:/announcements";
    }

    @RequestMapping(value = "/students", method = RequestMethod.POST)
    public String handleStudentCreateForm(@Valid @ModelAttribute("studentForm") StudentCreateForm studentForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the details");
            redirectAttributes.addAttribute("selected", "single");
            return "redirect:/newstudent";
        }

        try {
            LOGGER.debug("Processing school create form = {}", studentForm);

            Student student = studentService.AddStudentWithGroup(studentForm);

            LOGGER.debug("student created admission = {}", studentService.getStudentbyAdmission(studentForm.getSchoolid(), studentForm.getAdmission()));
        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            bindingResult.reject("student.error", "An Error Occurred while adding the student");
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the details");
            redirectAttributes.addAttribute("selected", "single");
            return "redirect:/newstudent";
        }
        redirectAttributes.addFlashAttribute("success", "Student has been added successfully");
        redirectAttributes.addAttribute("selected", "single");
        return "redirect:/newstudent";
    }

    @RequestMapping(value = "/editstudent", method = RequestMethod.POST)
    public String handleStudentEditForm(@Valid @ModelAttribute("studentEditForm") StudentEditForm studentEditForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the details");
            return "redirect:/students";
        }

        try {
            LOGGER.debug("Processing school edit form = {}", studentEditForm);

            Student student = studentService.editStudent(studentEditForm);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            bindingResult.reject("student.error", "An Error Occurred while editing the student");
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the details");
            return "redirect:/students";
        }
        redirectAttributes.addFlashAttribute("success", "Student has been edited successfully");
        return "redirect:/students";
    }

    //delete user
    @RequestMapping(value="/deletestudents", method = RequestMethod.POST)
    public String deleteStudents (@RequestParam String studentstodelete, RedirectAttributes redirectAttributes) {
        try {
            studentService.deleteStudents(studentstodelete);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            redirectAttributes.addFlashAttribute("success", "Error! Student(s) not deleted. Please try again.");
            return "redirect:/students";
        }
        redirectAttributes.addFlashAttribute("success", "Student(s) have been deleted successfully");
        return "redirect:/students";
    }


    @RequestMapping(value="/addstudentstogroups", method = RequestMethod.POST)
    public String AddStudentsToGroups (@RequestParam String studentstoadd, @RequestParam String groupstoaddto, RedirectAttributes redirectAttributes) {
        try {
            studentService.AddStudentsToGroups(studentstoadd,groupstoaddto);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            redirectAttributes.addFlashAttribute("success", "Error! Student(s) not added to groups. Please try again.");
            return "redirect:/students";
        }
        redirectAttributes.addFlashAttribute("success", "Student(s) have been added successfully to the groups.");
        return "redirect:/students";
    }

    @RequestMapping(value="/addstudentstonewgroup", method = RequestMethod.POST)
    public String AddStudentsToNewGroups (@Valid @ModelAttribute("studentGroupAddForm") StudentGroupCreateForm studentGroupCreateForm, BindingResult bindingResult, @RequestParam String studentstoaddtonewgroup, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the details");
            return "redirect:/students";
        }
        try {
            studentService.AddStudentsToNewGroup(studentGroupCreateForm,studentstoaddtonewgroup);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            redirectAttributes.addFlashAttribute("success", "Error! Student(s) not added to new group. Please try again.");
            return "redirect:/students";
        }
        redirectAttributes.addFlashAttribute("success", "Student(s) have been added successfully to the new group.");
        return "redirect:/students";
    }

    @RequestMapping(value="/movestudentstogroups", method = RequestMethod.POST)
    public String MoveStudentsToGroups (@RequestParam String studentstomove, @RequestParam String groupstomoveto, @RequestParam Long currentgroupmove, RedirectAttributes redirectAttributes) {

        try {
            studentService.MoveStudentsToGroups(studentstomove,groupstomoveto,currentgroupmove);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            redirectAttributes.addFlashAttribute("success", "Error! Student(s) not moved to groups. Please try again.");
            return "redirect:/students";
        }
        redirectAttributes.addFlashAttribute("success", "Student(s) have been moved successfully to the groups.");
        return "redirect:/students";
    }

    @RequestMapping(value="/movestudentstonewgroup", method = RequestMethod.POST)
    public String MoveStudentsToNewGroups (@Valid @ModelAttribute("studentGroupAddForm") StudentGroupCreateForm studentGroupCreateForm, Long currentgroupcreatemove ,BindingResult bindingResult, @RequestParam String studentstomovetonewgroup, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingmoveerrors", "Error! Please confirm the details");
            return "redirect:/students";
        }
        try {
            studentService.MoveStudentsToNewGroup(studentGroupCreateForm,studentstomovetonewgroup, currentgroupcreatemove);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            redirectAttributes.addFlashAttribute("success", "Error! Student(s) not moved to new group. Please try again.");
            return "redirect:/students";
        }
        redirectAttributes.addFlashAttribute("success", "Student(s) have been moved successfully to the new group.");
        return "redirect:/students";
    }

    @RequestMapping(value="/removestudentsfromgroup", method = RequestMethod.POST)
    public String RemoveStudentsFromGroup (@RequestParam String studentstoremove, @RequestParam Long currentgroupremove, RedirectAttributes redirectAttributes) {
        try {
            studentService.RemoveStudentFromGroup(studentstoremove,currentgroupremove);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            redirectAttributes.addFlashAttribute("success", "Error! Student(s) not removed from group. Please try again.");
            return "redirect:/students";
        }
        redirectAttributes.addFlashAttribute("success", "Student(s) have been removed successfully from the group.");
        return "redirect:/students";
    }


    @RequestMapping(value = "/newstudent", method = RequestMethod.GET)
    public String addNewStudent(Model model, Authentication authentication, @RequestParam("selected") String selected) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();

        model.addAttribute("studentForm", new StudentCreateForm());
        model.addAttribute("studentGroupAddForm", new StudentGroupCreateForm());
        model.addAttribute("studentGroups", schoolService.getSchoolById(currentUser.getUser().getSchoolid()).get().getStudentGroupSet());

        if(selected.equals("upload")){
            model.addAttribute("activeClass", "upload");
        } else if(selected.equals("single")){
            model.addAttribute("activeClass", "single");
        }

        return "newstudent";
    }

    @RequestMapping(value = "/addstudenttogroup", method = RequestMethod.POST)
    public String handleStudentCreateForm(@Valid @ModelAttribute("studentGroupAddForm") StudentGroupAddForm studentGroupAddForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            model.addAttribute("bindingadderrors", "Error! Please confirm the details");
            model.addAttribute("studentCount", studentService.count(studentGroupAddForm.getSchoolid()));
            return "students";
        }

        try {
            Student student;
            StudentCreateForm studentForm = new StudentCreateForm();

            Optional<Student> studentOptional = studentService.getStudentbyAdmission(studentGroupAddForm.getSchoolid(), studentGroupAddForm.getAdmission());
            if (studentOptional.isPresent()) {
                studentService.AddStudentToGroup(studentGroupAddForm);
            } else {
                studentForm.setAdmission(studentGroupAddForm.getAdmission());
                studentForm.setStudentName(studentGroupAddForm.getStudentName());
                studentForm.setSchoolid(studentGroupAddForm.getSchoolid());
                student = studentService.create(studentForm);
                studentService.AddStudentToGroup(studentGroupAddForm);

            }

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            bindingResult.reject("student.error", "An Error Occurred while adding the student");
            model.addAttribute("bindingadderrors", "Error! Please confirm the details");
            model.addAttribute("studentCount", studentService.count(studentGroupAddForm.getSchoolid()));
            return "students";
        }
        redirectAttributes.addFlashAttribute("success", "Student has been added successfully");
        return "redirect:/students";
    }

    @RequestMapping(value = "/searchstudent", method = RequestMethod.POST)
    public String seachStudent(@RequestParam("admissionNo") String admissionNo, @RequestParam("schoolid") Long schoolid, RedirectAttributes redirectAttributes, Model model) {

        Optional<Student> student = studentService.getStudentbyAdmission(schoolid, admissionNo);

        if (student.isPresent()) {
            ArrayList<Student> students = new ArrayList<Student>();
            students.add(student.get());
            model.addAttribute("students", students);
            model.addAttribute("studentCount", studentService.count(schoolid));
            model.addAttribute("studentForm", new StudentCreateForm());
            model.addAttribute("studentGroupAddForm", new StudentGroupCreateForm());
            model.addAttribute("studentGroups", schoolService.getSchoolById(schoolid).get().getStudentGroupSet());
            model.addAttribute("selectedGroup", "all_students");
            return "students";
        } else {
            redirectAttributes.addFlashAttribute("success", "No student found");
            return "redirect:/students";
        }
    }

    @RequestMapping(value = "/addexam", method = RequestMethod.POST)
    public String handleExamCreateForm(@Valid @ModelAttribute("examForm") ExamCreateForm examForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            model.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            model.addAttribute("examCount", examService.count(examForm.getSchoolid()));
            return "results";
        }

        try {
            LOGGER.debug("Processing exam create form = {}", examForm);

            Exam exam = examService.create(examForm);

            LOGGER.debug("exam created = {}", exam);
        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            bindingResult.reject("exam.error", "An Error Occurred while adding the exam");
            model.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            model.addAttribute("examCount", examService.count(examForm.getSchoolid()));
            return "results";
        }
        redirectAttributes.addFlashAttribute("success", "Exam has been added successfully");
        return "redirect:/results";
    }

    @RequestMapping(value = "/exam", method = RequestMethod.GET)
    public String seachExam(@RequestParam("admissionNo") String admissionNo, @RequestParam("examid") Long examid, Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();

        Optional<Exam> exam = examService.getExambyId(examid);

        if (exam.isPresent()) {
            model.addAttribute("exam", exam.get());
        } else {
            model.addAttribute("noexam", "No Exam with the specified number was found");
        }

        return "exam";
    }

    @RequestMapping(value = "/schoolperiod")
    public String getSchoolPeriodsPage(Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        model.addAttribute("periodCount", schoolPeriodService.count(currentUser.getUser().getSchoolid()));
        model.addAttribute("schoolPeriodForm", new SchoolPeriodForm());
        model.addAttribute("schoolPeriods", schoolPeriodService.getAllSchoolPeriods(currentUser.getUser().getSchoolid()));
        return "schoolperiod";
    }

    @RequestMapping(value = "/addschoolperiod", method = RequestMethod.POST)
    public String handleSchoolPeriodCreateForm(@Valid @ModelAttribute("schoolPeriodForm") SchoolPeriodForm schoolPeriodForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();

        if (bindingResult.hasErrors()) {
            // failed validation
            model.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            model.addAttribute("periodCount", schoolPeriodService.count(schoolPeriodForm.getSchoolid()));
            return "schoolperiod";
        }

        try {
            LOGGER.debug("Processing school period create form = {}", schoolPeriodForm);

            SchoolPeriod schoolPeriod = schoolPeriodService.create(schoolPeriodForm);

            LOGGER.debug("SchoolPeriod created = {}", schoolPeriod);
        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school period", e);
            bindingResult.reject("schoolPeriod.error", "An Error Occurred while adding the School Academic Period");
            model.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            model.addAttribute("periodCount", schoolPeriodService.count(schoolPeriodForm.getSchoolid()));
            return "schoolperiod";
        }
        redirectAttributes.addFlashAttribute("success", "School Academic Period has been added successfully");
        return "redirect:/schoolperiod";
    }

    @RequestMapping(value = "/schoollevel")
    public String getSchoolLevelPage(Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        model.addAttribute("levelCount", schoolLevelService.count(currentUser.getUser().getSchoolid()));
        model.addAttribute("schoolLevelForm", new SchoolLevelForm());
        model.addAttribute("schoolLevels", schoolLevelService.getAllSchoolLevels(currentUser.getUser().getSchoolid()));
        return "schoollevel";
    }

    @RequestMapping(value = "/addschoollevel", method = RequestMethod.POST)
    public String handleSchoolLevelCreateForm(@Valid @ModelAttribute("schoolLevelForm") SchoolLevelForm schoolLevelForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            model.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            model.addAttribute("levelCount", schoolLevelService.count(schoolLevelForm.getSchoolid()));
            return "schoollevel";
        }

        try {
            LOGGER.debug("Processing school level create form = {}", schoolLevelForm);

            SchoolLevel schoolLevel = schoolLevelService.create(schoolLevelForm);

            LOGGER.debug("SchoolLevel created = {}", schoolLevel);
        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school level", e);
            bindingResult.reject("schoolLevel.error", "An Error Occurred while adding the School Class Level");
            model.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            model.addAttribute("levelCount", schoolLevelService.count(schoolLevelForm.getSchoolid()));
            return "schoollevel";
        }
        redirectAttributes.addFlashAttribute("success", "School Class Level has been added successfully");
        return "redirect:/schoollevel";
    }

    @RequestMapping(value = "/selectedschoollevel", method = RequestMethod.POST)
    public String handleSelectedSchoolLevelForm(@Valid @ModelAttribute("selectedSchoolLevelForm") SelectedSchoolLevelForm selectedSchoolLevelForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        Long schoolid = currentUser.getUser().getSchoolid();
        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/fees";
        }

        try {
            LOGGER.debug("Processing school level create form = {}", selectedSchoolLevelForm);

            int schoolLevelCount = schoolLevelService.count(schoolid);
            int feeItemCount = schoolFeesService.countFeeItems(schoolid);

            SchoolLevel schoolLevel = schoolLevelService.find(selectedSchoolLevelForm.getSelectedSchoolLevelid());
            Collection<SchoolLevel> schoolLevels = schoolLevelService.getAllSchoolLevels(schoolid);
            Collection<SchoolLevelPeriodTotal> periodTotals = schoolFeesService.getPeriodTotalBySchoolLevel(schoolLevel.getSchoollevelid());

            Iterator iterator = periodTotals.iterator();
            Map<Long, String> periodAmountMap = new HashMap<Long, String>();

            while (iterator.hasNext()) {
                SchoolLevelPeriodTotal total = (SchoolLevelPeriodTotal) iterator.next();
                periodAmountMap.put(total.getSchoolperiodid(), total.getAmount());
            }


            Collection<FeeItem> feeItems = schoolFeesService.getFeeItems(schoolid);

            if (feeItemCount > 0) {

                Iterator feeItemIterator = feeItems.iterator();
                Map<Long, Map<Long, String>> feeItemsAmount = new HashMap<Long, Map<Long, String>>();
                while (feeItemIterator.hasNext()) {
                    FeeItem feeItem = (FeeItem) feeItemIterator.next();
                    Collection<FeeItemAmount> feeItemAmounts = schoolFeesService.getFeeAmountByItemandSchoolLevel(feeItem.getFeeitemid(), schoolLevel.getSchoollevelid());

                    Iterator i = feeItemAmounts.iterator();
                    Map<Long, String> itemPeriodAmount = new HashMap<Long, String>();
                    while (i.hasNext()) {
                        FeeItemAmount feeItemAmount = (FeeItemAmount) i.next();
                        itemPeriodAmount.put(feeItemAmount.getSchoolperiodid(), feeItemAmount.getAmount());
                    }
                    feeItemsAmount.put(feeItem.getFeeitemid(), itemPeriodAmount);

                }

                model.addAttribute("feeItemsAmount", feeItemsAmount);
            }
            model.addAttribute("feeItems", feeItems);
            model.addAttribute("schoolLevels", schoolLevels);
            model.addAttribute("selectedSchoolLevel", schoolLevel);
            model.addAttribute("periodTotals", periodAmountMap);

            model.addAttribute("levelCount", schoolLevelCount);
            model.addAttribute("periodCount", schoolPeriodService.count(schoolid));
            model.addAttribute("schoolPeriods", schoolPeriodService.getAllSchoolPeriods(schoolid));
            model.addAttribute("feeItemCount", feeItemCount);
            model.addAttribute("feeItemForm", new FeeItemForm());
            model.addAttribute("feeItemAmountForm", new FeeItemAmountForm());
            model.addAttribute("schoolLevelPeriodTotalForm", new SchoolLevelPeriodTotalForm());
            model.addAttribute("selectedSchoolLevelForm", new SelectedSchoolLevelForm());


        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school level", e);
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/fees";
        }

        return "fees";
    }


    @RequestMapping(value = "/fees")
    public String getFeesPage(Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        Long schoolid = currentUser.getUser().getSchoolid();

        int schoolLevelCount = schoolLevelService.count(schoolid);
        int feeItemCount = schoolFeesService.countFeeItems(schoolid);

        if (schoolLevelCount > 0) {
            Collection<SchoolLevel> schoolLevels = schoolLevelService.getAllSchoolLevels(schoolid);
            SchoolLevel selectedLevel = schoolLevels.iterator().next();
            Collection<SchoolLevelPeriodTotal> periodTotals = schoolFeesService.getPeriodTotalBySchoolLevel(selectedLevel.getSchoollevelid());

            Iterator iterator = periodTotals.iterator();
            Map<Long, String> periodAmountMap = new HashMap<Long, String>();
            while (iterator.hasNext()) {
                SchoolLevelPeriodTotal total = (SchoolLevelPeriodTotal) iterator.next();
                periodAmountMap.put(total.getSchoolperiodid(), total.getAmount());
            }

            Collection<FeeItem> feeItems = schoolFeesService.getFeeItems(schoolid);

            if (feeItemCount > 0) {

                Iterator feeItemIterator = feeItems.iterator();
                Map<Long, Map<Long, String>> feeItemsAmount = new HashMap<Long, Map<Long, String>>();
                while (feeItemIterator.hasNext()) {
                    FeeItem feeItem = (FeeItem) feeItemIterator.next();
                    Collection<FeeItemAmount> feeItemAmounts = schoolFeesService.getFeeAmountByItemandSchoolLevel(feeItem.getFeeitemid(), selectedLevel.getSchoollevelid());

                    Iterator i = feeItemAmounts.iterator();
                    Map<Long, String> itemPeriodAmount = new HashMap<Long, String>();
                    while (i.hasNext()) {
                        FeeItemAmount feeItemAmount = (FeeItemAmount) i.next();
                        itemPeriodAmount.put(feeItemAmount.getSchoolperiodid(), feeItemAmount.getAmount());
                    }
                    feeItemsAmount.put(feeItem.getFeeitemid(), itemPeriodAmount);

                }

                model.addAttribute("feeItemsAmount", feeItemsAmount);
            }
            model.addAttribute("feeItems", feeItems);
            model.addAttribute("schoolLevels", schoolLevels);
            model.addAttribute("selectedSchoolLevel", selectedLevel);
            model.addAttribute("periodTotals", periodAmountMap);

        }


        model.addAttribute("levelCount", schoolLevelCount);
        model.addAttribute("periodCount", schoolPeriodService.count(schoolid));
        model.addAttribute("schoolPeriods", schoolPeriodService.getAllSchoolPeriods(schoolid));
        model.addAttribute("feeItemCount", feeItemCount);
        model.addAttribute("feeItemForm", new FeeItemForm());
        model.addAttribute("feeItemAmountForm", new FeeItemAmountForm());
        model.addAttribute("schoolLevelPeriodTotalForm", new SchoolLevelPeriodTotalForm());
        model.addAttribute("selectedSchoolLevelForm", new SelectedSchoolLevelForm());
        return "fees";
    }

    @RequestMapping(value = "/addfeeitem", method = RequestMethod.POST)
    public String handleSchoolLevelCreateForm(@Valid @ModelAttribute("feeItemForm") FeeItemForm feeItemForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        Long schoolid = currentUser.getUser().getSchoolid();
        if (bindingResult.hasErrors()) {
            // failed validation
            model.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/fees";
        }

        try {
            LOGGER.debug("Processing Fee Item create form = {}", feeItemForm);

            FeeItem feeItem = schoolFeesService.createFeeItem(feeItemForm);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the fee item", e);
            bindingResult.reject("schoolLevel.error", "An Error Occurred while adding the fee breakdown item");
            redirectAttributes.addAttribute("bindingadderrors", "Error! Please confirm the input details");

            return "redirect:/fees";
        }
        redirectAttributes.addFlashAttribute("success", "fee breakdown item has been added successfully");
        return "redirect:/fees";
    }

    @RequestMapping(value = "/addschoollevelperiodtotal", method = RequestMethod.POST)
    public String handleSchoolLevelCreateForm(@Valid @ModelAttribute("schoolLevelPeriodTotalForm") SchoolLevelPeriodTotalForm schoolLevelPeriodTotalForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        Long schoolid = currentUser.getUser().getSchoolid();

        if (bindingResult.hasErrors()) {
            // failed validation
            model.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/fees";
        }

        try {
            LOGGER.debug("Processing Fee Item create form = {}", schoolLevelPeriodTotalForm);

            Optional<SchoolLevelPeriodTotal> schoolLevelPeriodTotal = schoolFeesService.getSchoolLevelPeriodTotal(schoolLevelPeriodTotalForm.getSchoolperiodid(), schoolLevelPeriodTotalForm.getSchoollevelid());

            if (schoolLevelPeriodTotal.isPresent()) {
                schoolFeesService.UpdateSchoolLevelPeriodTotal(schoolLevelPeriodTotal.get().getSchoollevelperiodtotalid(), schoolLevelPeriodTotalForm.getAmount());
            } else {
                SchoolLevelPeriodTotal schoolLevelPeriodTotalNew = schoolFeesService.createSchoolLevelPeriodTotal(schoolLevelPeriodTotalForm);
                LOGGER.debug("SchoolLevelPeriodTotal created = {}", schoolLevelPeriodTotal);
            }


        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the fee item", e);
            bindingResult.reject("schoolLevel.error", "An Error Occurred while adding the fee breakdown item");
            redirectAttributes.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/fees";
        }
        redirectAttributes.addFlashAttribute("success", "Amount added successfully");
        return "redirect:/fees";
    }

    @RequestMapping(value = "/addfeeitemamount", method = RequestMethod.POST)
    public String handleSchoolLevelCreateForm(@Valid @ModelAttribute("feeItemAmountAmountForm") FeeItemAmountForm feeItemAmountForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model, Authentication authentication) {

        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        Long schoolid = currentUser.getUser().getSchoolid();
        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/fees";
        }

        try {
            LOGGER.debug("Processing Fee Item create form = {}", feeItemAmountForm);

            Optional<FeeItemAmount> feeItemAmount = schoolFeesService.getFeeItemAmount(feeItemAmountForm.getFeeitemid(), feeItemAmountForm.getSchoolperiodid(), feeItemAmountForm.getSchoollevelid());

            if (feeItemAmount.isPresent()) {
                schoolFeesService.UpdateFeeItemAmount(feeItemAmount.get().getFeeitemamountid(), feeItemAmountForm.getAmount());
            } else {
                FeeItemAmount feeItemAmountNew = schoolFeesService.createFeeItemAmount(feeItemAmountForm);
                LOGGER.debug("FeeItemAmount created = {}", feeItemAmount);
            }


        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the fee item", e);
            bindingResult.reject("schoolLevel.error", "An Error Occurred while adding the fee breakdown item");
            redirectAttributes.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/fees";
        }
        redirectAttributes.addFlashAttribute("success", "Amount added successfully");
        return "redirect:/fees";
    }

    @RequestMapping(value = "/schoolprofile", method = RequestMethod.GET)
    public String getSchoolBioPage(Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        Optional<School> school = schoolService.getSchoolById(currentUser.getUser().getSchoolid());
        Collection<SchoolType> schoolTypes = schoolService.findAllSchoolCategories();

        if (school.isPresent()) {
            model.addAttribute("school", school.get());
        }
        model.addAttribute("schoolBioForm", new SchoolBioForm());
        model.addAttribute("schoolTypes", schoolTypes);
        model.addAttribute("schoolTypeAddForm", new SchoolTypeAddForm());
        model.addAttribute("phoneNumberAddForm", new PhoneNumberAddForm());
        model.addAttribute("emailAddForm", new EmailAddForm());
        return "schoolprofile";
    }

    @RequestMapping(value = "/schoolprofile", method = RequestMethod.POST)
    public String handleSchoolBioCreateForm(@Valid @ModelAttribute("schoolBioForm") SchoolBioForm schoolBioForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model, Authentication authentication) {

        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        Long schoolid = currentUser.getUser().getSchoolid();
        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/schoolprofile";
        }

        try {
            LOGGER.debug("Processing School Bio create form = {}", schoolBioForm);
            schoolService.updateSchoolBio(schoolBioForm);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save school profile", e);
            bindingResult.reject("school.error", "An Error Occurred while updating the school profile");
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/schoolprofile";
        }
        redirectAttributes.addFlashAttribute("success", "School Profile updated successfully");
        return "redirect:/schoolprofile";
    }


    @RequestMapping(value = "/messages")
    public String getAdmissionPage(Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        model.addAttribute("dmForm", new DirectMessageSchoolForm());
        //model.addAttribute("directMessages",  );
        model.addAttribute("admissionRequests", schoolService.getAdmissionsBySchoolId(currentUser.getUser().getSchoolid()));
        return "admission";
    }

    @RequestMapping(value = "/phonenumberadd", method = RequestMethod.POST)
    public String addSchoolPhoneNumber(@Valid @ModelAttribute("phoneNumberAddForm") PhoneNumberAddForm phoneNumberAddForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        Long schoolid = currentUser.getUser().getSchoolid();

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/schoolprofile";
        }

        try {
            LOGGER.debug("processing phone Number Add Form = {)", phoneNumberAddForm);
            schoolService.addPhoneNumber(phoneNumberAddForm);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to add the phone number ", e);
            bindingResult.reject("school.error", "An Error Occurred while adding the phone number");
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/schoolprofile";
        }
        redirectAttributes.addFlashAttribute("success", "Phone Number added successfully");
        return "redirect:/schoolprofile";

    }

    @RequestMapping(value = "/emailadd", method = RequestMethod.POST)
    public String addSchoolEmail(@Valid @ModelAttribute("emailAddForm") EmailAddForm emailAddForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        Long schoolid = currentUser.getUser().getSchoolid();

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/schoolprofile";
        }

        try {
            LOGGER.debug("processing Email Add Form = {)", emailAddForm);
            schoolService.addEmail(emailAddForm);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to add the email ", e);
            bindingResult.reject("school.error", "An Error Occurred while adding the email ");
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/schoolprofile";
        }
        redirectAttributes.addFlashAttribute("success", "Email added successfully");
        return "redirect:/schoolprofile";

    }

    @RequestMapping(value = "/schooltypeadd", method = RequestMethod.POST)
    public String addSchoolPhoneNumber(@Valid @ModelAttribute("schoolTypeAddForm") SchoolTypeAddForm schoolTypeAddForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        Long schoolid = currentUser.getUser().getSchoolid();

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/schoolprofile";
        }

        try {
            LOGGER.debug("processing School Type Add Form = {)", schoolTypeAddForm);
            schoolService.addSchoolType(schoolTypeAddForm);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to add the school category ", e);
            bindingResult.reject("school.error", "An Error Occurred while adding the school category");
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/schoolprofile";
        }
        redirectAttributes.addFlashAttribute("success", "School Category added successfully");
        return "redirect:/schoolprofile";

    }


    @RequestMapping(value = "/studentgroup")
    public String getStudentGroupPage(Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
        Optional<School> school = schoolService.getSchoolById(currentUser.getUser().getSchoolid());
        if (school.isPresent()) {
            model.addAttribute("groupCount", school.get().getStudentGroupSet().size());
            model.addAttribute("studentGroupCreateForm", new StudentGroupCreateForm());
            model.addAttribute("studentGroups", school.get().getStudentGroupSet());
            return "studentgroup";
        } else {
            model.addAttribute("groupCount", 0);
            model.addAttribute("studentGroupCreateForm", new StudentGroupCreateForm());
            model.addAttribute("studentGroups", null);
            return "studentgroup";
        }

    }

    @RequestMapping(value = "/newstudentgroup")
    public String getAddStudentGroupPage(Model model, Authentication authentication) {
            model.addAttribute("studentGroupCreateForm", new StudentGroupCreateForm());
            return "newstudentgroup";
    }

    @RequestMapping(value = "/addstudentgroup", method = RequestMethod.POST)
    public String handleSchoolGroupCreateForm(@Valid @ModelAttribute("studentGroupCreateForm") StudentGroupCreateForm studentGroupCreateForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {
        Optional<School> school = schoolService.getSchoolById(studentGroupCreateForm.getSchoolid());

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/newstudentgroup";
        }
        try {

            StudentGroup studentGroup = studentService.createStudentGroup(studentGroupCreateForm);

            LOGGER.debug("SchoolLevel created = {}", studentGroup);
        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the student group", e);
            bindingResult.reject("studentGroup.error", "An Error Occurred while adding the student group");
            redirectAttributes.addAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/newstudentgroup";
        }
        redirectAttributes.addFlashAttribute("success", "Student Communication Group has been added successfully");
        return "redirect:/newstudentgroup";
    }

    @RequestMapping(value = "/editstudentgroup", method = RequestMethod.POST)
    public String EditStudentGroup(@Valid @ModelAttribute("studentGroupCreateForm") StudentGroupCreateForm studentGroupCreateForm, BindingResult bindingResult, RedirectAttributes redirectAttributes){
        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/studentgroup";
        }
        try{
            studentService.editStudentGroup(studentGroupCreateForm);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the student group", e);
            redirectAttributes.addFlashAttribute("success", "Student group not edited. Please try again");
            return "redirect:/studentgroup";
        }
        redirectAttributes.addFlashAttribute("success", "Student Communication Group has been edited successfully");
        return "redirect:/studentgroup";

    }

    @RequestMapping(value = "/deletestudentgroup", method = RequestMethod.POST)
    public String DeleteStudentGroup( @RequestParam("studentgroupid") Long studentgroupid,  RedirectAttributes redirectAttributes){


        try{
            studentService.deleteStudentGroup(studentgroupid);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the student group", e);
            redirectAttributes.addFlashAttribute("success", "Student group not deleted. Please try again");
            return "redirect:/studentgroup";
        }
        redirectAttributes.addFlashAttribute("success", "Student Communication Group has been deleted successfully");
        return "redirect:/studentgroup";

    }


    @RequestMapping(value = "/parents", method = RequestMethod.GET)
    public String getParentsPage(Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();

        model.addAttribute("parentsCount", studentService.SubscriberCount(currentUser.getUser().getSchoolid()));
        model.addAttribute("parentAddForm", new ParentAddForm());
        return "parents";

    }

    @RequestMapping(value = "/newparent", method = RequestMethod.GET)
    public String addNewParent(Model model, Authentication authentication, @RequestParam("selected") String selected) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();

        model.addAttribute("parentAddForm", new ParentAddForm());

        if(selected.equals("upload")){
            model.addAttribute("activeClass", "upload");
        } else if(selected.equals("single")){
            model.addAttribute("activeClass", "single");
        }

        return "newparent";
    }

    @RequestMapping(value = "/addparenttostudent", method = RequestMethod.POST)
    public String handleSchoolLevelCreateForm(@Valid @ModelAttribute("parentAddForm") ParentAddForm parentAddForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("success", "parent not added. please try again");
            return "redirect:/students";
        }

        try {
            Subscriber subscriber = studentService.createSubscriber(parentAddForm);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the parent contact", e);
            bindingResult.reject("parents.error", "An Error Occurred while adding the Parent");
            redirectAttributes.addFlashAttribute("success", "parent not added. please try again");
            return "redirect:/students";
        }
        redirectAttributes.addFlashAttribute("success", "Parent has been added successfully");
        return "redirect:/students";
    }

    @RequestMapping(value = "/addparent", method = RequestMethod.POST)
    public String HandleParentCreateForm(@Valid @ModelAttribute("parentAddForm") ParentAddForm parentAddForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            redirectAttributes.addAttribute("selected", "single");
            return "redirect:/newparent";
        }

        try {
            Subscriber subscriber = studentService.createSubscriber(parentAddForm);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the parent contact", e);
            bindingResult.reject("parents.error", "An Error Occurred while adding the Parent");
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            redirectAttributes.addAttribute("selected", "single");
            return "redirect:/newparent";
        }
        redirectAttributes.addFlashAttribute("success", "Parent has been added successfully");
        redirectAttributes.addAttribute("selected", "single");
        return "redirect:/newparent";
    }

    @RequestMapping(value = "/editparent", method = RequestMethod.POST)
    public String handleParentEditForm(@Valid @ModelAttribute("parentEditForm") ParentEditForm parentEditForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the details");
            return "redirect:/students";
        }

        try {
            studentService.editSubscriber(parentEditForm);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            bindingResult.reject("student.error", "An Error Occurred while editing the student");
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the details");
            return "redirect:/students";
        }
        redirectAttributes.addFlashAttribute("success", "Student has been edited successfully");
        return "redirect:/students";
    }

    @RequestMapping(value="/deleteparent", method = RequestMethod.POST)
    public String deleteParent (@RequestParam Long studentid, @RequestParam Long parenttodelete, RedirectAttributes redirectAttributes) {
        try {
            studentService.deleteSubscriber(studentid,parenttodelete);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the school", e);
            redirectAttributes.addFlashAttribute("success", "Error! Parent not deleted. Please try again.");
            return "redirect:/students";
        }
        redirectAttributes.addFlashAttribute("success", "Parent has been deleted successfully");
        return "redirect:/students";
    }

    @RequestMapping(value = "/addsmscontactgroup", method = RequestMethod.POST)
    public String handleSchoolLevelCreateForm(@Valid @ModelAttribute("smsContactGroupAddForm") SmsContactGroupCreateForm form, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/bulksms";
        }

        try {
            SmsContactGroup smsContactGroup = smsContactService.createSmsContactGroup(form);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the contact group", e);
            bindingResult.reject("parents.error", "An Error Occurred while adding the contact group");
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/bulksms";
        }
        redirectAttributes.addFlashAttribute("success", "SMS Contact Group has been added successfully");
        return "redirect:/bulksms";
    }

    @RequestMapping(value = "/addsmscontact", method = RequestMethod.POST)
    public String handleSchoolLevelCreateForm(@Valid @ModelAttribute("smsContactAddForm") SmsContactCreateForm form, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/bulksms";
        }

        try {
            SmsContact smsContact = smsContactService.createSmsContact(form);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to save the contact", e);
            bindingResult.reject("parents.error", "An Error Occurred while adding the contact");
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/bulksms";
        }
        redirectAttributes.addFlashAttribute("success", "SMS Contact has been added successfully");
        return "redirect:/bulksms";
    }

    @RequestMapping(value = "/bulksms", method = RequestMethod.GET)
    public String getBulkSmsPage(@RequestParam(value = "selectedgroup", required = false) String selectedGroup, Model model, Authentication authentication) {
        CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();

        if (selectedGroup == null) {

            model.addAttribute("smsContacts", smsContactService.findSmsContactBySchool(currentUser.getUser().getSchoolid()));
            model.addAttribute("smsContactCount", smsContactService.smsContactCount(currentUser.getUser().getSchoolid()));
            model.addAttribute("smsContactGroupCount", smsContactService.smsContactGroupCount(currentUser.getUser().getSchoolid()));
            model.addAttribute("smsContactAddForm", new SmsContactCreateForm());
            model.addAttribute("smsContactGroupAddForm", new SmsContactGroupCreateForm());
            model.addAttribute("smsMessageForm", new SmsMessageForm());
            model.addAttribute("smsContactGroups", smsContactService.findSmsContactGroupsBySchool(currentUser.getUser().getSchoolid()));
            model.addAttribute("studentGroups", schoolService.getSchoolById(currentUser.getUser().getSchoolid()).get().getStudentGroupSet());
            model.addAttribute("selectedGroup", "all_contacts");
            return "bulksms";
        } else {

            SmsContactGroup smsContactGroup = smsContactService.findSmsContactGroup(Long.parseLong(selectedGroup));
            if (smsContactGroup != null) {
                model.addAttribute("smsContacts", smsContactGroup.getSmsContactSet());
            } else {
                model.addAttribute("smsContacts", new ArrayList<SmsContactGroup>());
            }
            model.addAttribute("smsContactCount", smsContactService.smsContactCount(currentUser.getUser().getSchoolid()));
            model.addAttribute("smsContactGroupCount", smsContactService.smsContactGroupCount(currentUser.getUser().getSchoolid()));
            model.addAttribute("smsContactAddForm", new SmsContactCreateForm());
            model.addAttribute("smsContactGroupAddForm", new SmsContactGroupCreateForm());
            model.addAttribute("smsMessageForm", new SmsMessageForm());
            model.addAttribute("smsContactGroups", smsContactService.findSmsContactGroupsBySchool(currentUser.getUser().getSchoolid()));
            model.addAttribute("studentGroups", schoolService.getSchoolById(currentUser.getUser().getSchoolid()).get().getStudentGroupSet());
            model.addAttribute("selectedGroup", Long.parseLong(selectedGroup));
            return "bulksms";
        }

    }

    @RequestMapping(value = "/sendtoparents", method = RequestMethod.POST)
    public String SendtoParents(@Valid @ModelAttribute("smsMessageForm") SmsMessageForm form, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/bulksms";
        }

        try {
             smsService.processParentSMS(form);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to send the message", e);
            bindingResult.reject("parents.error", "An Error Occurred while sending the message");
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/bulksms";
        }
        redirectAttributes.addFlashAttribute("success", "Message has been sent successfully");
        return "redirect:/bulksms";
    }

    @RequestMapping(value = "/sendtocontactgroups", method = RequestMethod.POST)
    public String SendtoContactGroups(@Valid @ModelAttribute("smsMessageForm") SmsMessageForm form, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/bulksms";
        }

        try {
            smsService.processContactGroupSMS(form);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to send the message", e);
            bindingResult.reject("parents.error", "An Error Occurred while sending the message");
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/bulksms";
        }
        redirectAttributes.addFlashAttribute("success", "Message has been sent successfully");
        return "redirect:/bulksms";
    }

    @RequestMapping(value = "/sendtoothers", method = RequestMethod.POST)
    public String SendtoOthers(@Valid @ModelAttribute("smsMessageForm") SmsMessageForm form, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            // failed validation
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/bulksms";
        }

        try {
            smsService.processOtherSMS(form);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to send the message", e);
            bindingResult.reject("parents.error", "An Error Occurred while sending the parent message");
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/bulksms";
        }
        redirectAttributes.addFlashAttribute("success", "Message has been sent successfully");
        return "redirect:/bulksms";
    }

    @RequestMapping(value = "/senddirectsmsfromstudents", method = RequestMethod.POST)
    public String SendDirectSMSFromStudents(@RequestParam String studentstosendmessageto, @RequestParam String textmessage, RedirectAttributes redirectAttributes){
        try {
            smsService.processDirectSMSFromStudents(studentstosendmessageto, textmessage);

        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Exception occurred when trying to send the message", e);
            redirectAttributes.addFlashAttribute("bindingadderrors", "Error! Please confirm the input details");
            return "redirect:/students";
        }
        redirectAttributes.addFlashAttribute("success", "Message has been sent successfully");
        return "redirect:/students";
    }

    @RequestMapping(value = "/parentsperstudent", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetValidUntil(@RequestParam("student_id") String studentid) {
        Collection<SubscriberStudent> subscriberStudents = studentService.findByStudentid(Long.parseLong(studentid));
        List<ParentSubtable> parents = new ArrayList<ParentSubtable>();
        for (SubscriberStudent subscriberStudent : subscriberStudents){
            ParentSubtable parent = new ParentSubtable(subscriberStudent);
            parents.add(parent);
        }
        String json = new Gson().toJson(parents);
        String data = "{\"data\": "+ json +" }";
        return data;
    }

    @RequestMapping(value = "/sampledownload/{sample_name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource getFile(@PathVariable("sample_name") String sampleName) {
        return new FileSystemResource("/opt/shuledial/file_samples/" + sampleName + ".csv");
    }



}
