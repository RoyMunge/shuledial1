package org.sturgeon.sdial.sms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.model.*;
import org.sturgeon.sdial.common.model.forms.SmsMessageForm;
import org.sturgeon.sdial.common.repository.SmsMessageRepository;
import org.sturgeon.sdial.common.service.school.SmsContactService;
import org.sturgeon.sdial.common.service.school.StudentService;
import org.sturgeon.sdial.sms.handler.SMSSendService;
import org.sturgeon.sdial.sms.handler.SmsHandler;
import org.sturgeon.sdial.sms.utils.SmsMessageBag;
import org.sturgeon.sdial.sms.utils.SmsRequest;

import java.util.*;

/**
 * Created by User on 11/04/2017.
 */
@Service
public class SmsServiceImpl implements SmsService {

    SmsContactService smsContactService;
    StudentService studentService;
    SmsMessageRepository smsMessageRepository;
    SMSSendService smsSendService = new SMSSendService();
    SmsHandler smsHandler = new SmsHandler();

    @Autowired
    public SmsServiceImpl(SmsContactService smsContactService, SmsMessageRepository smsMessageRepository) {
        this.smsContactService = smsContactService;
        this.smsMessageRepository = smsMessageRepository;
    }

    @Override
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    @Override
    public void processParentSMS(SmsMessageForm form) {
        if (form.getSendtoAll()) {
            String phoneNumbers = new String();
            SmsMessage newSmsMessage = new SmsMessage();
            newSmsMessage.setDate(new Date());
            newSmsMessage.setMessageType("PARENTS");
            newSmsMessage.setRecipient("all_parents");
            newSmsMessage.setText(form.getText());
            newSmsMessage.setStatus("SUBMITTED");
            SmsMessage savedSmsMessage = smsMessageRepository.save(newSmsMessage);

            Collection<Student> students = studentService.getStudentsbySchool(form.getSchoolid());
            int count = 0;
            if (!students.isEmpty()) {
                for (Student student : students) {
                    Collection<SubscriberStudent> subscriberStudents = studentService.findByStudentid(student.getStudentid());
                    if (!subscriberStudents.isEmpty()) {
                        for (SubscriberStudent subscriberStudent : subscriberStudents) {
                            phoneNumbers = phoneNumbers + "," + subscriberStudent.getSubscriber().getPhoneNumber();
                            count++;
                            if (count == 99) {
                                SmsMessageBag smsMessageBag = new SmsMessageBag(savedSmsMessage.getText(), phoneNumbers.substring(1));
                                smsSendService.sendSMS(smsMessageBag);
                                phoneNumbers = "";
                                count = 0;
                            }
                        }
                    }
                }
            }

            SmsMessageBag smsMessageBag = new SmsMessageBag(savedSmsMessage.getText(), phoneNumbers.substring(1));
            smsSendService.sendSMS(smsMessageBag);

        } else {
            for (String recipient : form.getRecipients()) {
                String phoneNumbers = new String();
                SmsMessage newSmsMessage = new SmsMessage();
                newSmsMessage.setDate(new Date());
                newSmsMessage.setMessageType("PARENTS");
                newSmsMessage.setRecipientgroupid(Long.parseLong(recipient));
                newSmsMessage.setText(form.getText());
                newSmsMessage.setStatus("SUBMITTED");
                SmsMessage savedSmsMessage = smsMessageRepository.save(newSmsMessage);

                Collection<Student> students = studentService.getStudentsbyGroup(savedSmsMessage.getRecipientgroupid());
                int count = 0;
                if (!students.isEmpty()) {
                    for (Student student : students) {
                        Collection<SubscriberStudent> subscriberStudents = studentService.findByStudentid(student.getStudentid());
                        if (!subscriberStudents.isEmpty()) {
                            for (SubscriberStudent subscriberStudent : subscriberStudents) {
                                phoneNumbers = phoneNumbers + "," + subscriberStudent.getSubscriber().getPhoneNumber();
                                count++;
                                if (count == 99) {
                                    SmsMessageBag smsMessageBag = new SmsMessageBag(savedSmsMessage.getText(), phoneNumbers.substring(1));
                                    smsSendService.sendSMS(smsMessageBag);
                                    phoneNumbers = "";
                                    count = 0;
                                }
                            }
                        }
                    }
                }

                SmsMessageBag smsMessageBag = new SmsMessageBag(savedSmsMessage.getText(), phoneNumbers.substring(1));
                smsSendService.sendSMS(smsMessageBag);
            }

        }

    }

    @Override
    public void processContactGroupSMS(SmsMessageForm form) {
        if (form.getSendtoAll()) {
            String phoneNumbers = new String();
            SmsMessage newSmsMessage = new SmsMessage();
            newSmsMessage.setDate(new Date());
            newSmsMessage.setMessageType("CONTACTS");
            newSmsMessage.setRecipient("all_contacts");
            newSmsMessage.setText(form.getText());
            newSmsMessage.setStatus("SUBMITTED");
            SmsMessage savedSmsMessage = smsMessageRepository.save(newSmsMessage);

            Collection<SmsContact> contacts = smsContactService.findSmsContactBySchool(form.getSchoolid());
            int count = 0;
            for (SmsContact contact : contacts) {
                phoneNumbers = phoneNumbers + "," + contact.getPhoneNumber();
                count++;
                if (count == 99) {
                    SmsMessageBag smsMessageBag = new SmsMessageBag(savedSmsMessage.getText(), phoneNumbers.substring(1));
                    smsSendService.sendSMS(smsMessageBag);
                    phoneNumbers = "";
                    count = 0;
                }
            }

            SmsMessageBag smsMessageBag = new SmsMessageBag(savedSmsMessage.getText(), phoneNumbers.substring(1));
            smsSendService.sendSMS(smsMessageBag);

        } else {
            for (String recipient : form.getRecipients()) {
                String phoneNumbers = new String();
                SmsMessage newSmsMessage = new SmsMessage();
                newSmsMessage.setDate(new Date());
                newSmsMessage.setMessageType("CONTACTS");
                newSmsMessage.setRecipientgroupid(Long.parseLong(recipient));
                newSmsMessage.setText(form.getText());
                newSmsMessage.setStatus("SUBMITTED");
                SmsMessage savedSmsMessage = smsMessageRepository.save(newSmsMessage);

                SmsContactGroup contactGroup = smsContactService.findSmsContactGroup(Long.parseLong(recipient));
                int count = 0;
                for (SmsContact contact : contactGroup.getSmsContactSet()) {
                    phoneNumbers = phoneNumbers + "," + contact.getPhoneNumber();
                    count++;
                    if (count == 99) {
                        SmsMessageBag smsMessageBag = new SmsMessageBag(savedSmsMessage.getText(), phoneNumbers.substring(1));
                        smsSendService.sendSMS(smsMessageBag);
                        phoneNumbers = "";
                        count = 0;
                    }
                }

                SmsMessageBag smsMessageBag = new SmsMessageBag(savedSmsMessage.getText(), phoneNumbers.substring(1));
                smsSendService.sendSMS(smsMessageBag);

            }

        }
    }

    @Override
    public void processOtherSMS(SmsMessageForm form) {
        if (form.getOtherRecipients() != null) {
            List<String> recipients = new ArrayList<String>();
            recipients = Arrays.asList(form.getOtherRecipients().split("\\s*,\\s*"));
            for (String recipient : recipients) {
                recipient = processPhoneNumber(recipient);
                SmsMessage newSmsMessage = new SmsMessage();
                newSmsMessage.setDate(new Date());
                newSmsMessage.setMessageType("SINGLE_RECIPIENT");
                newSmsMessage.setRecipient(recipient);
                newSmsMessage.setText(form.getText());
                newSmsMessage.setStatus("SUBMITTED");
                SmsMessage savedSmsMessage = smsMessageRepository.save(newSmsMessage);

                SmsMessageBag smsMessageBag = new SmsMessageBag(savedSmsMessage.getText(), recipient);
                smsSendService.sendSMS(smsMessageBag);
            }
        }

    }

    @Override
    public void processDirectSMSFromStudents(String studentsids, String textMessage) {
        List<String> studentidList = Arrays.asList(studentsids.split("\\s*,\\s*"));

        String phoneNumbers = new String();
        int count = 0;

        if (!studentidList.isEmpty()) {

            SmsMessage newSmsMessage = new SmsMessage();
            newSmsMessage.setDate(new Date());
            newSmsMessage.setMessageType("DIRECT");
            newSmsMessage.setRecipient("direct");
            newSmsMessage.setText(textMessage);
            newSmsMessage.setStatus("SUBMITTED");
            SmsMessage savedSmsMessage = smsMessageRepository.save(newSmsMessage);

            for (String studentid : studentidList) {
                Collection<SubscriberStudent> subscriberStudents = studentService.findByStudentid(Long.parseLong(studentid));
                if (!subscriberStudents.isEmpty()) {
                    for (SubscriberStudent subscriberStudent : subscriberStudents) {
                        phoneNumbers = phoneNumbers + "," + subscriberStudent.getSubscriber().getPhoneNumber();
                        count++;
                        if (count == 99) {
                            SmsMessageBag smsMessageBag = new SmsMessageBag(savedSmsMessage.getText(), phoneNumbers.substring(1));
                            smsSendService.sendSMS(smsMessageBag);
                            phoneNumbers = "";
                            count = 0;
                        }
                    }
                }
            }

            SmsMessageBag smsMessageBag = new SmsMessageBag(savedSmsMessage.getText(), phoneNumbers.substring(1));
            smsSendService.sendSMS(smsMessageBag);
        }

    }

    private String processPhoneNumber(String phoneNumber) {
        String intlPhoneNumber;
        if (phoneNumber.startsWith("07")) {
            intlPhoneNumber = phoneNumber.replace("07", "2547");
            return intlPhoneNumber;
        } else if (phoneNumber.startsWith("+")) {
            intlPhoneNumber = phoneNumber.replace("+", "");
            return intlPhoneNumber;
        } else {
            return phoneNumber;
        }
    }
}
