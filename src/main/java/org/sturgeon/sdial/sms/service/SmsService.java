package org.sturgeon.sdial.sms.service;

import org.sturgeon.sdial.common.model.forms.SmsMessageForm;
import org.sturgeon.sdial.common.service.school.StudentService;

/**
 * Created by User on 11/04/2017.
 */
public interface SmsService {
    void processParentSMS(SmsMessageForm form);
    void processContactGroupSMS(SmsMessageForm form);
    void processOtherSMS(SmsMessageForm form);
    void processDirectSMSFromStudents(String studentsids, String textMessage);
    void setStudentService(StudentService studentService );

}
