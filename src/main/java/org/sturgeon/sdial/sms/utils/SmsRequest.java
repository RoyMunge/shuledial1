package org.sturgeon.sdial.sms.utils;

import java.util.ArrayList;

/**
 * Created by User on 11/04/2017.
 */
public class SmsRequest {
    private Long requestid;
    private String text;
    private ArrayList<String> recipients;

    public SmsRequest(Long requestid, String text, ArrayList<String> recipients) {
        this.requestid = requestid;
        this.text = text;
        this.recipients = recipients;
    }

    public Long getRequestid() {
        return requestid;
    }

    public void setRequestid(Long requestid) {
        this.requestid = requestid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ArrayList<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(ArrayList<String> recipients) {
        this.recipients = recipients;
    }
}
