package org.sturgeon.sdial.sms.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Roy on 7/25/2017.
 */
public class UjumbeSMSResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("status")
    private Status status;

    @JsonProperty("meta")
    private Meta meta;

    public UjumbeSMSResponse() {
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    private class Status implements Serializable {

        private static final long serialVersionUID = 1L;

        @JsonProperty("code")
        private String code;

        @JsonProperty("type")
        private String type;

        @JsonProperty("description")
        private String description;

        public Status() {
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    private class Meta implements Serializable{

        private static final long serialVersionUID = 1L;

        @JsonProperty("recipients")
        private Integer recipients;

        @JsonProperty("credits_deducted")
        private Integer credits_deducted;

        @JsonProperty("available_credits")
        private String available_credits;

        @JsonProperty("user")
        private String user;

        @JsonProperty("date_time")
        private DateTime date_time;

        public Meta() {
        }

        public Integer getRecipients() {
            return recipients;
        }

        public void setRecipients(Integer recipients) {
            this.recipients = recipients;
        }

        public Integer getCredits_deducted() {
            return credits_deducted;
        }

        public void setCredits_deducted(Integer credits_deducted) {
            this.credits_deducted = credits_deducted;
        }

        public String getAvailable_credits() {
            return available_credits;
        }

        public void setAvailable_credits(String available_credits) {
            this.available_credits = available_credits;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public DateTime getDate_time() {
            return date_time;
        }

        public void setDate_time(DateTime date_time) {
            this.date_time = date_time;
        }
    }

    public class DateTime implements Serializable{

        private static final long serialVersionUID = 1L;

        @JsonProperty("date")
        private String date;

        @JsonProperty("timezone_type")
        private Integer timezone_type;

        @JsonProperty("timezone")
        private String timezone;

        public DateTime() {
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Integer getTimezone_type() {
            return timezone_type;
        }

        public void setTimezone_type(Integer timezone_type) {
            this.timezone_type = timezone_type;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }
    }
}
