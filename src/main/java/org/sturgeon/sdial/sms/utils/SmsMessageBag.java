package org.sturgeon.sdial.sms.utils;

import java.util.ArrayList;

/**
 * Created by Roy on 7/25/2017.
 */
public class SmsMessageBag {
    private String sender;
    private String message;
    private String numbers;

    public SmsMessageBag(String message, String numbers) {
        this.sender = "ShuleDial";
        this.message = message;
        this.numbers = numbers;
    }

    public String getSender() {
        return sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNumbers() {
        return numbers;
    }

    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }
}
