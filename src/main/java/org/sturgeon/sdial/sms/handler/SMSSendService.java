package org.sturgeon.sdial.sms.handler;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.sturgeon.sdial.mobileapp.utils.HeaderRequestInterceptor;
import org.sturgeon.sdial.sms.utils.SmsMessageBag;
import org.sturgeon.sdial.sms.utils.UjumbeSMSResponse;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Created by Roy on 7/25/2017.
 */
public class SMSSendService {

    private static final String UJUMBE_SMS_AUTH_KEY = "YjQyOWNmMTYzZTQ4MzEzMzkxZTFjNmU3NWJkM2Mx";
    private static final String UJUMBE_SMS_API = "http://ujumbesms.co.ke/api/messaging";
    private static final String UJUMBE_SMS_EMAIL = "roywanyaga@gmail.com";


    public String sendSMS(SmsMessageBag smsMessageBag) {

        String jsonRequest = "{ \"data\":[ { \"message_bag\":" + new Gson().toJson(smsMessageBag) + " } ] }";

        System.out.println("the json string to send////////  " +  jsonRequest);

        HttpEntity<String> request = new HttpEntity<String>(jsonRequest);

        CompletableFuture<String> smsRequest = this.send(request);
        CompletableFuture.allOf(smsRequest).join();

        String smsResponse = null;

        try {
            smsResponse = smsRequest.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        return smsResponse;
    }

    @Async
    private CompletableFuture<String> send(HttpEntity<String> entity) {

        RestTemplate restTemplate = new RestTemplate();

        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
        interceptors.add(new HeaderRequestInterceptor("X-Authorization",  UJUMBE_SMS_AUTH_KEY));
        interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
        interceptors.add(new HeaderRequestInterceptor("email", UJUMBE_SMS_EMAIL));
        interceptors.add(new HeaderRequestInterceptor("Cache-Control", "no-cache"));
        interceptors.add(new HeaderRequestInterceptor("User-Agent", "Custom User-Agent"));

        restTemplate.setInterceptors(interceptors);
        String smsResponse = null;
        try{
            smsResponse = restTemplate.postForObject(UJUMBE_SMS_API, entity, String.class);
        } catch (RestClientException e){
            e.printStackTrace();
        }



        return CompletableFuture.completedFuture(smsResponse);
    }
}
