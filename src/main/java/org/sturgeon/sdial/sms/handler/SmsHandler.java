package org.sturgeon.sdial.sms.handler;

import com.google.gson.Gson;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.sturgeon.sdial.sms.utils.SmsRequest;

/**
 * Created by User on 11/04/2017.
 */
@Service
public class SmsHandler {

    @Async
    public void SendSms(SmsRequest smsRequest){

        final Logger LOGGER = LoggerFactory.getLogger(SmsHandler.class);

        try{

            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            String uri = new String("http://localhost/shuledial/sms.php");

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            String jsonRequest = new Gson().toJson(smsRequest);

            HttpEntity<String> entity = new HttpEntity<String>(jsonRequest, headers);

            //rt.put(uri,entity);

            ResponseEntity<String> response = rt.exchange(uri, HttpMethod.POST, entity, String.class);
           if (response.getStatusCode() == HttpStatus.OK) {
                //System.out.println("Sent Successfully " + response.getBody().toString() );
            } else if (response.getStatusCode() == HttpStatus.UNAUTHORIZED) {
               //System.out.println("Not Successfull " + response.getBody().toString() );
            }

        } catch (HttpClientErrorException e){
            LOGGER.error("error: " + e.getResponseBodyAsString());

        } catch(Exception e)
        {
            LOGGER.error("error: " + e.getMessage());
        }
    }
}
