package org.sturgeon.sdial;

/**
 * Created by Roy.Munge on 28/08/2015.
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application
{
    public static void main(String[] args)throws Exception {
        SpringApplication.run(Application.class, args);
    }
}
