package org.sturgeon.sdial.mobileapp.service;

import org.sturgeon.sdial.common.model.SchoolReferral;
import org.sturgeon.sdial.common.model.Subscriber;
import org.sturgeon.sdial.common.model.forms.*;

import java.util.Optional;

/**
 * Created by User on 13/05/2016.
 */
public interface SubscriberService {

    Optional<Subscriber> findSubscriberByPhoneNumber(String phoneNumber);

    Subscriber create(SubscriberCreateForm subscriberCreateForm);

    Optional<Subscriber> getUserByEmail(String userEmail);

    Optional<Subscriber> Authenticate(SubscriberLoginForm subscriberLoginForm);

    void registerDevice(String phoneNumber, String token);

    void requestSubscription(String studentid, String phoneNumber);

    void processPushNotification(PushNotificationForm form);

    SchoolReferral processSchoolReferral(SchoolReferralForm form);

}