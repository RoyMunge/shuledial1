package org.sturgeon.sdial.mobileapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.model.*;
import org.sturgeon.sdial.common.model.forms.*;
import org.sturgeon.sdial.common.repository.SchoolReferralRepository;
import org.sturgeon.sdial.common.repository.SubscriberRepository;
import org.sturgeon.sdial.common.repository.SubscriptionRequestRepository;
import org.sturgeon.sdial.common.service.school.SchoolService;
import org.sturgeon.sdial.common.service.school.SchoolServiceImpl;
import org.sturgeon.sdial.common.service.school.StudentService;
import org.sturgeon.sdial.mobileapp.utils.JCMNotification;
import org.sturgeon.sdial.mobileapp.utils.PushNotification;
import org.sturgeon.sdial.sms.service.SmsService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

/**
 * Created by User on 13/05/2016.
 */
@Service
public class SubscriberServiceImpl implements SubscriberService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriberServiceImpl.class);
    private final SubscriberRepository subscriberRepository;
    private final SubscriptionRequestRepository subscriptionRequestRepository;
    private final SchoolReferralRepository schoolReferralRepository;
    private final SchoolService schoolService;
    private final StudentService studentService;
    private final SmsService smsService;
    private final JCMPushNotificationService jcmPushNotificationService;

    @Autowired
    public SubscriberServiceImpl(SubscriberRepository subscriberRepository, SchoolService schoolService, SubscriptionRequestRepository subscriptionRequestRepository, StudentService studentService, SmsService smsService, JCMPushNotificationService jcmPushNotificationService, SchoolReferralRepository schoolReferralRepository) {
        this.subscriberRepository = subscriberRepository;
        this.schoolService = schoolService;
        this.subscriptionRequestRepository = subscriptionRequestRepository;
        this.studentService = studentService;
        this.smsService = smsService;
        this.jcmPushNotificationService = jcmPushNotificationService;
        this.schoolReferralRepository = schoolReferralRepository;
    }

    @Override
    public Optional<Subscriber> findSubscriberByPhoneNumber(String phoneNumber) {
        return subscriberRepository.findByPhoneNumber(processPhoneNumber(phoneNumber));
    }

    @Override
    public Subscriber create(SubscriberCreateForm subscriberCreateForm) {
        Subscriber subscriber = new Subscriber();
        subscriber.setPhoneNumber(processPhoneNumber(subscriberCreateForm.getUserPhone()));
        subscriber.setEmail(subscriberCreateForm.getUserEmail());
        Long twoWeeks = (new Date().getTime()) + (14 * 24 * 3600 * 1000);
        Date date = new Date(twoWeeks);
        subscriber.setValidUntil(date);
        subscriber.setPasswordHash(new BCryptPasswordEncoder().encode(subscriberCreateForm.getPassword()));
        subscriberRepository.save(subscriber);
        return subscriber;
    }

    @Override
    public Optional<Subscriber> getUserByEmail(String userEmail) {
        return subscriberRepository.findOneByEmail(userEmail);
    }

    @Override
    public Optional<Subscriber> Authenticate(SubscriberLoginForm subscriberLoginForm) {
        final String subscriberPhoneNumber = subscriberLoginForm.getSubscriberPhoneNumber();
        return subscriberRepository.findByPhoneNumber(subscriberPhoneNumber);
    }

    @Override
    public void registerDevice(String phoneNumber, String token) {
        subscriberRepository.updateDeviceToken(phoneNumber, token);
    }

    @Override
    public void requestSubscription(String studentid, String phoneNumber) {

        Optional<Student> student = studentService.getStudentbyId(Long.parseLong(studentid));
        if (student.isPresent()) {

            SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
            subscriptionRequest.setPhoneNumber(processPhoneNumber(phoneNumber));
            subscriptionRequest.setStudentid(Long.parseLong(studentid));
            subscriptionRequest.setSchoolid(student.get().getSchoolid());
            subscriptionRequest.setDate(new Date());
            subscriptionRequestRepository.save(subscriptionRequest);

            Collection<SubscriberStudent> subscriberStudents = studentService.findPrimaryByStudentid(student.get().getStudentid());
            if (subscriberStudents != null) {
                String recipients = new String();
                for (SubscriberStudent subscriberStudent : subscriberStudents) {
                    if(subscriberStudent.getPrimaryContact()) {
                        recipients = subscriberStudent.getSubscriber().getPhoneNumber() + ",";
                    }
                }
                if(!recipients.isEmpty()) {
                    SmsMessageForm smsMessageForm = new SmsMessageForm();
                    smsMessageForm.setOtherRecipients(recipients);
                    smsMessageForm.setText("Dear parent, a user with number " + phoneNumber + " requests to access " + getFirstName(student.get().getStudentName()) + "'s account on ShuleDial. To give access, dial *882*91# or go to the application and select the Add User menu.");
                    smsService.processOtherSMS(smsMessageForm);
                }
            }

        }
    }

    @Override
    public void processPushNotification(PushNotificationForm form) {
        if (form.getSendtoAll()) {
            ArrayList<String> tokens = new ArrayList<String>();

            Collection<Student> students = studentService.getStudentsbySchool(form.getSchoolid());

            if (!students.isEmpty()) {
                for (Student student : students) {
                    Collection<SubscriberStudent> subscriberStudents = studentService.findByStudentid(student.getStudentid());
                    if (!subscriberStudents.isEmpty()) {
                        for (SubscriberStudent subscriberStudent : subscriberStudents) {
                            String deviceToken = subscriberStudent.getSubscriber().getDeviceToken();
                            if (deviceToken != null) {
                                tokens.add(deviceToken);
                            }
                        }
                    }
                }
            }

            if (!tokens.isEmpty()) {
                PushNotification pushNotification = new PushNotification("normal", new JCMNotification(form.getTitle(), form.getMessage()), tokens);
                jcmPushNotificationService.sendNotification(pushNotification);
            }

        } else {
            ArrayList<String> tokens = new ArrayList<String>();

            for (String recipient : form.getRecipients()) {
                Collection<Student> students = studentService.getStudentsbyGroup(Long.parseLong(recipient));
                if (!students.isEmpty()) {
                    for (Student student : students) {
                        Collection<SubscriberStudent> subscriberStudents = studentService.findByStudentid(student.getStudentid());
                        if (!subscriberStudents.isEmpty()) {
                            for (SubscriberStudent subscriberStudent : subscriberStudents) {
                                String deviceToken = subscriberStudent.getSubscriber().getDeviceToken();
                                if (deviceToken != null) {
                                    tokens.add(deviceToken);
                                }
                            }
                        }
                    }
                }
            }

            if (!tokens.isEmpty()) {
                PushNotification pushNotification = new PushNotification("default", new JCMNotification(form.getTitle(), form.getMessage()), tokens);
                jcmPushNotificationService.sendNotification(pushNotification);
            }

        }
    }

    @Override
    public SchoolReferral processSchoolReferral(SchoolReferralForm form) {
        SchoolReferral schoolReferral = new SchoolReferral();

        schoolReferral.setSchoolName(form.getSchoolName());
        schoolReferral.setSchoolLocation(form.getSchoolLocation());
        schoolReferral.setContactPerson(form.getContactPerson());
        schoolReferral.setEmail(form.getEmail());
        schoolReferral.setPhoneNumber(form.getPhoneNumber());

        return schoolReferralRepository.save(schoolReferral);
    }

    private String getFirstName(String name) {
        if (name.indexOf(" ") > -1) {
            return name.substring(0, name.indexOf(" "));
        } else {
            return name;
        }
    }

    private String processPhoneNumber(String phoneNumber) {
        String intlPhoneNumber;
        if (phoneNumber.startsWith("07")) {
            intlPhoneNumber = phoneNumber.replace("07", "2547");
            return intlPhoneNumber;
        } else if (phoneNumber.startsWith("+")) {
            intlPhoneNumber = phoneNumber.replace("+", "");
            return intlPhoneNumber;
        } else {
            return phoneNumber;
        }
    }
}