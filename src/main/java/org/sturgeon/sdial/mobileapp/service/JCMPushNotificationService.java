package org.sturgeon.sdial.mobileapp.service;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.google.gson.Gson;
import org.codehaus.jackson.map.util.JSONPObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.sturgeon.sdial.mobileapp.utils.FirebaseResponse;
import org.sturgeon.sdial.mobileapp.utils.HeaderRequestInterceptor;
import org.sturgeon.sdial.mobileapp.utils.PushNotification;


/**
 *
 * Created by Roy on 5/17/2017
 *
 */

@Service
public class JCMPushNotificationService {

    private static final String FCM_SERVER_KEY = "AAAAvFBCnwY:APA91bEp5Z7mzRCSSWI_D52M7yYjGm4rCQmewK-d0Nb8FkVWXd8x1mSlpJeDzfdcFYyKwNLIMW635PsJ57o0vj7rEAZphOvyoPQVzw0JBQhWD4oaZ8rI62eIYGgXhYTfWPoL5TpLkCrA";
    private static final String FCM_API = "https://fcm.googleapis.com/fcm/send";


    public FirebaseResponse sendNotification(PushNotification push) {

        HttpEntity<String> request = new HttpEntity<String>(new Gson().toJson(push));

        CompletableFuture<FirebaseResponse> pushNotification = this.send(request);
        CompletableFuture.allOf(pushNotification).join();

        FirebaseResponse firebaseResponse = null;

        try {
            firebaseResponse = pushNotification.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        return firebaseResponse;
    }

    @Async
    private CompletableFuture<FirebaseResponse> send(HttpEntity<String> entity) {

        RestTemplate restTemplate = new RestTemplate();

        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
        interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + FCM_SERVER_KEY));
        interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
        restTemplate.setInterceptors(interceptors);
        FirebaseResponse firebaseResponse = null;
        try{
            firebaseResponse = restTemplate.postForObject(FCM_API, entity, FirebaseResponse.class);
        } catch (RestClientException e){
            e.printStackTrace();
        }



        return CompletableFuture.completedFuture(firebaseResponse);
    }
}