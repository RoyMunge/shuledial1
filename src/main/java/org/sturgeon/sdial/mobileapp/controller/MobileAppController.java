package org.sturgeon.sdial.mobileapp.controller;

import com.google.gson.Gson;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.sturgeon.sdial.common.model.*;
import org.sturgeon.sdial.common.model.forms.*;
import org.sturgeon.sdial.common.service.school.*;
import org.sturgeon.sdial.mobileapp.service.SubscriberService;
import org.sturgeon.sdial.mobileapp.utils.*;
import org.sturgeon.sdial.sms.service.SmsService;
import org.sturgeon.sdial.sms.utils.SmsRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by User on 13/05/2016.
 */

@Controller
public class MobileAppController {

    private final SubscriberService subscriberService;
    private final AnnouncementService announcementService;
    private final EventService eventService;
    private final SchoolService schoolService;
    private final StudentService studentService;
    private final ExamResultsService examResultsService;
    private final SchoolFeesService schoolFeesService;
    private final SchoolLevelService schoolLevelService;
    private final SchoolPeriodService schoolPeriodService;
    private final SchoolTypeService schoolTypeService;
    private final SmsService smsService;

    @Autowired
    public MobileAppController(SubscriberService subscriberService, AnnouncementService announcementService, EventService eventService, SchoolService schoolService, StudentService studentService, ExamResultsService examResultsService, SchoolFeesService schoolFeesService, SchoolLevelService schoolLevelService, SchoolPeriodService schoolPeriodService, SchoolTypeService schoolTypeService, SmsService smsService) {
        this.subscriberService = subscriberService;
        this.announcementService = announcementService;
        this.eventService = eventService;
        this.schoolService = schoolService;
        this.studentService = studentService;
        this.examResultsService = examResultsService;
        this.schoolFeesService = schoolFeesService;
        this.schoolLevelService = schoolLevelService;
        this.schoolPeriodService = schoolPeriodService;
        this.schoolTypeService = schoolTypeService;
        this.smsService = smsService;
    }


    @RequestMapping(value = "/mobileapp/register", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public
    @ResponseBody
    String RegisterSubscriber(@RequestBody SubscriberCreateForm subscriberCreateForm) {
        Boolean isRegistered = false;
        Subscriber subscriber = subscriberService.create(subscriberCreateForm);

        if (subscriber != null) {
            isRegistered = true;
        }
        RegistrationResponse registrationResponse = new RegistrationResponse(isRegistered);

        return new Gson().toJson(registrationResponse);
    }

    @RequestMapping(value = "/mobileapp/login", method = RequestMethod.POST)
    public
    @ResponseBody
    String LoginSubscriber(@RequestBody SubscriberLoginForm subscriberLoginForm) {
        Boolean isValidUser = false;
        Optional<Subscriber> subscriber = subscriberService.Authenticate(subscriberLoginForm);
        if (subscriber.isPresent()) {
            if (new BCryptPasswordEncoder().matches(subscriberLoginForm.getSubscriberPin(), subscriber.get().getPasswordHash())) {
                isValidUser = true;
            }
        }
        LoginResponse loginResponse = new LoginResponse(isValidUser);
        return new Gson().toJson(loginResponse);
    }

    @RequestMapping(value = "/mobileapp/subscriber", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetSubscriber(@RequestParam("phoneNumber") String phoneNumebr) {
        Optional<Subscriber> optional = subscriberService.findSubscriberByPhoneNumber(phoneNumebr);

        if (optional.isPresent()) {
            return new Gson().toJson(optional.get());
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/mobileapp/addstudent", method = RequestMethod.GET)
    public
    @ResponseBody
    String AddStudent(@RequestParam("studentid") String studentid, @RequestParam("phonenumber") String phonenumber) {

        Optional<Student> student = studentService.getStudentbyId(Long.parseLong(studentid));
        if (student.isPresent()) {
            subscriberService.requestSubscription(studentid, phonenumber);
            return new Gson().toJson("success");
        } else {
            return new Gson().toJson("error");
        }
    }

    @RequestMapping(value = "/mobileapp/students", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetStudents(@RequestParam("phonenumber") String phoneNumber) {
        Optional<Subscriber> optional = subscriberService.findSubscriberByPhoneNumber(phoneNumber);
        List<Student> students = new ArrayList<Student>();
        if (optional.isPresent()) {
            Collection<SubscriberStudent> subscriberStudents = studentService.findBySubscriberid(optional.get().getSubscriberid());
            if (subscriberStudents != null) {
                ArrayList<StudentResponse> studentResponseList = new ArrayList<StudentResponse>();
                for (SubscriberStudent subscriberStudent : subscriberStudents) {
                    Student student = subscriberStudent.getStudent();
                    StudentResponse studentResponse = new StudentResponse(student);
                    studentResponseList.add(studentResponse);
                }
                return new Gson().toJson(studentResponseList);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/mobileapp/announcements", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetAnnouncements(@RequestParam("studentid") String studentid) {
        Optional<Student> student = studentService.getStudentbyId(Long.parseLong(studentid));

        if (student.isPresent()) {
            Collection<Announcement> announcements = announcementService.getAnnouncementsbySchoolId(student.get().getSchool().getSchoolid());
            if (!announcements.isEmpty()) {
                ArrayList<AnnouncementResponse> announcementsResponse = new ArrayList<AnnouncementResponse>();
                Iterator iterator = announcements.iterator();
                while (iterator.hasNext()) {
                    Announcement announcement = (Announcement) iterator.next();
                    AnnouncementResponse announcementResponse = new AnnouncementResponse(announcement, Long.parseLong(studentid));
                    announcementsResponse.add(announcementResponse);
                }
                return new Gson().toJson(announcementsResponse);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/mobileapp/events", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetEvents(@RequestParam("studentid") String studentid) {
        Optional<Student> student = studentService.getStudentbyId(Long.parseLong(studentid));
        if (student.isPresent()) {
            Collection<Event> events = eventService.getUpcomingEvents(student.get().getSchool().getSchoolid());
            if (!events.isEmpty()) {
                ArrayList<EventResponse> eventsResponse = new ArrayList<EventResponse>();
                Iterator iterator = events.iterator();
                while (iterator.hasNext()) {
                    Event event = (Event) iterator.next();
                    EventResponse eventResponse = new EventResponse(event, Long.parseLong(studentid));
                    eventsResponse.add(eventResponse);
                }
                return new Gson().toJson(eventsResponse);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/mobileapp/results", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetExamResults(@RequestParam("studentid") String studentid) {
        Collection<ExamResults> examResults = examResultsService.getExamResultsByStudentId(Long.parseLong(studentid));
        if (!examResults.isEmpty()) {
            ArrayList<ExamResultsResponse> examResultsResponse = new ArrayList<ExamResultsResponse>();
            Iterator iterator = examResults.iterator();
            while (iterator.hasNext()) {
                ExamResults results = (ExamResults) iterator.next();
                ExamResultsResponse resultsResponse = new ExamResultsResponse(results);
                examResultsResponse.add(resultsResponse);
            }
            return new Gson().toJson(examResultsResponse);
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/mobileapp/student", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetStudent(@RequestParam("admission") String admission, @RequestParam("shortname") String shortname) {
        Optional<School> school = schoolService.getSchoolByShortName(shortname);
        if (school.isPresent()) {
            Optional<Student> student = studentService.getStudentbyAdmission(school.get().getSchoolid(), admission);
            if (student.isPresent()) {
                StudentResponse studentResponse = new StudentResponse(student.get());
                return new Gson().toJson(studentResponse);

            } else {
                return null;
            }
        } else {
            return null;
        }

    }

    @RequestMapping(value = "/mobileapp/school", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetSchool(@RequestParam("shortname") String shortname) {
        Optional<School> school = schoolService.getSchoolByShortName(shortname);
        if (school.isPresent()) {
            SchoolResponse schoolResponse = new SchoolResponse(school.get().getShortName(), school.get().getName());
            return new Gson().toJson(schoolResponse);
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/mobileapp/schools", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetSchools(@RequestParam("searchstring") String searchstring) {
        Collection<School> schools = schoolService.searchSchools(searchstring);
        if (!schools.isEmpty()) {
            ArrayList<SchoolResponse> schoolResponseList = new ArrayList<SchoolResponse>();
            Iterator iterator = schools.iterator();
            while (iterator.hasNext()) {
                School school = (School) iterator.next();
                SchoolResponse schoolResponse = new SchoolResponse(school.getShortName(), school.getName());
                schoolResponseList.add(schoolResponse);
            }
            return new Gson().toJson(schoolResponseList);
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/mobileapp/schoolbio", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetSchoolBio(@RequestParam("shortname") String shortname) {
        Optional<School> school = schoolService.getSchoolByShortName(shortname);
        if (school.isPresent()) {
            SchoolBioResponse schoolBioResponse = new SchoolBioResponse(school.get());
            return new Gson().toJson(schoolBioResponse);
        } else {
            return null;
        }

    }

    @RequestMapping(value = "/mobileapp/schoolsbio", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetAllSchoolBio() {

        Collection<School> schools = schoolService.getAllSchools();
        if (!schools.isEmpty()) {
            ArrayList<SchoolBioMinimalResponse> schoolResponseList = new ArrayList<SchoolBioMinimalResponse>();
            Iterator iterator = schools.iterator();
            while (iterator.hasNext()) {
                School school = (School) iterator.next();
                SchoolBioMinimalResponse schoolBioMinimalResponse = new SchoolBioMinimalResponse(school);
                schoolResponseList.add(schoolBioMinimalResponse);
            }

            return new Gson().toJson(schoolResponseList);
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/mobileapp/schoolsearchdropdowns", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetSchoolSearchDropdowns() {
        Collection<County> counties = schoolService.getAllCounties();
        Collection<SchoolType> schoolTypes = schoolTypeService.getAllSchoolTypes();
        SchoolSearchDropdowns schoolSearchDropdowns = new SchoolSearchDropdowns(counties, schoolTypes);
        return new Gson().toJson(schoolSearchDropdowns);
    }

    @RequestMapping(value = "/mobileapp/admissiondropdowns", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetAdmissionDropdowns(@RequestParam("schoolid") String schoolid) {
        Optional<School> school = schoolService.getSchoolById(Long.parseLong(schoolid));
        if (school.isPresent()) {
            Collection<SchoolLevel> schoolLevels = schoolLevelService.getAllSchoolLevels(Long.parseLong(schoolid));
            Collection<QualificationLevel> qualificationLevels = schoolService.getQualificationLevels();
            AdmissionDropdowns admissionDropdowns = new AdmissionDropdowns(schoolLevels, qualificationLevels);
            return new Gson().toJson(admissionDropdowns);
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/mobileapp/admission", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public
    @ResponseBody
    String AdmissionRequest(@RequestBody AdmissionForm admissionForm) {
        Boolean isRegistered = false;
        Admission admission = schoolService.createAdmission(admissionForm);

        if (admission != null) {
            isRegistered = true;
        }
        RegistrationResponse registrationResponse = new RegistrationResponse(isRegistered);

        return new Gson().toJson(registrationResponse);
    }

    @RequestMapping(value = "/mobileapp/schoolsearchbycountyandtype", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetSchoolsByCountyandType(@RequestParam("countyid") String countyid, @RequestParam("schooltypeid") String schooltypeid) {
        if (countyid.equals("963852741") && !schooltypeid.equals("963852741")) {
            Collection<School> schools = schoolService.getSchoolsByType(Long.parseLong(schooltypeid));
            if (!schools.isEmpty()) {
                ArrayList<SchoolBioMinimalResponse> schoolResponseList = new ArrayList<SchoolBioMinimalResponse>();
                Iterator iterator = schools.iterator();
                while (iterator.hasNext()) {
                    School school = (School) iterator.next();
                    SchoolBioMinimalResponse schoolBioMinimalResponse = new SchoolBioMinimalResponse(school);
                    schoolResponseList.add(schoolBioMinimalResponse);
                }
                return new Gson().toJson(schoolResponseList);
            } else {
                return null;
            }
        } else if (!countyid.equals("963852741") && schooltypeid.equals("963852741")) {
            Collection<School> schools = schoolService.getSchoolsByCounty(Long.parseLong(countyid));
            if (!schools.isEmpty()) {
                ArrayList<SchoolBioMinimalResponse> schoolResponseList = new ArrayList<SchoolBioMinimalResponse>();
                Iterator iterator = schools.iterator();
                while (iterator.hasNext()) {
                    School school = (School) iterator.next();
                    SchoolBioMinimalResponse schoolBioMinimalResponse = new SchoolBioMinimalResponse(school);
                    schoolResponseList.add(schoolBioMinimalResponse);
                }
                return new Gson().toJson(schoolResponseList);
            } else {
                return null;
            }
        } else {
            Collection<School> schoolsByType = schoolService.getSchoolsByType(Long.parseLong(schooltypeid));

            ArrayList<SchoolBioMinimalResponse> schoolResponseList = new ArrayList<SchoolBioMinimalResponse>();

            if (!schoolsByType.isEmpty()) {
                for (School school : schoolsByType) {
                    if (school.getSubCounty().getCounty().getCountyid() == Long.parseLong(countyid)) {
                        SchoolBioMinimalResponse schoolBioMinimalResponse = new SchoolBioMinimalResponse(school);
                        schoolResponseList.add(schoolBioMinimalResponse);
                    }
                }

                return new Gson().toJson(schoolResponseList);
            } else {
                return null;
            }

        }
    }

    @RequestMapping(value = "/mobileapp/feedropdowns", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetFeeDropdowns(@RequestParam("studentid") String studentid) {

        Optional<Student> student = studentService.getStudentbyId(Long.parseLong(studentid));

        if (student.isPresent()) {

            Collection<SchoolPeriod> schoolPeriodCollection = schoolPeriodService.getAllSchoolPeriods(student.get().getSchoolid());
            Collection<SchoolLevel> schoolLevelCollection = schoolLevelService.getAllSchoolLevels(student.get().getSchoolid());

            if (!schoolPeriodCollection.isEmpty() && !schoolLevelCollection.isEmpty()) {
                SchoolFeesDropdowns schoolFeesDropdowns = new SchoolFeesDropdowns(schoolPeriodCollection, schoolLevelCollection);

                return new Gson().toJson(schoolFeesDropdowns);

            } else {
                return null;
            }
        } else {
            return null;
        }

    }

    @RequestMapping(value = "/mobileapp/feebalance", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetFeeBalance(@RequestParam("studentid") String studentid) {
        Optional<FeeBalance> feeBalance = schoolFeesService.getFeeBalance(Long.parseLong(studentid));
        if (feeBalance.isPresent()) {
            FeeBalance fee = feeBalance.get();
            School school = schoolService.getSchoolById(fee.getStudent().getSchoolid()).get();
            SchoolFeesBalanceResponse schoolFeesBalanceResponse = new SchoolFeesBalanceResponse(fee.getBalance(), fee.getPaid(), school.getFeesPaymentInfo());
            return new Gson().toJson(schoolFeesBalanceResponse);
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/mobileapp/periodtotal", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetSchoolPeriodTotal(@RequestParam("schoolperiodid") String schoolperiodid, @RequestParam("schoollevelid") String schoollevelid) {
        Optional<SchoolLevelPeriodTotal> schoolLevelPeriodTotal = schoolFeesService.getSchoolLevelPeriodTotal(Long.parseLong(schoolperiodid), Long.parseLong(schoollevelid));
        if (schoolLevelPeriodTotal.isPresent()) {
            SchoolLevelPeriodTotal total = schoolLevelPeriodTotal.get();
            SchoolPeriodTotalResponse schoolPeriodTotalResponse = new SchoolPeriodTotalResponse(total.getSchoolPeriod().getSchoolPeriodName(), total.getSchoolLevel().getSchoolLevelName(), total.getSchoolPeriod().getStartDate(), total.getSchoolPeriod().getEndDate(), total.getAmount());
            return new Gson().toJson(schoolPeriodTotalResponse);
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/mobileapp/feeitems", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetFeeItems(@RequestParam("schoolperiodid") String schoolperiodid, @RequestParam("schoollevelid") String schoollevelid) {
        Collection<FeeItemAmount> feeItemAmountCollection = schoolFeesService.getBySchoolPeriodandSchoolLevel(Long.parseLong(schoolperiodid), Long.parseLong(schoollevelid));
        if (!feeItemAmountCollection.isEmpty()) {
            ArrayList<FeeItemResponse> feeItemResponseArrayList = new ArrayList<FeeItemResponse>();
            for (FeeItemAmount feeItemAmount : feeItemAmountCollection) {
                FeeItemResponse feeItemResponse = new FeeItemResponse(feeItemAmount.getFeeItem().getFeeItemName(), feeItemAmount.getAmount());
                feeItemResponseArrayList.add(feeItemResponse);
            }
            return new Gson().toJson(feeItemResponseArrayList);
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/mobileapp/verification", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public
    @ResponseBody
    String AdmissionRequest(@RequestBody VerificationForm form) {
        Boolean isSubscriber = false;
        Optional<Subscriber> subscriber = subscriberService.findSubscriberByPhoneNumber(form.getPhoneNumber());

        if (subscriber.isPresent()) {
            isSubscriber = true;
            SmsMessageForm smsMessageForm = new SmsMessageForm();
            smsMessageForm.setOtherRecipients(form.getPhoneNumber());
            smsMessageForm.setText("Verification code: " + form.getVerificationCode());
            smsService.processOtherSMS(smsMessageForm);
        }
        VerificationResponse verificationResponse = new VerificationResponse(isSubscriber);

        return new Gson().toJson(verificationResponse);
    }

    @RequestMapping(value = "/mobileapp/referral", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public
    @ResponseBody
    String ReferralRequest(@RequestBody SchoolReferralForm form) {
        Boolean isRegistered = false;
        SchoolReferral schoolReferral = subscriberService.processSchoolReferral(form);

        if (schoolReferral != null){
            isRegistered = true;
        }

        RegistrationResponse registrationResponse = new RegistrationResponse(isRegistered);

        return new Gson().toJson(registrationResponse);
    }

    @RequestMapping(value = "/mobileapp/validuntil", method = RequestMethod.GET)
    public
    @ResponseBody
    String GetValidUntil(@RequestParam("phonenumber") String phoneNumber) {
        String response = null;
        Optional<Subscriber> subscriber = subscriberService.findSubscriberByPhoneNumber(phoneNumber);
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        if (subscriber.isPresent()) {
            Date validUntil = subscriber.get().getValidUntil();
            response = dateFormat.format(validUntil);
        }

        return response;
    }

    @RequestMapping(value = "/mobileapp/token", method = RequestMethod.GET)
    public
    @ResponseBody
    String RegisterDevice(@RequestParam("phonenumber") String phoneNumber, @RequestParam("token") String token) {
        subscriberService.registerDevice(phoneNumber, token);
        return "ok";
    }

    @RequestMapping(value = "/mobileapp/addsubscriber", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public
    @ResponseBody
    String AddSubscriber(@RequestBody SubscriberAddForm subscriberAddForm) {
        Boolean isRegistered = false;
        Optional<SubscriberStudent> subscriberStudent = studentService.findbyPhoneNumberandStudentId(subscriberAddForm.getMsisdn(),Long.parseLong(subscriberAddForm.getStudentid()));

        if (subscriberStudent.isPresent() && subscriberStudent.get().getPrimaryContact()) {
            Student student = subscriberStudent.get().getStudent();
            ParentAddForm form = new ParentAddForm();
            form.setParentName(subscriberAddForm.getPhoneNumber());
            form.setAdmission(student.getAdmission());
            form.setStudentName(student.getStudentName());
            form.setPhoneNumber(subscriberAddForm.getName());
            form.setIsPrimary(false);
            form.setSchoolid(student.getSchoolid());

            Subscriber subscriber = studentService.createSubscriber(form);

            if (subscriber != null){

                isRegistered = true;
            }

        }

        RegistrationResponse registrationResponse = new RegistrationResponse(isRegistered);
        return new Gson().toJson(registrationResponse);
    }

    @RequestMapping(value = "/app", method = RequestMethod.GET, produces = "application/apk")
    public ResponseEntity<InputStreamResource> download() throws IOException {

        File file = new File("/opt/shuledial/shuledial.apk");
        if (!file.exists()) {
            file = new File("D:/test.apk");
            if (!file.exists()) {
                throw new FileNotFoundException("Oops! File not found");
            }
        }

        InputStreamResource isResource = new InputStreamResource(new FileInputStream(file));
        FileSystemResource fileSystemResource = new FileSystemResource(file);
        String fileName = FilenameUtils.getName(file.getAbsolutePath());
        fileName = new String(fileName.getBytes("UTF-8"), "iso-8859-1");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.setContentLength(fileSystemResource.contentLength());
        headers.setContentDispositionFormData("attachment", fileName);
        return new ResponseEntity<InputStreamResource>(isResource, headers, HttpStatus.OK);
    }


}
