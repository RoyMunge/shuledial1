package org.sturgeon.sdial.mobileapp.utils;

/**
 * Created by User on 06/02/2017.
 */
public class SchoolFeesBalanceResponse {

    private String balance;
    private String paid;
    private String paymentInfo;

    public SchoolFeesBalanceResponse(String balance, String paid, String paymentInfo) {
        this.balance = balance;
        this.paid = paid;
        this.paymentInfo = paymentInfo;
    }
}
