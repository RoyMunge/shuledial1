package org.sturgeon.sdial.mobileapp.utils;

import org.sturgeon.sdial.common.model.Announcement;

import java.util.Date;

/**
 * Created by User on 13/06/2016.
 */
public class AnnouncementResponse {
    private Long announcementid;
    private Long studentid;
    private String announcementName;
    private Date postingDate;
    private String description;

    public AnnouncementResponse(Announcement announcement, Long studentid){
        this.announcementid = announcement.getAnnouncementid();
        this.announcementName = announcement.getAnnouncementName();
        this.postingDate = announcement.getPostingDate();
        this.description = announcement.getDescription();
        this.studentid = studentid;
    }
}
