package org.sturgeon.sdial.mobileapp.utils;

import java.util.List;

/**
 * Created by Roy on 5/17/2017.
 */
public class PushNotification {
    private List<String> registration_ids;

    private String priority;

    private JCMNotification notification;

    private JCMNotification data;

    public PushNotification(String priority, JCMNotification notification, List<String> registration_ids) {
        this.priority = priority;
        this.notification = notification;
        this.data = notification;
        this.registration_ids = registration_ids;
    }

    public PushNotification() {
    }

}
