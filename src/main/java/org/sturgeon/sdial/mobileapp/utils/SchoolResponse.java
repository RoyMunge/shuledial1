package org.sturgeon.sdial.mobileapp.utils;

/**
 * Created by User on 17/09/2016.
 */
public class SchoolResponse {
    private String schoolShortName;
    private String schoolName;

    public SchoolResponse(String schoolShortName, String schoolName){
        this.schoolShortName = schoolShortName;
        this.schoolName = schoolName;
    }
}