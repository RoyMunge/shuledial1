package org.sturgeon.sdial.mobileapp.utils;

/**
 * Created by User on 06/02/2017.
 */
public class SchoolLevelResponse {
    private String schoollevelid;
    private String schoollevelname;

    public SchoolLevelResponse(String schoollevelid, String schoollevelname) {
        this.schoollevelid = schoollevelid;
        this.schoollevelname = schoollevelname;
    }
}
