package org.sturgeon.sdial.mobileapp.utils;

import org.sturgeon.sdial.common.model.SchoolLevel;
import org.sturgeon.sdial.common.model.SchoolPeriod;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by User on 06/02/2017.
 */
public class SchoolFeesDropdowns {
    ArrayList<SchoolPeriodResponse> schoolPeriods;
    ArrayList<SchoolLevelResponse> schoolLevels;

    public SchoolFeesDropdowns(Collection<SchoolPeriod> schoolPeriodCollection, Collection<SchoolLevel> schoolLevelCollection){
        this.schoolPeriods = new ArrayList<SchoolPeriodResponse>();
        this.schoolLevels = new ArrayList<SchoolLevelResponse>();

        for(SchoolPeriod schoolPeriod : schoolPeriodCollection){
            SchoolPeriodResponse schoolPeriodResponse = new SchoolPeriodResponse(schoolPeriod.getSchoolperiodid().toString(), schoolPeriod.getSchoolPeriodName());
            schoolPeriods.add(schoolPeriodResponse);
        }

        for(SchoolLevel schoolLevel : schoolLevelCollection){
            SchoolLevelResponse schoolLevelResponse = new SchoolLevelResponse(schoolLevel.getSchoollevelid().toString(), schoolLevel.getSchoolLevelName());
            schoolLevels.add(schoolLevelResponse);
        }
    }
}
