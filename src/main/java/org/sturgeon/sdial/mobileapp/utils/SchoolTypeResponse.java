package org.sturgeon.sdial.mobileapp.utils;

/**
 * Created by User on 06/02/2017.
 */
public class SchoolTypeResponse {
    private Long schooltypeid;
    private String schoolTypeName;

    public SchoolTypeResponse(){}

    public SchoolTypeResponse(Long schooltypeid, String schoolTypeName) {
        this.schooltypeid = schooltypeid;
        this.schoolTypeName = schoolTypeName;
    }
}
