package org.sturgeon.sdial.mobileapp.utils;

import org.sturgeon.sdial.common.model.Student;

/**
 * Created by User on 08/06/2016.
 */
public class StudentResponse {
    private Long studentid;
    private String admission;
    private String studentName;
    private String schoolName;

    public StudentResponse(Student student){
        this.studentid = student.getStudentid();
        this.admission = student.getAdmission();
        this.studentName = student.getStudentName();
        this.schoolName = student.getSchool().getName();
    }
}
