package org.sturgeon.sdial.mobileapp.utils;

/**
 * Created by User on 06/02/2017.
 */
public class FeeItemResponse {
    private String name;
    private String amount;

    public FeeItemResponse(String name, String amount) {
        this.name = name;
        this.amount = amount;
    }
}
