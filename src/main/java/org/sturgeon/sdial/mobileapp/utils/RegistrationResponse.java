package org.sturgeon.sdial.mobileapp.utils;

/**
 * Created by User on 22/05/2016.
 */
public class RegistrationResponse {
    private boolean isRegistered;

    public RegistrationResponse() {}

    public RegistrationResponse(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }
}
