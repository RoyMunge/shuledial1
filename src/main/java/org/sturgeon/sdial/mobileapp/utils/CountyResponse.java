package org.sturgeon.sdial.mobileapp.utils;

/**
 * Created by User on 06/02/2017.
 */
public class CountyResponse {
    private Long countyid;
    private String countyName;

    public CountyResponse(){}

    public CountyResponse(Long countyid, String countyName) {
        this.countyid = countyid;
        this.countyName = countyName;
    }
}
