package org.sturgeon.sdial.mobileapp.utils;

/**
 * Created by Roy on 5/9/2017.
 */
public class VerificationResponse {
    private Boolean isSubscriber;

    public VerificationResponse() {
    }

    public VerificationResponse(Boolean isSubscriber) {
        this.isSubscriber = isSubscriber;
    }
}
