package org.sturgeon.sdial.mobileapp.utils;

/**
 * Created by User on 22/05/2016.
 */
public class LoginResponse {
    private boolean isValidUser;

    public LoginResponse() {}

    public LoginResponse(boolean isValidUser) {
        this.isValidUser = isValidUser;
    }
}