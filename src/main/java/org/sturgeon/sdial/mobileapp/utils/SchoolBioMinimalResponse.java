package org.sturgeon.sdial.mobileapp.utils;

import org.sturgeon.sdial.common.model.School;

/**
 * Created by User on 25/01/2017.
 */
public class SchoolBioMinimalResponse {
    private Long schoolid;
    private String name;
    private String shortName;
    private String motto;
    private String physicalAddress;
    private String bio;
    private String county;

    public SchoolBioMinimalResponse (School school){
        this.schoolid = school.getSchoolid();
        this.name = school.getName();
        this.shortName = school.getShortName();
        this.motto = school.getMotto();
        this.physicalAddress = school.getPhysicalAddress();
        this.bio = school.getOverview();
        this.county = school.getSubCounty().getCounty().getCountyName();
    }
}
