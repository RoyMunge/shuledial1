package org.sturgeon.sdial.mobileapp.utils;

import java.util.Date;

import org.sturgeon.sdial.common.model.ExamResults;

/**
 * Created by User on 20/06/2016.
 */
public class ExamResultsResponse {
    private Long examresultid;
    private Long studentid;
    private String examName;
    private Date examDate;
    private String english;
    private String kiswahili;
    private String maths;
    private String science;
    private String biology;
    private String physics;
    private String chemistry;
    private String history;
    private String geography;
    private String cre;
    private String ire;
    private String hre;
    private String homescience;
    private String art;
    private String agriculture;
    private String commputer;
    private String aviation;
    private String french;
    private String german;
    private String arabic;
    private String music;
    private String businessstudies;
    private String socialstudies;
    private String ssre;
    private String total;
    private String average;
    private String grade;
    private String position;

    public ExamResultsResponse(ExamResults examResults) {
        this.examresultid = examResults.getExamresultid();
        this.studentid = examResults.getStudentid();
        this.examName = examResults.getExam().getExamName();
        this.examDate = examResults.getExam().getExamDate();
        this.english = examResults.getEnglish();
        this.kiswahili = examResults.getKiswahili();
        this.maths = examResults.getMaths();
        this.science = examResults.getScience();
        this.biology = examResults.getBiology();
        this.physics = examResults.getPhysics();
        this.chemistry = examResults.getChemistry();
        this.history = examResults.getHistory();
        this.geography = examResults.getGeography();
        this.cre = examResults.getCre();
        this.ire = examResults.getIre();
        this.hre = examResults.getHre();
        this.homescience = examResults.getHomescience();
        this.art = examResults.getArt();
        this.agriculture = examResults.getAgriculture();
        this.commputer = examResults.getCommputer();
        this.aviation = examResults.getAviation();
        this.french = examResults.getFrench();
        this.german = examResults.getGerman();
        this.arabic = examResults.getArabic();
        this.music = examResults.getMusic();
        this.businessstudies = examResults.getBusinessstudies();
        this.socialstudies = examResults.getSocialstudies();
        this.ssre = examResults.getSsre();
        this.total = examResults.getTotal();
        this.average = examResults.getAverage();
        this.grade = examResults.getGrade();
        this.position = examResults.getPosition();
    }
}
