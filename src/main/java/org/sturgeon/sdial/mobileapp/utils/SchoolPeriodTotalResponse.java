package org.sturgeon.sdial.mobileapp.utils;

import java.util.Date;

/**
 * Created by User on 06/02/2017.
 */
public class SchoolPeriodTotalResponse {
    private String periodName;
    private String levelName;
    private Date startDate;
    private Date endDate;
    private String totalAmount;

    public SchoolPeriodTotalResponse(String periodName, String levelName, Date startDate, Date endDate, String totalAmount) {
        this.periodName = periodName;
        this.levelName = levelName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.totalAmount = totalAmount;
    }
}
