package org.sturgeon.sdial.mobileapp.utils;


import org.sturgeon.sdial.common.model.Event;

import java.util.Date;

/**
 * Created by User on 13/06/2016.
 */
public class EventResponse {
    private Long eventid;
    private Long studentid;
    private String eventName;
    private String location;
    private Date eventDate;
    private Date endDate;
    private String description;

    public EventResponse(Event event, Long studentid){
        this.eventid = event.getEventid();
        this.eventName = event.getEventName();
        this.location = event.getLocation();
        this.eventDate = event.getEventDate();
        this.endDate = event.getEndDate();
        this.description = event.getDescription();
        this.studentid = studentid;
    }
}
