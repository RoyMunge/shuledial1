package org.sturgeon.sdial.mobileapp.utils;

import org.sturgeon.sdial.common.model.QualificationLevel;
import org.sturgeon.sdial.common.model.SchoolLevel;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by User on 30/01/2017.
 */
public class AdmissionDropdowns {

    private ArrayList<String> schoolLevels;
    private ArrayList<String> qualificationLevels;

    public AdmissionDropdowns(Collection<SchoolLevel> schoolLevelCollection, Collection<QualificationLevel> qualificationLevelCollection){
        schoolLevels = new ArrayList<String>();
        qualificationLevels = new ArrayList<String>();

        for(SchoolLevel schoolLevel: schoolLevelCollection){
            schoolLevels.add(schoolLevel.getSchoolLevelName());
        }

        for(QualificationLevel qualificationLevel: qualificationLevelCollection){
            qualificationLevels.add(qualificationLevel.getQualificationLevelName());
        }
    }
}
