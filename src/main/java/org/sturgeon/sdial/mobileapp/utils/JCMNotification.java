package org.sturgeon.sdial.mobileapp.utils;

/**
 * Created by Roy on 5/17/2017.
 */
public class JCMNotification {
    private String title;
    private String body;

    public JCMNotification(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return body;
    }

    public void setMessage(String body) {
        this.body = body;
    }
}
