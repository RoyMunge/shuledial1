package org.sturgeon.sdial.mobileapp.utils;


import org.sturgeon.sdial.common.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 24/11/2016.
 */
public class SchoolBioResponse {

    private Long schoolid;
    private String name;
    private String shortName;
    private List<String> phoneNumberSet;
    private List<String> emailSet;
    private List<String> schoolTypeSet;
    private String website;
    private String motto;
    private String physicalAddress;
    private String postalAddress;
    private String bio;
    private String admissionInfo;
    private String feesPaymentInfo;
    private String county;
    private String subCounty;

    public SchoolBioResponse(School school){
        this.schoolid = school.getSchoolid();
        this.name = school.getName();
        this.shortName = school.getShortName();
        this.website = school.getWebsite();
        this.motto = school.getMotto();
        this.physicalAddress = school.getPhysicalAddress();
        this.postalAddress = school.getPostalAddress();
        this.bio = school.getBio();
        this.admissionInfo = school.getAdmissionInfo();
        this.feesPaymentInfo = school.getFeesPaymentInfo();
        this.phoneNumberSet = new ArrayList<String>();
        this.emailSet = new ArrayList<String>();
        this.schoolTypeSet = new ArrayList<String>();
        this.county = school.getSubCounty().getCounty().getCountyName();
        this.subCounty = school.getSubCounty().getSubcountyname();


        for (PhoneNumber phoneNumber: school.getPhoneNumberSet()){
            phoneNumberSet.add(phoneNumber.getPhoneNumber());
        }

        for (Email email: school.getEmailSet()) {
            emailSet.add(email.getEmail());
        }

        for (SchoolType schoolType: school.getSchoolTypeSet()) {
            schoolTypeSet.add(schoolType.getSchoolTypeName());
        }
    }

}
