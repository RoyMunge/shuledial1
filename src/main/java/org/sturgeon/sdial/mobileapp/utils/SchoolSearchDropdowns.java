package org.sturgeon.sdial.mobileapp.utils;

import org.sturgeon.sdial.common.model.County;
import org.sturgeon.sdial.common.model.SchoolType;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by User on 30/01/2017.
 */
public class SchoolSearchDropdowns {
    ArrayList<CountyResponse> counties;
    ArrayList<SchoolTypeResponse> categories;

    public SchoolSearchDropdowns(Collection<County> countyCollection, Collection<SchoolType> schoolTypeCollection){
        this.counties = new ArrayList<CountyResponse>();
        this.categories = new ArrayList<SchoolTypeResponse>();

        for (County county : countyCollection){
            CountyResponse countyResponse = new CountyResponse(county.getCountyid(), county.getCountyName());
            counties.add(countyResponse);
        }
        for (SchoolType schoolType : schoolTypeCollection){
            SchoolTypeResponse schoolTypeResponse = new SchoolTypeResponse(schoolType.getSchooltypeid(), schoolType.getSchoolTypeName());
            categories.add(schoolTypeResponse);
        }
    }

}
