package org.sturgeon.sdial.ussd.transport;

/**
 * Created by Roy.Munge on 27/08/2015.
 */
public class SafaricomRequest
{
    private String session_id;
    private String from_address;
    private String dest_address;
    private String ussd_parms;

    public SafaricomRequest(String session_id, String from_address, String dest_address, String ussd_parms)
    {
        this.session_id = session_id;
        this.from_address = from_address;
        this.dest_address = dest_address;
        this.ussd_parms = ussd_parms;
    }
}