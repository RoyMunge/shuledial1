package org.sturgeon.sdial.ussd.transport;

/**
 * Created by Roy.Munge on 27/08/2015.
 */
public class AirtelRequest
{
    private String session_id;
    private String msisdn;
    private String msc;
    private String input;

    public AirtelRequest(String session_id, String msisdn, String msc, String input)
    {
        this.session_id = session_id;
        this.msisdn = msisdn;
        this.msc = msc;
        this.input = input;
    }
}
