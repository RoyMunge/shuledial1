package org.sturgeon.sdial.ussd.utils;

import org.sturgeon.sdial.ussd.states.StateInterface;

/**
 * Created by Roy. W. Munge  on 28/01/2016.
 */
public class Response {

    private StateInterface next;
    private String response;
    private boolean isEndString;

    public Response(StateInterface next, String response, boolean isEndString) {
        this.next = next;
        this.response = response;
        this.isEndString = isEndString;
    }

    public StateInterface getNext() {
        return next;
    }

    public void setNext(StateInterface next) {
        this.next = next;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public boolean isEndString() {
        return isEndString;
    }

    public void setIsEndString(boolean isEndString) {
        this.isEndString = isEndString;
    }
}
