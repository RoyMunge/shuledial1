package org.sturgeon.sdial.ussd.states;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.sturgeon.sdial.common.model.School;
import org.sturgeon.sdial.common.model.Student;
import org.sturgeon.sdial.common.service.school.*;
import org.sturgeon.sdial.ussd.session.Session;
import org.sturgeon.sdial.ussd.utils.Response;

/**
 * Created by Roy.Munge on 27/08/2015.
 */
public enum State implements StateInterface
{

    WELCOME{
        @Override
        public Response process(String paramString, Session session) {

            String reply = "Welcome to ShuleDial\n\nPlease enter school short name:";
            State next = this.SHORTNAME;
            return new Response(next, reply, false);
        }
    },

    SHORTNAME{

        @Autowired
        SchoolService schoolService;

        @Override
        public Response process(String paramString, Session session) {
            State next;
            String reply;

            System.out.println(schoolService);

            School school = schoolService.getSchoolByShortName(paramString).get();

            if (school != null){
                session.setSchool(school);
                reply = "Enter students Admission number:";
                next = this.ADMISSION;
            } else {
                reply = "The school short name you provided does not exist \n Please confirm and try again.";
                next = this.SHORTNAME;
            }

            return new Response(next,reply,false);
        }
    },

    ADMISSION{
        @Autowired
        StudentService studentService;

        @Override
        public Response process(String paramString, Session session) {
            State next;
            String reply;

            Student student = studentService.getStudentbyAdmission(session.getSchool().getSchoolid(), paramString).get();

            if (student != null){
                session.setStudent(student);
                reply = student.getStudentName() +"\nMain Menu:\n1. Results\n2. Fees\n3. Events and Dates\n4. Announcements";
                next = this.MENU;
            } else {
                reply = "The admission does not exist in " + session.getSchool().getName()+ "\nPlease confirm and try again.";
                next = this.ADMISSION;
            }

            return new Response(next,reply,false);
        }
    },

    MENU{

        @Override
        public Response process(String paramString, Session session) {
            State next;
            String reply;

            next = this.MENU;
            reply = "Thanks";

            return new Response(next,reply,false);
        }
    },


}