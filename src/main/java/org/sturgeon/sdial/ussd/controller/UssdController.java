package org.sturgeon.sdial.ussd.controller;

/**
 * Created by Roy.Munge on 27/08/2015.
 */


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.sturgeon.sdial.ussd.handlers.ProcessUSSD;

@RestController
public class UssdController
{
    @Autowired
    ProcessUSSD processUSSD;

    @RequestMapping(value={"/SDIAL/USSD/SAFARICOM"}, method={org.springframework.web.bind.annotation.RequestMethod.GET}, produces={"text/plain"})
    public ResponseEntity<String> SafaricomRequest(@RequestParam(value="MSISDN", required=true) String msisdn, @RequestParam(value="SERVICE_CODE", required=true) String service_code, @RequestParam(value="SESSION_ID", required=true) String session_id, @RequestParam(value="INPUT", required=true) String ussd_string)
    {
        String response = this.processUSSD.Safaricom(msisdn, service_code, session_id, ussd_string);
        return new ResponseEntity(response, HttpStatus.OK);
    }
}
