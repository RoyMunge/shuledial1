package org.sturgeon.sdial.ussd.states;

import org.sturgeon.sdial.ussd.handlers.ServiceManager;
import org.sturgeon.sdial.ussd.session.Session;
import org.sturgeon.sdial.ussd.utils.Response;

/**
 * Created by Roy W. Munge on 29/01/2016.
 */

public interface StateInterface {
    Response process(String paramString, Session session, ServiceManager serviceManager);
}
