package org.sturgeon.sdial.ussd.states;


import org.sturgeon.sdial.common.model.*;
import org.sturgeon.sdial.common.model.forms.ParentAddForm;
import org.sturgeon.sdial.common.model.forms.SubscriberAddForm;
import org.sturgeon.sdial.common.service.school.*;
import org.sturgeon.sdial.ussd.handlers.ServiceManager;
import org.sturgeon.sdial.ussd.session.Session;
import org.sturgeon.sdial.ussd.utils.Response;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Optional;

/**
 * Created by Roy.Munge on 27/08/2015.
 */
public enum State implements StateInterface {

    WELCOME {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            State next;
            String reply;
            Boolean isEnd;

            Optional<Subscriber> subscriber = serviceManager.getSubscriberService().findSubscriberByPhoneNumber(session.getMsisdn());
            if (subscriber.isPresent()) {
                Collection<SubscriberStudent> subscriberStudents = serviceManager.getStudentService().findBySubscriberid(subscriber.get().getSubscriberid());

                if (subscriberStudents == null) {
                    reply = "CON Welcome to ShuleDial\nPlease enter school short name:";
                    next = this.SHORTNAME;
                    isEnd = false;
                }

                if (!subscriberStudents.isEmpty()) {

                    reply = "CON Welcome to ShuleDial.\nSelect Student Account:";
                    int counter = 1;
                    for (SubscriberStudent subscriberStudent : subscriberStudents) {
                        reply = reply + "\n" + String.valueOf(counter) + ". " + subscriberStudent.getStudent().getStudentName();
                        session.getSelectionMap().put(String.valueOf(counter), subscriberStudent.getStudent().getStudentid().toString());
                        counter++;
                    }
                    reply = reply + "\n99. Add Student Account";
                    next = this.INTERMIDIATE_MENU;
                    isEnd = false;
                } else {
                    reply = "CON Welcome to ShuleDial\nPlease enter school short name:";
                    next = this.SHORTNAME;
                    isEnd = false;
                }

            } else {
                reply = "CON Welcome to ShuleDial\nPlease enter school short name:";
                next = this.SHORTNAME;
                isEnd = false;
            }


            return new Response(next, reply, isEnd);
        }
    },

    INTERMIDIATE_MENU {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            State next;
            String reply;
            Boolean isEnd;

            if (paramString.equals("99")) {
                reply = "CON Add Student Account\nPlease enter school short name:";
                next = this.SHORTNAME;
                isEnd = false;
            } else {

                Long studentid = Long.parseLong(session.getSelectionMap().get(paramString));
                session.getSelectionMap().clear();

                if (studentid != null) {

                    Optional<Student> student = serviceManager.getStudentService().getStudentbyId(studentid);
                    if (student.isPresent()) {
                        session.setStudent(student.get());
                        reply = "CON " + student.get().getStudentName() + "\nMain Menu:\n1. Results\n2. Fees\n3. Events and Dates\n4. Announcements\n5.Add User\n000. Logout";
                        next = this.MENU;
                        isEnd = false;
                    } else {
                        reply = "END The Student Account has not been setup yet. Please contact the school.";
                        next = null;
                        isEnd = true;
                    }
                } else {

                    Optional<Subscriber> subscriber = serviceManager.getSubscriberService().findSubscriberByPhoneNumber(session.getMsisdn());
                    if (subscriber.isPresent()) {
                        Collection<SubscriberStudent> subscriberStudents = serviceManager.getStudentService().findBySubscriberid(subscriber.get().getSubscriberid());
                        if (!subscriberStudents.isEmpty()) {

                            reply = "CON Invalid choice.\nSelect Student Account:";
                            int counter = 1;
                            for (SubscriberStudent subscriberStudent : subscriberStudents) {
                                reply = reply + "\n" + String.valueOf(counter) + ". " + subscriberStudent.getStudent().getStudentName();
                                session.getSelectionMap().put(String.valueOf(counter), subscriberStudent.getStudent().getStudentid().toString());
                                counter++;
                            }
                            reply = reply + "\n99. Add Student Account";
                            next = this.INTERMIDIATE_MENU;
                            isEnd = false;
                        } else {
                            reply = "CON Welcome to ShuleDial\nPlease enter school short name:";
                            next = this.SHORTNAME;
                            isEnd = false;
                        }

                    } else {
                        reply = "CON Welcome to ShuleDial\nPlease enter school short name:";
                        next = this.SHORTNAME;
                        isEnd = false;
                    }


                }
            }

            return new Response(next, reply, isEnd);
        }

    },

    SHORTNAME {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            State next;
            String reply;
            Boolean isEnd;

            Optional<School> school = serviceManager.getSchoolService().getSchoolByShortName(paramString);

            if (school.isPresent()) {
                session.getSelectionMap().put("schoolid", school.get().getSchoolid().toString());
                session.getSelectionMap().put("schoolname", school.get().getName());
                reply = "CON Enter students Admission number:";
                next = this.ADMISSION;
                isEnd = false;
            } else {
                reply = "CON The school short name you provided does not exist \nPlease confirm and enter again.";
                next = this.SHORTNAME;
                isEnd = false;
            }

            return new Response(next, reply, isEnd);
        }
    },

    ADMISSION {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            State next;
            String reply;
            Boolean isEnd;

            Optional<Student> student = serviceManager.getStudentService().getStudentbyAdmission(Long.parseLong(session.getSelectionMap().get("schoolid")), paramString);

            if (student.isPresent()) {
                session.getSelectionMap().clear();
                serviceManager.getSubscriberService().requestSubscription(student.get().getStudentid().toString(), session.getMsisdn());

                reply = "END " + student.get().getStudentName() + "\nYou will be notified via SMS once you have been authorized to access the requested Student's Account.";
                next = null;
                isEnd = true;
            } else {
                reply = "CON The admission number does not exist in " + session.getSelectionMap().get("schoolname") + "\nPlease confirm and enter again.";
                next = this.ADMISSION;
                isEnd = false;
            }

            return new Response(next, reply, isEnd);
        }
    },

    MENU {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            State next;
            String reply;
            Boolean isEnd;

            if (paramString.equals("1")) {
                Collection<ExamResults> exams = serviceManager.getExamResultsService().getExamResultsByStudentId(session.getStudent().getStudentid());

                if (exams == null) {
                    reply = "CON No exam results have been updated yet.\n00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                }
                if (!exams.isEmpty()) {

                    reply = "CON Select Exam:";

                    int counter = 1;
                    for (ExamResults exam : exams) {
                        reply = reply + "\n" + String.valueOf(counter) + ". " + exam.getExam().getExamName();
                        session.getSelectionMap().put(String.valueOf(counter), exam.getExamresultid().toString());
                        counter++;
                        if (counter > 5) {
                            break;
                        }
                    }
                    reply = reply + "\n00. Main Menu000. Logout";
                    next = this.RESULTS;
                    isEnd = false;
                } else {
                    reply = "CON No exam results have been updated yet.\n00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                }

            } else if (paramString.equals("2")) {
                Optional<FeeBalance> feeBalanceOptional = serviceManager.getSchoolFeesService().getFeeBalance(session.getStudent().getStudentid());
                if (feeBalanceOptional.isPresent()) {
                    FeeBalance feeBalance = feeBalanceOptional.get();
                    reply = "CON You have paid Ksh. " + feeBalance.getPaid() + " and your balance is Kshs. " + feeBalance.getBalance() + "\n00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                } else {
                    reply = "CON School fees details have not been loaded yet.\n00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                }

            } else if (paramString.equals("3")) {
                Collection<Event> events = serviceManager.getEventService().getUpcomingEventsByStudent(session.getStudent().getStudentid());

                if (events == null) {
                    reply = "CON No events have been updated yet.\n00. Main Menu 000.Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                } else if (!events.isEmpty()) {

                    reply = "CON Select Event:";

                    int counter = 1;
                    for (Event event : events) {
                        reply = reply + "\n" + String.valueOf(counter) + ". " + event.getEventName();
                        session.getSelectionMap().put(String.valueOf(counter), event.getEventid().toString());
                        counter++;
                        if (counter > 5) {
                            break;
                        }
                    }
                    reply = reply + "\n00. Main Menu 000. Logout";
                    next = this.EVENTS;
                    isEnd = false;

                } else {
                    reply = "CON No events have been updated yet.\n00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                }


            } else if (paramString.equals("4")) {

                Collection<Announcement> announcements = serviceManager.getAnnouncementService().getAnnouncementsByStudent(session.getStudent().getStudentid());

                if (announcements == null) {
                    reply = "CON No announcements have been updated yet.\n00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                }else if (!announcements.isEmpty()) {
                    reply = "CON Select Announcement:";

                    int counter = 1;
                    for (Announcement announcement : announcements) {
                        reply = reply + "\n" + String.valueOf(counter) + ". " + announcement.getAnnouncementName();
                        session.getSelectionMap().put(String.valueOf(counter), announcement.getAnnouncementid().toString());
                        counter++;
                        if (counter > 5) {
                            break;
                        }
                    }
                    reply = reply + "\n00. Main Menu 000. Logout";
                    next = this.ANNOUNCEMENTS;
                    isEnd = false;
                } else {
                    reply = "CON no announcements have been updated yet.\n00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                }

            } else if (paramString.equals("5")) {
                Optional<SubscriberStudent> subscriberStudent = serviceManager.getStudentService().findbyPhoneNumberandStudentId(session.getMsisdn(), session.getStudent().getStudentid());
                if (subscriberStudent.isPresent()) {
                    if (subscriberStudent.get().getPrimaryContact()) {
                        reply = "CON Add new User to access " + getFirstName(session.getStudent().getStudentName()) + "'s Account.\nEnter User's Name:";
                        next = INTERMIDIATE_ADD_PARENT;
                        isEnd = false;
                    } else {
                        reply = "CON Only the Primary Parent is allowed to add Users\n00. Main Menu 000. Logout";
                        next = INTERMIDIATE_MENU;
                        isEnd = false;
                    }
                } else {
                    reply = "CON Only the Primary Parent is allowed to add Users\n00. Main Menu 000. Logout";
                    next = INTERMIDIATE_MENU;
                    isEnd = false;
                }
            } else if (paramString.equals("000")) {
                reply = "END Thank you for using ShuleDial. You can now download our application on playstore.";
                next = null;
                isEnd = true;
            } else {
                reply = "CON Wrong input. Try again:\nMain Menu:\n1. Results\n2. Fees\n3. Events and Dates\n4. Announcements\n5.Add User\n000. Logout";
                next = this.MENU;
                isEnd = false;

            }

            return new Response(next, reply, isEnd);
        }

        private String getFirstName(String name) {
            if (name.indexOf(" ") > -1) {
                return name.substring(0, name.indexOf(" "));
            } else {
                return name;
            }
        }
    },

    INTERMIDIATE_DEFAULT {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            State next = this.MENU;
            String reply = "";
            Boolean isEnd;

            if (paramString.equals("00")) {
                reply = "CON " + session.getStudent().getStudentName() + "\nMain Menu:\n1. Results\n2. Fees\n3. Events and Dates\n4. Announcements\n5.Add User\n000. Logout";
                next = this.MENU;
                isEnd = false;
            } else {
                reply = "END Thank you for using ShuleDial. You can now download our application on playstore.";
                next = null;
                isEnd = true;
            }

            return new Response(next, reply, isEnd);
        }
    },

    INTERMIDIATE_RESULTS {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            State next = this.MENU;
            String reply = "";
            Boolean isEnd;

            if (paramString.equals("0")) {
                Collection<ExamResults> exams = serviceManager.getExamResultsService().getExamResultsByStudentId(session.getStudent().getStudentid());

                if (exams == null) {
                    reply = "CON No exam results have been updated yet.\n00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                }

                if (!exams.isEmpty()) {

                    reply = "CON Select Exam:";

                    int counter = 1;
                    for (ExamResults exam : exams) {
                        reply = reply + "\n" + String.valueOf(counter) + ". " + exam.getExam().getExamName();
                        session.getSelectionMap().put(String.valueOf(counter), exam.getExamresultid().toString());
                        counter++;
                    }
                    reply = reply + "\n00. Main Menu 000. Logout";
                    next = this.RESULTS;
                    isEnd = false;
                } else {
                    reply = "CON No exam results have been updated yet.\n00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                }

            } else if (paramString.equals("00")) {
                reply = "CON " + session.getStudent().getStudentName() + "\nMain Menu:\n1. Results\n2. Fees\n3. Events and Dates\n4. Announcements\n5.Add User\n000. Logout";
                next = this.MENU;
                isEnd = false;

            } else {
                reply = "END Thank you for using ShuleDial. You can now download our application on playstore.";
                next = null;
                isEnd = true;
            }

            return new Response(next, reply, isEnd);
        }
    },

    INTERMIDIATE_EVENTS {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            State next = this.MENU;
            String reply = "";
            Boolean isEnd;

            if (paramString.equals("0")) {
                Collection<Event> events = serviceManager.getEventService().getUpcomingEvents(session.getStudent().getSchool().getSchoolid());

                if (events == null) {
                    reply = "CON No exam results have been updated yet.\n00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                }

                if (!events.isEmpty()) {

                    reply = "CON Select Event:";

                    int counter = 1;
                    for (Event event : events) {
                        reply = reply + "\n" + String.valueOf(counter) + ". " + event.getEventName();
                        session.getSelectionMap().put(String.valueOf(counter), event.getEventid().toString());
                        counter++;
                    }
                    reply = reply + "\n00. Main Menu 000. Logout";
                    next = this.EVENTS;
                    isEnd = false;

                } else {
                    reply = "CON No events have been updated yet.\n00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                }


            } else if (paramString.equals("00")) {
                reply = "CON " + session.getStudent().getStudentName() + "\nMain Menu:\n1. Results\n2. Fees\n3. Events and Dates\n4. Announcements\n5.Add User\n000. Logout";
                next = this.MENU;
                isEnd = false;

            } else {
                reply = "END Thank you for using ShuleDial. You can now download our application on playstore.";
                next = null;
                isEnd = true;
            }

            return new Response(next, reply, isEnd);
        }
    },

    INTERMIDIATE_ANNOUNCEMENTS {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            State next = this.MENU;
            String reply = "";
            Boolean isEnd;

            if (paramString.equals("0")) {
                Collection<Announcement> announcements = serviceManager.getAnnouncementService().getAnnouncementsbySchoolId(session.getStudent().getSchool().getSchoolid());

                if (announcements == null) {
                    reply = "CON No exam results have been updated yet.\n00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                }

                if (!announcements.isEmpty()) {
                    reply = "CON Select Announcement:";

                    int counter = 1;
                    for (Announcement announcement : announcements) {
                        reply = reply + "\n" + String.valueOf(counter) + ". " + announcement.getAnnouncementName();
                        session.getSelectionMap().put(String.valueOf(counter), announcement.getAnnouncementid().toString());
                        counter++;
                    }
                    reply = reply + "\n00. Main Menu 000. Logout";
                    next = this.ANNOUNCEMENTS;
                    isEnd = false;
                } else {
                    reply = "CON no announcements have been updated yet.\n00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_DEFAULT;
                    isEnd = false;
                }

            } else if (paramString.equals("00")) {
                reply = "CON " + session.getStudent().getStudentName() + "\nMain Menu:\n1. Results\n2. Fees\n3. Events and Dates\n4. Announcements\n5.Add User\n000. Logout";
                next = this.MENU;
                isEnd = false;
            } else {
                reply = "Thank you for using ShuleDial. You can now download our application on playstore.";
                next = null;
                isEnd = true;
            }

            return new Response(next, reply, isEnd);
        }
    },


    RESULTS {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            State next;
            String reply;
            Boolean isEnd;

            if (paramString.equals("00")) {
                reply = "CON " + session.getStudent().getStudentName() + "\nMain Menu:\n1. Results\n2. Fees\n3. Events and Dates\n4. Announcements\n5.Add User\n000. Logout";
                next = this.MENU;
                isEnd = false;
                return new Response(next, reply, isEnd);
            }
            if (paramString.equals("000")) {
                reply = "END Thank you for using ShuleDial. You can now download our application on playstore.";
                next = null;
                isEnd = true;
                return new Response(next, reply, isEnd);
            }

            Long examid = Long.parseLong(session.getSelectionMap().get(paramString));
            session.getSelectionMap().clear();

            if (examid != null) {
                Optional<ExamResults> examResultsOptional = serviceManager.getExamResultsService().getExamResultsById(examid);
                if (examResultsOptional.isPresent()) {
                    ExamResults examResults = examResultsOptional.get();
                    reply = "CON ";

                    if (examResults.getPosition() != null) {
                        reply = reply + " POS " + examResults.getPosition();
                    }
                    if (examResults.getTotal() != null) {
                        reply = reply + " Total " + examResults.getTotal();
                    }
                    if (examResults.getAverage() != null) {
                        reply = reply + " Average " + examResults.getAverage();
                    }
                    if (examResults.getGrade() != null) {
                        reply = reply + " Grade " + examResults.getGrade();
                    }
                    if (examResults.getEnglish() != null) {
                        reply = reply + " ENG " + examResults.getEnglish();
                    }
                    if (examResults.getKiswahili() != null) {
                        reply = reply + " KIS " + examResults.getKiswahili();
                    }
                    if (examResults.getMaths() != null) {
                        reply = reply + " MATH " + examResults.getMaths();
                    }
                    if (examResults.getScience() != null) {
                        reply = reply + " SCI " + examResults.getScience();
                    }
                    if (examResults.getBiology() != null) {
                        reply = reply + " BIO " + examResults.getBiology();
                    }
                    if (examResults.getPhysics() != null) {
                        reply = reply + " PHY " + examResults.getPhysics();
                    }
                    if (examResults.getChemistry() != null) {
                        reply = reply + " CHEM " + examResults.getChemistry();
                    }
                    if (examResults.getHistory() != null) {
                        reply = reply + " HIST " + examResults.getHistory();
                    }
                    if (examResults.getGeography() != null) {
                        reply = reply + " GEO " + examResults.getGeography();
                    }
                    if (examResults.getCre() != null) {
                        reply = reply + " CRE " + examResults.getCre();
                    }
                    if (examResults.getIre() != null) {
                        reply = reply + " IRE " + examResults.getIre();
                    }
                    if (examResults.getHre() != null) {
                        reply = reply + " HRE " + examResults.getHre();
                    }
                    if (examResults.getHomescience() != null) {
                        reply = reply + " Home SCI " + examResults.getHomescience();
                    }
                    if (examResults.getArt() != null) {
                        reply = reply + " ART " + examResults.getArt();
                    }
                    if (examResults.getAgriculture() != null) {
                        reply = reply + " AGRI " + examResults.getAgriculture();
                    }
                    if (examResults.getCommputer() != null) {
                        reply = reply + " COMP " + examResults.getCommputer();
                    }
                    if (examResults.getAviation() != null) {
                        reply = reply + " Aviation " + examResults.getAviation();
                    }
                    if (examResults.getFrench() != null) {
                        reply = reply + " FRE " + examResults.getFrench();
                    }
                    if (examResults.getGerman() != null) {
                        reply = reply + " GERM " + examResults.getGerman();
                    }
                    if (examResults.getArabic() != null) {
                        reply = reply + " Arabic " + examResults.getArabic();
                    }
                    if (examResults.getMusic() != null) {
                        reply = reply + " MUSIC " + examResults.getMusic();
                    }
                    if (examResults.getBusinessstudies() != null) {
                        reply = reply + "BStudies " + examResults.getBusinessstudies();
                    }
                    if (examResults.getSocialstudies() != null) {
                        reply = reply + " SS" + examResults.getSocialstudies();
                    }
                    if (examResults.getSsre() != null) {
                        reply = reply + " SSRE " + examResults.getSsre();
                    }

                    reply = reply + "\n0. Back 00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_RESULTS;
                    isEnd = false;
                } else {
                    reply = "CON The results for the exam are not available yet.\n0. Back 00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_RESULTS;
                    isEnd = false;
                }
            } else {
                reply = "CON The results for the exam are not available yet.\n0. Back 00. Main Menu 000. Logout";
                next = this.INTERMIDIATE_RESULTS;
                isEnd = false;
            }

            return new Response(next, reply, isEnd);
        }
    },

    EVENTS {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            State next;
            String reply;
            Boolean isEnd;

            if (paramString.equals("00")) {
                reply = "CON " + session.getStudent().getStudentName() + "\nMain Menu:\n1. Results\n2. Fees\n3. Events and Dates\n4. Announcements\n5.Add User\n000. Logout";
                next = this.MENU;
                isEnd = false;
                return new Response(next, reply, isEnd);
            }
            if (paramString.equals("000")) {
                reply = "END Thank you for using ShuleDial. You can now download our application on playstore.";
                next = null;
                isEnd = true;
                return new Response(next, reply, isEnd);
            }

            Long eventid = Long.parseLong(session.getSelectionMap().get(paramString));
            session.getSelectionMap().clear();
            if (eventid != null) {
                Optional<Event> eventOptional = serviceManager.getEventService().getEventbyId(eventid);
                if (eventOptional.isPresent()) {
                    Event event = eventOptional.get();
                    reply = "CON " + event.getEventName() + ", at: " + event.getLocation() + " on: " + dateFormat.format(event.getEventDate()) + " " + dateFormat.format(event.getEndDate()) + "\n" + event.getDescription();
                    reply = reply + "\n0. Back 00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_EVENTS;
                    isEnd = false;
                } else {
                    reply = "CON The details for the event are not available yet.\n0. Back 00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_EVENTS;
                    isEnd = false;
                }
            } else {
                reply = "CON The details for the event are not available yet.\n0. Back 00. Main Menu 000. Logout";
                next = this.INTERMIDIATE_EVENTS;
                isEnd = false;
            }

            return new Response(next, reply, isEnd);
        }
    },

    ANNOUNCEMENTS {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            State next;
            String reply;
            Boolean isEnd;

            if (paramString.equals("00")) {
                reply = "CON " + session.getStudent().getStudentName() + "\nMain Menu:\n1. Results\n2. Fees\n3. Events and Dates\n4. Announcements\n5.Add User\n000. Logout";
                next = this.MENU;
                isEnd = false;
                return new Response(next, reply, isEnd);
            }
            if (paramString.equals("000")) {
                reply = "END Thank you for using ShuleDial. You can now download our application on playstore.";
                next = null;
                isEnd = true;
                return new Response(next, reply, isEnd);
            }

            Long announcementid = Long.parseLong(session.getSelectionMap().get(paramString));
            session.getSelectionMap().clear();
            if (announcementid != null) {
                Optional<Announcement> announcementOptional = serviceManager.getAnnouncementService().getAnnouncementById(announcementid);
                if (announcementOptional.isPresent()) {
                    Announcement announcement = announcementOptional.get();
                    reply = "CON " + announcement.getDescription();
                    reply = reply + "\n0. Back 00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_ANNOUNCEMENTS;
                    isEnd = false;
                } else {
                    reply = "CON The details for the announcement are not available yet.\n0. Back 00. Main Menu 000. Logout";
                    next = this.INTERMIDIATE_ANNOUNCEMENTS;
                    isEnd = false;
                }

            } else {
                reply = "CON The details for the announcement are not available yet.\n0. Back 00. Main Menu 000. Logout";
                next = this.INTERMIDIATE_ANNOUNCEMENTS;
                isEnd = false;
            }

            return new Response(next, reply, isEnd);
        }
    },

    INTERMIDIATE_ADD_PARENT {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            String reply;
            State next;
            Boolean isEnd;

            session.getSelectionMap().put("parentName", paramString);
            reply = "CON Enter Parent's or User's Phone Number:";
            next = ADD_PARENT;
            isEnd = false;


            return new Response(next, reply, isEnd);
        }
    },

    ADD_PARENT {
        @Override
        public Response process(String paramString, Session session, ServiceManager serviceManager) {
            String reply;
            State next;
            Boolean isEnd;

            String pattern = "2547(\\d{8})|07(\\d{8})";
            if (paramString.matches(pattern)) {
                ParentAddForm form = new ParentAddForm();
                form.setIsPrimary(false);
                form.setPhoneNumber(paramString);
                form.setStudentName(session.getStudent().getStudentName());
                form.setAdmission(session.getStudent().getAdmission());
                form.setSchoolid(session.getStudent().getSchool().getSchoolid());
                form.setParentName(session.getSelectionMap().get("parentName"));

                Subscriber subscriber = serviceManager.getStudentService().createSubscriber(form);

                if (subscriber != null) {
                    reply = "CON User added successfully\n00. Main Menu 000. Logout";
                    next = INTERMIDIATE_DEFAULT;
                    isEnd = false;
                } else {
                    reply = "CON Error in adding User. Try Again Later.\n00. Main Menu 000. Logout";
                    next = INTERMIDIATE_DEFAULT;
                    isEnd = false;
                }
            } else {
                reply = "CON Invalid Phone Number. Please input again:";
                next = ADD_PARENT;
                isEnd = false;
            }
            return new Response(next, reply, isEnd);
        }
    }


}