package org.sturgeon.sdial.ussd.handlers;

/**
 * Created by Roy.Munge on 27/08/2015.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.ussd.utils.Response;
import org.sturgeon.sdial.ussd.session.Session;
import org.sturgeon.sdial.ussd.session.SessionManager;

@Service
public class ProcessUSSD
{

    @Autowired
    SessionManager sessionManager;

    @Autowired
    ServiceManager serviceManager;

    public String Safaricom(String msisdn, String service_code, String session_id, String input)
    {

        System.out.println(this.sessionManager.session.size());
        Response response;
        if (!this.sessionManager.sessionExits(msisdn, session_id))
        {
            this.sessionManager.createSession(msisdn, session_id);
            Session thisSession = this.sessionManager.getSession(msisdn);
            response = thisSession.getState().process(input, thisSession, serviceManager);
            if (response.isEndString()){
                sessionManager.deleteSession(msisdn,session_id);
            } else {
                thisSession.setState(response.getNext());
                this.sessionManager.updateSession(msisdn, thisSession);
            }
        }
        else
        {
            Session thisSession = this.sessionManager.getSession(msisdn);
            response = thisSession.getState().process(input, thisSession, serviceManager);
            if (response.isEndString()){
                sessionManager.deleteSession(msisdn,session_id);
            } else {
                thisSession.setState(response.getNext());
                this.sessionManager.updateSession(msisdn, thisSession);
            }
        }
        return response.getResponse();
    }
}

