package org.sturgeon.sdial.ussd.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.service.school.*;
import org.sturgeon.sdial.mobileapp.service.SubscriberService;

/**
 * Created by Roy W. Munge on 29/01/2016.
 */
@Service
public class ServiceManager {
    private final SchoolService schoolService;
    private final StudentService studentService;
    private final ExamService examService;
    private final ExamResultsService examResultsService;
    private final SchoolFeesService schoolFeesService;
    private final EventService eventService;
    private final AnnouncementService announcementService;
    private final SubscriberService subscriberService;

    @Autowired
    public ServiceManager(SchoolService schoolService, StudentService studentService, ExamService examService, ExamResultsService examResultsService, SchoolFeesService schoolFeesService, EventService eventService, AnnouncementService announcementService, SubscriberService subscriberService) {
        this.schoolService = schoolService;
        this.studentService = studentService;
        this.examService = examService;
        this.examResultsService = examResultsService;
        this.schoolFeesService = schoolFeesService;
        this.eventService = eventService;
        this.announcementService = announcementService;
        this.subscriberService = subscriberService;
    }

    public SchoolService getSchoolService() {
        return schoolService;
    }

    public StudentService getStudentService() {
        return studentService;
    }

    public ExamService getExamService() {
        return examService;
    }

    public ExamResultsService getExamResultsService() {
        return examResultsService;
    }

    public SchoolFeesService getSchoolFeesService() {
        return schoolFeesService;
    }

    public EventService getEventService() {
        return eventService;
    }

    public AnnouncementService getAnnouncementService() {
        return announcementService;
    }

    public SubscriberService getSubscriberService() {
        return subscriberService;
    }
}
