package org.sturgeon.sdial.ussd.session;

/**
 * Created by Roy.Munge on 27/08/2015.
 */

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.ussd.states.State;

@Service
public class SessionManager
{
    public ConcurrentMap<String, Session> session = new ConcurrentHashMap();

    public boolean sessionExits(String msisdn, String session_id)
    {
        if (this.session.get(msisdn) != null)
        {
            if (((Session)this.session.get(msisdn)).getSession_id().equals(session_id))
            {
                return true;
            }
            deleteSession(msisdn, session_id);
            return false;
        }
        return false;
    }

    public void createSession(String msisdn, String session_id)
    {
        Session newSession = new Session(session_id, State.WELCOME, msisdn);
        this.session.put(msisdn, newSession);
    }

    public void deleteSession(String msisdn, String session_id)
    {
        if (this.session.get(msisdn) != null) {
            this.session.remove(msisdn);
        }
    }

    public Session getSession(String msisdn)
    {
        return (Session)this.session.get(msisdn);
    }

    public void updateSession(String msisdn, Session sessionupdate)
    {
        this.session.replace(msisdn, sessionupdate);
    }
}
