package org.sturgeon.sdial.ussd.session;

/**
 * Created by Roy.Munge on 27/08/2015.
 */

import org.sturgeon.sdial.common.model.School;
import org.sturgeon.sdial.common.model.Student;
import org.sturgeon.sdial.ussd.states.StateInterface;

import java.util.HashMap;

public class Session
{
    private String session_id;
    private StateInterface state;
    private String msisdn;
    private Student student;
    private HashMap<String, String> selectionMap;

    public Session(String session_id, StateInterface state, String msisdn)
    {
        this.session_id = session_id;
        this.state = state;
        this.msisdn = msisdn;
        this.student = null;
        this.selectionMap = new HashMap<String, String>();
    }


    public void setStudent(Student student)
    {
        this.student = student;
    }

    public void setState(StateInterface state)
    {
        this.state = state;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getSession_id()
    {
        return this.session_id;
    }

    public StateInterface getState()
    {
        return this.state;
    }

    public Student getStudent()
    {
        return this.student;
    }

    public HashMap<String, String> getSelectionMap() {
        return selectionMap;
    }
}
