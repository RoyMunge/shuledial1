package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.sturgeon.sdial.common.model.SchoolBio;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 23/11/2016.
 */
public interface SchoolBioRepository extends JpaRepository<SchoolBio, Long> {
    Optional<SchoolBio> findBySchoolid(Long schoolid);

    @Transactional
    @Modifying
    void Update(Long schoolid, String physicalAddress, String postallAddress, String bio, String vacancy);
}
