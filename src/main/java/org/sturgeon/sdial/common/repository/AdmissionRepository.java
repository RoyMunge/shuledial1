package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.Admission;

import java.util.Collection;

/**
 * Created by User on 23/11/2016.
 */
public interface AdmissionRepository extends JpaRepository<Admission, Long> {
    Collection<Admission> findBySchoolid(Long schoolid);
}
