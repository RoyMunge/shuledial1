package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.AnnouncementStudentGroup;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Roy on 5/23/2017.
 */
public interface AnnouncementStudentGroupRepository extends JpaRepository<AnnouncementStudentGroup, Long> {
    Collection<AnnouncementStudentGroup> findByStudentgroupid(Long studentgroupid);
    Collection<AnnouncementStudentGroup> findBySchoolid(Long schoolid);
    Collection<Long> findByStudent(Long schoolid, ArrayList<Long> studentgroupids);
    Collection<Long> findRecentByStudent(Long schoolid, ArrayList<Long> studentgroupids);
}
