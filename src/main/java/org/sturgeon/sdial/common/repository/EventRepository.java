package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.Event;

import java.util.Collection;

/**
 * Created by User on 9/22/2015.
 */
public interface EventRepository extends JpaRepository<Event, Long> {

    Collection<Event> findBySchoolid(Long schoolid);

    int CountEvent(Long schoolid);

    Collection<Event> UpcomingEvents(Long schoolid);

    Collection<Event> findByIds(Collection<Long> eventids);

    Collection<Event> EventByName(Long schoolid, String name);
}