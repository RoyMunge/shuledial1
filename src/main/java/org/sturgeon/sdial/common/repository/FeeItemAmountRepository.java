package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;
import org.sturgeon.sdial.common.model.FeeItemAmount;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 19/11/2016.
 */
public interface FeeItemAmountRepository extends JpaRepository<FeeItemAmount, Long>{
    @Transactional
    @Modifying
    void UpdateAmount(Long feeitemamountid, String amount);
    Optional<FeeItemAmount> getFeeItemAmount(Long feeitemid,Long schoolperiodid, Long schoollevelid);
    Collection<FeeItemAmount> getByItemandSchoolLevel(Long feeitemid, Long schoollevel);
    Collection<FeeItemAmount> getBySchoolPeriodandSchoolLevel(Long schoolperiodid, Long schoollevelid);
}
