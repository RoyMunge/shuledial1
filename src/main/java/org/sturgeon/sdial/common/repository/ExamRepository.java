package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.Exam;

import java.util.Collection;

/**
 * Created by User on 9/22/2015.
 */
public interface ExamRepository extends JpaRepository<Exam, Long> {

    Collection<Exam> findBySchoolid(Long schoolid);

    Collection<Exam> findRecent(Long schoolid);

    int CountExam(Long schoolid);
}