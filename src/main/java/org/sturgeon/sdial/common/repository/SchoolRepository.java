package org.sturgeon.sdial.common.repository;


        import org.springframework.data.jpa.repository.JpaRepository;
        import org.springframework.data.jpa.repository.Modifying;
        import org.sturgeon.sdial.common.model.School;

        import javax.transaction.Transactional;
        import java.util.Collection;
        import java.util.Optional;

public interface SchoolRepository extends JpaRepository<School, Long> {

    Optional<School> findOneByShortName(String shortname);

    Optional<School> findOneByName(String schoolname);

    Collection<School> searchSchools(String searchstring);

    @Transactional
    @Modifying
    void Update(Long schoolid, String website, String motto, String physicalAddress, String postalAddress, String bio, String admissionInfo, String feePaymentInfo, String overview);
}

