package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.SchoolLevel;

import java.util.Collection;

/**
 * Created by User on 18/11/2016.
 */
public interface SchoolLevelRepository extends JpaRepository<SchoolLevel, Long> {
    int CountSchoolLevel(Long schoolid);
    Collection<SchoolLevel> findBySchoolid(Long schoolid);
}
