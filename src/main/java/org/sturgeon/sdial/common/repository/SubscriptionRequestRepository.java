package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.SubscriptionRequest;

/**
 * Created by Roy on 5/9/2017.
 */
public interface SubscriptionRequestRepository extends JpaRepository<SubscriptionRequest, Long> {
}
