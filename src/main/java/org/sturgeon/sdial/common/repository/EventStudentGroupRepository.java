package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.EventStudentGroup;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Roy on 5/23/2017.
 */
public interface EventStudentGroupRepository extends JpaRepository<EventStudentGroup, Long> {
    Collection<EventStudentGroup> findByStudentgroupid(Long studentgroupid);
    Collection<EventStudentGroup> findBySchoolid(Long schoolid);
    Collection<Long> findByStudent(Long schoolid, ArrayList<Long> studentgroupids);
    Collection<Long> findUpcomingByStudent(Long schoolid, ArrayList<Long> studentgroupids);
}
