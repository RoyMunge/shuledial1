package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.SchoolType;

/**
 * Created by User on 18/11/2016.
 */
public interface SchoolTypeRepository extends JpaRepository<SchoolType, Long> {

}
