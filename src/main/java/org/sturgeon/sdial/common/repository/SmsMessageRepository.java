package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.SmsMessage;

/**
 * Created by User on 11/04/2017.
 */
public interface SmsMessageRepository extends JpaRepository<SmsMessage, Long> {

}
