package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.sturgeon.sdial.common.model.Subscriber;
import org.sturgeon.sdial.ussd.states.StateInterface;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 13/05/2016.
 */
public interface SubscriberRepository extends JpaRepository<Subscriber, Long> {

    Optional<Subscriber> findByPhoneNumber(String phoneNumber);

    Optional<Subscriber> findOneByEmail (String userEmail);

   Optional<Subscriber> findByPhoneNumberAndPin(String phoneNumber, String pin);

    @Transactional
    @Modifying
   void updateDeviceToken(String phoneNumber, String deviceToken);
}
