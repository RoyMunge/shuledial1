package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.SmsContactGroup;

import java.util.Collection;

/**
 * Created by User on 01/04/2017.
 */
public interface SmsContactGroupRepository extends JpaRepository<SmsContactGroup, Long> {
    Collection<SmsContactGroup> findBySchoolid(Long schoolid);
    int CountSmsContactGroup(Long schoolid);
}
