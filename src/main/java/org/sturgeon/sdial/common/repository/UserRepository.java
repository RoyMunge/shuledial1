package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.PortalUser;

import java.util.Optional;

public interface UserRepository extends JpaRepository<PortalUser, Long> {

    Optional<PortalUser> findOneByEmail(String email);
}
