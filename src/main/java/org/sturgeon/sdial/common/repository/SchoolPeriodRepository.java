package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.SchoolPeriod;

import java.util.Collection;

/**
 * Created by User on 18/11/2016.
 */
public interface SchoolPeriodRepository extends JpaRepository<SchoolPeriod, Long> {
    int CountSchoolPeriod(Long schoolid);
    Collection<SchoolPeriod> findBySchoolid(Long schoolid);
}
