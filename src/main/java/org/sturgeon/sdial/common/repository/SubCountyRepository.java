package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.SubCounty;

/**
 * Created by User on 19/01/2017.
 */
public interface SubCountyRepository extends JpaRepository<SubCounty, Long> {
}
