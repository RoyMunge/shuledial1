package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;
import org.sturgeon.sdial.common.model.SchoolLevelPeriodTotal;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 19/11/2016.
 */
public interface SchoolLevelPeriodTotalRepository extends JpaRepository<SchoolLevelPeriodTotal, Long> {
    @Transactional
    @Modifying
    void UpdateAmount(Long schoollevelperiodtotalid, String amount);
    Collection<SchoolLevelPeriodTotal> getBySchoolLevel(Long SchoolLevel);
    Optional<SchoolLevelPeriodTotal> getSchoolLevelPeriodTotal(Long schoolperiodid, Long schoollevelid);

}
