package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.SmsContact;
import org.sturgeon.sdial.common.model.SmsContactGroup;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 01/04/2017.
 */
public interface SmsContactRepository extends JpaRepository<SmsContact, Long> {
    Collection<SmsContact> findBySchoolid(Long schoolid);
    int CountSmsContact (Long schoolid);
    Optional<SmsContact> findByPhoneNumber(String phoneNumber, Long schoolid);
}
