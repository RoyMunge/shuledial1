package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.Student;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 9/22/2015.
 */
public interface StudentRepository extends JpaRepository<Student, Long> {

    Optional<Student> findBySchoolidAndAdmission(Long schoolid, String admission);

    Collection<Student> findBySchoolid(Long schoolid);

    int CountStudent(Long schoolid);

}
