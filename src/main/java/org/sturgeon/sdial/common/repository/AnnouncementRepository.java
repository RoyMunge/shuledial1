package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.Announcement;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by User on 9/22/2015.
 */
public interface AnnouncementRepository extends JpaRepository<Announcement, Long> {

    Collection<Announcement> findBySchoolid(Long schoolid);

    Collection<Announcement> findByIds(Collection<Long> announcementids);

    int Count(Long schoolid);
}