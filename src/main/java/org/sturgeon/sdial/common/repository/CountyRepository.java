package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.County;

/**
 * Created by User on 19/01/2017.
 */
public interface CountyRepository extends JpaRepository<County, Long> {
    
}
