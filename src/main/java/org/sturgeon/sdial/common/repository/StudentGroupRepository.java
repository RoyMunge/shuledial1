package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.Student;
import org.sturgeon.sdial.common.model.StudentGroup;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 14/03/2017.
 */
public interface StudentGroupRepository extends JpaRepository<StudentGroup, Long> {
    Collection<StudentGroup> findBySchoolid(Long schoolid);
}
