package org.sturgeon.sdial.common.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.SubscriberStudent;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 31/03/2017.
 */
public interface SubscriberStudentRepository extends JpaRepository<SubscriberStudent, Long> {
    int CountSubscriber(Long schoolid);
    Collection<SubscriberStudent> findBySubscriberid(Long subscriberid);
    Collection<SubscriberStudent> findByStudentid(Long studentid);
    Collection<SubscriberStudent> findPrimaryByStudentid(Long studentid);
    Optional<SubscriberStudent> findByIds(Long subscriberid, Long studentid);
    void deleteByStudentid(Long studentid);
    void deleteByStudentidAndSubscriberid(Long studentid,Long subscriberid);
}