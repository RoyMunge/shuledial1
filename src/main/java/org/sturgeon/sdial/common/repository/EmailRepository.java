package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.Email;

/**
 * Created by User on 25/01/2017.
 */
public interface EmailRepository extends JpaRepository<Email, Long> {
}
