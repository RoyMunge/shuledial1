package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.SchoolLevelPeriod;

/**
 * Created by User on 19/11/2016.
 */
public interface SchoolLevelPeriodRepository extends JpaRepository<SchoolLevelPeriod, Long> {
}
