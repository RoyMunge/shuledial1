package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.sturgeon.sdial.common.model.FeeBalance;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * Created by User on 23/11/2016.
 */
public interface FeeBalanceRepository extends JpaRepository<FeeBalance, Long> {
    Optional<FeeBalance> findByStudentid(Long studentid);
    void deleteByStudentid(Long studentid);

}
