package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.FeeItem;

import java.util.Collection;

/**
 * Created by User on 19/11/2016.
 */
public interface FeeItemRepository extends JpaRepository<FeeItem, Long> {
    int CountFeeItem(Long schoolid);
    Collection<FeeItem> findBySchoolid(Long schoolid);
}
