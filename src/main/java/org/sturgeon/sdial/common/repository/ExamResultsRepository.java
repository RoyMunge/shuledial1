package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.ExamResults;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 9/22/2015.
 */
public interface ExamResultsRepository extends JpaRepository<ExamResults, Long> {

    Collection<ExamResults> findByStudentid(Long studentid);

    Collection<ExamResults> findByExamid(Long examid);


    Optional<ExamResults> findByStudentidAndExamid(Long studentid, Long examid);

    void deleteByStudentid(Long studentid);
}
