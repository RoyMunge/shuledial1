package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.SchoolReferral;

/**
 * Created by Roy on 5/29/2017.
 */
public interface SchoolReferralRepository extends JpaRepository<SchoolReferral, Long> {

}
