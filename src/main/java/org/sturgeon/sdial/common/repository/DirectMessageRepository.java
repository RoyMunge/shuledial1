package org.sturgeon.sdial.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.sturgeon.sdial.common.model.DirectMessage;

/**
 * Created by User on 23/04/2017.
 */
public interface DirectMessageRepository extends JpaRepository<DirectMessage, Long> {

}
