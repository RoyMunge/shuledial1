package org.sturgeon.sdial.common.service.school;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.model.SchoolLevel;
import org.sturgeon.sdial.common.model.forms.SchoolLevelForm;
import org.sturgeon.sdial.common.repository.SchoolLevelRepository;

import java.util.Collection;

/**
 * Created by User on 18/11/2016.
 */
@Service
public class SchoolLevelServiceImpl implements SchoolLevelService {

    SchoolLevelRepository schoolLevelRepository;

    @Autowired
    public SchoolLevelServiceImpl(SchoolLevelRepository schoolLevelRepository) {
        this.schoolLevelRepository = schoolLevelRepository;
    }

    @Override
    public int count(Long schoolid) {
        return schoolLevelRepository.CountSchoolLevel(schoolid);
    }

    @Override
    public SchoolLevel find(Long schoollevelid) {
        return schoolLevelRepository.findOne(schoollevelid);
    }

    @Override
    public SchoolLevel create(SchoolLevelForm schoolLevelForm) {
        SchoolLevel schoolLevel = new SchoolLevel();
        schoolLevel.setSchoolLevelName(schoolLevelForm.getSchoolLevelName());
        schoolLevel.setSchoolid(schoolLevelForm.getSchoolid());
        return schoolLevelRepository.save(schoolLevel);
    }

    @Override
    public Collection<SchoolLevel> getAllSchoolLevels(Long schoolId) {
        return schoolLevelRepository.findBySchoolid(schoolId);
    }
}

