package org.sturgeon.sdial.common.service.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.model.Exam;
import org.sturgeon.sdial.common.model.forms.ExamCreateForm;
import org.sturgeon.sdial.common.repository.ExamRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

/**
 * Created by User on 9/22/2015.
 */
@Service
public class ExamServiceImpl implements ExamService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolServiceImpl.class);
    ExamRepository examRepository;
    SchoolService schoolService;

    @Autowired
    public ExamServiceImpl(ExamRepository examRepository, SchoolService schoolService) {
        this.examRepository = examRepository;
        this.schoolService = schoolService;
    }

    @Override
    public Optional<Exam> getExambyId(Long examid) {
        LOGGER.debug("finding exam by id = {}", examid);
        return Optional.ofNullable(examRepository.findOne(examid));
    }

    @Override
    public Collection<Exam> getRecentExamsbySchoolId(Long school_id) {
        return examRepository.findRecent(school_id);
    }

    @Override
    public Collection<Exam> getExamsbySchoolId(Long schoolid) {
        LOGGER.debug("finding exams by school id = {}", schoolid);
        return examRepository.findBySchoolid(schoolid);
    }

    @Override
    public int count(Long schoolid){

        LOGGER.debug("count is = {}", examRepository.CountExam(schoolid));
        return examRepository.CountExam(schoolid);
    }

    @Override
    public Exam create(ExamCreateForm form) {
        Exam exam = new Exam();
        exam.setSchoolid(form.getSchoolid());
        exam.setExamDate(StringtoDate(form.getExamDate()));
        exam.setExamName(form.getExamName());
        exam.setSchool(schoolService.getSchoolById(form.getSchoolid()).get());
        return examRepository.save(exam);
    }

    private Date StringtoDate(String dateInString) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date parsedDate = formatter.parse(dateInString);
            return parsedDate;
        } catch (ParseException e) {
            LOGGER.debug("error in converting date", e);
            return null;
        }
    }

}
