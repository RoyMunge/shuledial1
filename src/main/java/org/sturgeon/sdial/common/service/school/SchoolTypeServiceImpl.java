package org.sturgeon.sdial.common.service.school;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.model.SchoolType;
import org.sturgeon.sdial.common.repository.SchoolTypeRepository;

import java.util.Collection;

/**
 * Created by User on 18/11/2016.
 */
@Service
public class SchoolTypeServiceImpl implements SchoolTypeService {

    private final SchoolTypeRepository schoolTypeRepository;

    @Autowired
    public SchoolTypeServiceImpl(SchoolTypeRepository schoolTypeRepository) {
        this.schoolTypeRepository = schoolTypeRepository;
    }

    @Override
    public Collection<SchoolType> getAllSchoolTypes() {
        return schoolTypeRepository.findAll();
    }
}
