package org.sturgeon.sdial.common.service.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.model.SchoolPeriod;
import org.sturgeon.sdial.common.model.forms.SchoolPeriodForm;
import org.sturgeon.sdial.common.repository.SchoolPeriodRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

/**
 * Created by User on 18/11/2016.
 */
@Service
public class SchoolPeriodServiceImpl implements SchoolPeriodService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolPeriodServiceImpl.class);
    SchoolPeriodRepository schoolPeriodRepository;

    @Autowired
    public SchoolPeriodServiceImpl(SchoolPeriodRepository schoolPeriodRepository) {
        this.schoolPeriodRepository = schoolPeriodRepository;
    }

    @Override
    public int count(Long schoolid) {
        return schoolPeriodRepository.CountSchoolPeriod(schoolid);
    }

    @Override
    public SchoolPeriod create(SchoolPeriodForm schoolPeriodForm) {
        SchoolPeriod schoolPeriod = new SchoolPeriod();
        schoolPeriod.setSchoolPeriodName(schoolPeriodForm.getSchoolPeriodName());
        schoolPeriod.setStartDate(StringtoDate(schoolPeriodForm.getStartDate()));
        schoolPeriod.setEndDate(StringtoDate(schoolPeriodForm.getEndDate()));
        schoolPeriod.setSchoolid(schoolPeriodForm.getSchoolid());
        return schoolPeriodRepository.save(schoolPeriod);
    }

    @Override
    public Collection<SchoolPeriod> getAllSchoolPeriods(Long schoolid) {
        return schoolPeriodRepository.findBySchoolid(schoolid);
    }

    private Date StringtoDate(String dateInString) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date parsedDate = formatter.parse(dateInString);
            return parsedDate;
        } catch (ParseException e) {
            LOGGER.debug("error in converting date", e);
            return null;
        }
    }


}
