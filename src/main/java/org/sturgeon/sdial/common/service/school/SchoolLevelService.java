package org.sturgeon.sdial.common.service.school;

import org.sturgeon.sdial.common.model.SchoolLevel;
import org.sturgeon.sdial.common.model.forms.SchoolLevelForm;

import java.util.Collection;

/**
 * Created by User on 18/11/2016.
 */
public interface SchoolLevelService {
    int count(Long schoolid);
    SchoolLevel find(Long schoollevelid);
    SchoolLevel create(SchoolLevelForm schoolLevelForm);
    Collection<SchoolLevel> getAllSchoolLevels(Long schoolId);
}
