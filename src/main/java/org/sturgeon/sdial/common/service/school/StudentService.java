package org.sturgeon.sdial.common.service.school;

import org.sturgeon.sdial.common.model.Student;
import org.sturgeon.sdial.common.model.StudentGroup;
import org.sturgeon.sdial.common.model.Subscriber;
import org.sturgeon.sdial.common.model.SubscriberStudent;
import org.sturgeon.sdial.common.model.forms.*;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

/**
 * Created by User on 9/22/2015.
 */
public interface StudentService {

    Optional<Student> getStudentbyId(Long student_id);

    Optional<Student> getStudentbyAdmission(Long school_id, String admission);

    Student create(StudentCreateForm form);

    Student editStudent(StudentEditForm form);

    void deleteStudents(String studentsToDelete);

    Student AddStudentWithGroup(StudentCreateForm form);

    int count(Long schoolid);

    StudentGroup createStudentGroup(StudentGroupCreateForm form);

    Student AddStudentToGroup(StudentGroupAddForm form);

    void AddStudentsToGroups(String studentsToAdd, String groupsToAddTo);

    void AddStudentsToNewGroup(StudentGroupCreateForm studentGroupCreateForm, String studentsToAdd);

    void MoveStudentsToGroups(String studentsToMove, String groupsToMoveTo, Long currentGroup);

    void MoveStudentsToNewGroup(StudentGroupCreateForm studentGroupCreateForm, String studentsToMove, Long currentGroup);

    void RemoveStudentFromGroup(String studentsToRemove, Long currentGroup);

    StudentGroup editStudentGroup(StudentGroupCreateForm form);

    void deleteStudentGroup(Long studentgroupid);

    Student DeleteStudentFromGroup(StudentGroupAddForm form);

    Collection<Student> getStudentsbySchool(Long schoolid);

    Set<Student> getStudentsbyGroup(Long studentgroupid);

    Subscriber createSubscriber(ParentAddForm form);

    int SubscriberCount(Long schoolid);

    Collection<SubscriberStudent> findByStudentid(Long studentid);

    Collection<SubscriberStudent> findBySubscriberid(Long subscriberid);

    Collection<SubscriberStudent> findPrimaryByStudentid(Long Studentid);

    Optional<StudentGroup> findStudentGroupById(Long studentgroupid);

    Collection<Subscriber> findSubscribersbySchool(Long schoolid);

    Collection<Subscriber> findPrimarySubscribersbySchool(Long schoolid);

    Optional<Subscriber> findSubscriberById(Long subscriberid);

    Collection<Subscriber> findSubscribersbyGroup(Long studentgroupid);

    Collection<Subscriber> findPrimarySubscribersbyGroup(Long studentgroupid);

    Optional<SubscriberStudent> findbyPhoneNumberandStudentId(String PhoneNumber, Long studentid);


    Subscriber editSubscriber(ParentEditForm parentEditForm);

    void deleteSubscriber(Long studentid, Long subscriberid);
}
