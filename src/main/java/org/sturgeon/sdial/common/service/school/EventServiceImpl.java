package org.sturgeon.sdial.common.service.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.model.Event;
import org.sturgeon.sdial.common.model.EventStudentGroup;
import org.sturgeon.sdial.common.model.Student;
import org.sturgeon.sdial.common.model.StudentGroup;
import org.sturgeon.sdial.common.model.forms.EventCreateForm;
import org.sturgeon.sdial.common.repository.EventRepository;
import org.sturgeon.sdial.common.repository.EventStudentGroupRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by User on 9/22/2015.
 */
@Service
public class EventServiceImpl implements EventService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolServiceImpl.class);
    private final EventRepository eventRepository;
    private final EventStudentGroupRepository eventStudentGroupRepository;
    private final SchoolService schoolService;
    private final StudentService studentService;

    @Autowired
    public EventServiceImpl(EventRepository eventRepository, SchoolService schoolService, EventStudentGroupRepository eventStudentGroupRepository, StudentService studentService) {
        this.eventRepository = eventRepository;
        this.eventStudentGroupRepository = eventStudentGroupRepository;
        this.schoolService = schoolService;
        this.studentService = studentService;
    }

    @Override
    public Optional<Event> getEventbyId(Long eventid) {
        LOGGER.debug("finding event by id = {}", eventid);
        return Optional.ofNullable(eventRepository.findOne(eventid));
    }

    @Override
    public Collection<Event> getEventsbySchoolId(Long schoolid) {
        LOGGER.debug("finding events by school id = {}", schoolid);
        return eventRepository.findBySchoolid(schoolid);
    }

    @Override
    public int count(Long schoolid) {

        LOGGER.debug("count is = {}", eventRepository.CountEvent(schoolid));
        return eventRepository.CountEvent(schoolid);
    }

    @Override
    public Event create(EventCreateForm form) {

        if (form.getRecipients() == null || form.getRecipients().isEmpty()) {
            Event event = new Event();
            event.setSchoolid(form.getSchoolid());
            event.setEventName(form.getEventName());
            event.setEventDate(StringtoDate(form.getEventDate()));
            event.setLocation(form.getLocation());
            event.setEndDate(StringtoDate(form.getEndDate()));
            event.setDescription(form.getDescription());
            event.setSchool(schoolService.getSchoolById(form.getSchoolid()).get());
            Event savedEvent = eventRepository.save(event);

            EventStudentGroup eventStudentGroup = new EventStudentGroup();
            eventStudentGroup.setEventid(savedEvent.getEventid());
            eventStudentGroup.setSchoolid(savedEvent.getSchoolid());
            eventStudentGroup.setStudentgroupid(null);
            eventStudentGroup.setEventDate(StringtoDate(form.getEndDate()));
            eventStudentGroupRepository.save(eventStudentGroup);

        } else {
            Event event = new Event();
            event.setSchoolid(form.getSchoolid());
            event.setEventName(form.getEventName());
            event.setEventDate(StringtoDate(form.getEventDate()));
            event.setLocation(form.getLocation());
            event.setEndDate(StringtoDate(form.getEndDate()));
            event.setDescription(form.getDescription());
            event.setSchool(schoolService.getSchoolById(form.getSchoolid()).get());
            Event savedEvent = eventRepository.save(event);

            for (String recipient : form.getRecipients()) {
                EventStudentGroup eventStudentGroup = new EventStudentGroup();
                eventStudentGroup.setEventid(savedEvent.getEventid());
                eventStudentGroup.setSchoolid(savedEvent.getSchoolid());
                eventStudentGroup.setStudentgroupid(Long.parseLong(recipient));
                eventStudentGroup.setEventDate(StringtoDate(form.getEndDate()));
                eventStudentGroupRepository.save(eventStudentGroup);
            }
        }
        return null;
    }


    @Override
    public Collection<Event> getUpcomingEvents(Long schoolid) {
        return eventRepository.UpcomingEvents(schoolid);
    }

    @Override
    public Collection<Event> getByName(Long schoolid, String name) {
        String searchName = "%" + name + "%";
        return eventRepository.EventByName(schoolid, searchName);
    }

    @Override
    public Collection<Event> getEventsByStudent(Long studentid) {
        Optional<Student> student = studentService.getStudentbyId(studentid);
        if (student.isPresent()) {
            Set<StudentGroup> studentGroups = student.get().getStudentGroupSet();
            ArrayList<Long> query = new ArrayList<Long>();
            if (studentGroups != null || !studentGroups.isEmpty()) {

                for (StudentGroup studentGroup : studentGroups) {
                    query.add(studentGroup.getStudentgroupid());
                }

            }
            Collection<Long> eventids = eventStudentGroupRepository.findByStudent(student.get().getSchoolid(), query);
            if (!eventids.isEmpty()) {
                return eventRepository.findByIds(eventids);
            } else {
                return null;
            }


        } else {
            return null;
        }
    }

    @Override
    public Collection<Event> getUpcomingEventsByStudent(Long studentid) {
        Optional<Student> student = studentService.getStudentbyId(studentid);
        if (student.isPresent()) {
            Set<StudentGroup> studentGroups = student.get().getStudentGroupSet();
            ArrayList<Long> query = new ArrayList<Long>();
            if (studentGroups != null || !studentGroups.isEmpty()) {
                for (StudentGroup studentGroup : studentGroups) {
                    query.add(studentGroup.getStudentgroupid());
                }
            }
            Collection<Long> eventids = eventStudentGroupRepository.findUpcomingByStudent(student.get().getSchoolid(), query);
            if (!eventids.isEmpty()) {
                return eventRepository.findByIds(eventids);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private Date StringtoDate(String dateInString) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        try {
            Date parsedDate = formatter.parse(dateInString);
            return parsedDate;
        } catch (ParseException e) {
            LOGGER.debug("error in converting date", e);
            return null;
        }
    }
}
