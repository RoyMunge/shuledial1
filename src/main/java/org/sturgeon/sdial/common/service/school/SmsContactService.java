package org.sturgeon.sdial.common.service.school;

import org.sturgeon.sdial.common.model.SmsContact;
import org.sturgeon.sdial.common.model.SmsContactGroup;
import org.sturgeon.sdial.common.model.forms.SmsContactCreateForm;
import org.sturgeon.sdial.common.model.forms.SmsContactGroupCreateForm;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 01/04/2017.
 */
public interface SmsContactService {
    SmsContactGroup createSmsContactGroup(SmsContactGroupCreateForm form);
    SmsContact createSmsContact(SmsContactCreateForm form);
    int smsContactCount(Long schoolid);
    int smsContactGroupCount(Long schoolid);
    Collection<SmsContact> findSmsContactBySchool(Long schoolid);
    Collection<SmsContactGroup> findSmsContactGroupsBySchool(Long schoolid);
    SmsContactGroup findSmsContactGroup(Long smscontactgroupid);

}
