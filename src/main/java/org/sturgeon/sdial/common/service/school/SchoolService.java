package org.sturgeon.sdial.common.service.school;


import org.sturgeon.sdial.common.model.*;
import org.sturgeon.sdial.common.model.forms.*;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface SchoolService {

    Optional<School> getSchoolById(Long school_id);

    Optional<School> getSchoolByShortName(String shortName);

    Optional<School> getSchoolByName(String schoolName);

    Collection<School> getAllSchools();

    School create(SchoolCreateForm schoolForm);

    Collection<School> searchSchools(String searchstring);

    void updateSchoolBio(SchoolBioForm schoolBioForm);

    Admission createAdmission(AdmissionForm admissionForm);

    Collection<Admission> getAdmissionsBySchoolId(Long schoolid);

    Set<School> getSchoolsByType(Long schooltypeid);

    Collection<County> getAllCounties();

    Collection<SchoolType> findAllSchoolCategories();

    County getCountyById(Long countyid);

    Collection<QualificationLevel> getQualificationLevels();

    Collection<School> getSchoolsBySubCounty(Long subcountyid);

    Collection<School> getSchoolsByCounty(Long countyid);

    void addPhoneNumber(PhoneNumberAddForm form);

    void addEmail(EmailAddForm form);

    void addSchoolType(SchoolTypeAddForm form);

    Collection<SubCounty> getAllSubCounties();

}