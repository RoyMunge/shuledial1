package org.sturgeon.sdial.common.service.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.model.ExamResults;
import org.sturgeon.sdial.common.model.forms.ExamResultsForm;
import org.sturgeon.sdial.common.repository.ExamResultsRepository;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 9/22/2015.
 */
@Service
public class ExamResultsServiceImpl implements ExamResultsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolServiceImpl.class);
    ExamResultsRepository examResultsRepository;
    StudentService studentService;
    ExamService examService;

    @Autowired
    public ExamResultsServiceImpl(ExamResultsRepository examResultRepository, StudentService studentService, ExamService examService) {
        this.examResultsRepository = examResultRepository;
        this.studentService = studentService;
        this.examService = examService;
    }

    @Override
    public Optional<ExamResults> getExamResultsById(Long examid) {
        LOGGER.debug("getting exam results by id = {}",examid);
        return Optional.ofNullable(examResultsRepository.findOne(examid));
    }

    @Override
    public Collection<ExamResults> getExamResultsByExamId(Long examid) {
        LOGGER.debug("getting exam results by exam id = {}",examid);
        return examResultsRepository.findByExamid(examid);
    }

    @Override
    public Collection<ExamResults> getExamResultsByStudentId(Long studentid) {
        LOGGER.debug("getting exam results by student id = {}",studentid);
        return examResultsRepository.findByStudentid(studentid);
    }

    @Override
    public Optional<ExamResults> getExamResultsByExamAndStudentId(Long examid, Long studentid) {
        LOGGER.debug("getting exam results by exam id = {} and student id = {}",examid, studentid);
        return examResultsRepository.findByStudentidAndExamid(studentid,examid);
    }

    @Override
    public ExamResults create(ExamResultsForm form) {
        ExamResults examResults = new ExamResults();
        examResults.setExamid(form.getExamid());
        examResults.setStudentid(form.getStudentid());
        examResults.setTotal(form.getTotal());
        examResults.setAverage(form.getAverage());
        examResults.setGrade(form.getGrade());
        examResults.setPosition(form.getPosition());
        examResults.setEnglish(form.getEnglish());
        examResults.setKiswahili(form.getKiswahili());
        examResults.setMaths(form.getMaths());
        examResults.setScience(form.getScience());
        examResults.setBiology(form.getBiology());
        examResults.setChemistry(form.getChemistry());
        examResults.setPhysics(form.getPhysics());
        examResults.setHistory(form.getHistory());
        examResults.setGeography(form.getGeography());
        examResults.setCre(form.getCre());
        examResults.setIre(form.getIre());
        examResults.setHre(form.getHre());
        examResults.setAgriculture(form.getAgriculture());
        examResults.setArt(form.getArt());
        examResults.setAviation(form.getAviation());
        examResults.setHomescience(form.getHomescience());
        examResults.setBusinessstudies(form.getBusinessstudies());
        examResults.setSocialstudies(form.getSocialstudies());
        examResults.setSsre(form.getSsre());
        examResults.setArabic(form.getArabic());
        examResults.setFrench(form.getFrench());
        examResults.setGerman(form.getGerman());
        examResults.setMusic(form.getMusic());
        examResults.setStudent(studentService.getStudentbyId(form.getStudentid()).get());
        examResults.setExam(examService.getExambyId(form.getExamid()).get());
        return examResultsRepository.save(examResults);
    }
}

