package org.sturgeon.sdial.common.service.school;

import org.sturgeon.sdial.common.model.ExamResults;
import org.sturgeon.sdial.common.model.forms.ExamResultsForm;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 20/06/2016.
 */
public interface ExamResultsService {
    Optional<ExamResults> getExamResultsById(Long examsecondary_id);

    Collection<ExamResults> getExamResultsByExamId(Long exam_id);

    Collection<ExamResults> getExamResultsByStudentId(Long student_id);

    Optional<ExamResults> getExamResultsByExamAndStudentId(Long exam_id, Long student_id);

    ExamResults create(ExamResultsForm form);
}
