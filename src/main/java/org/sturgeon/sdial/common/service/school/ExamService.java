package org.sturgeon.sdial.common.service.school;

import org.sturgeon.sdial.common.model.Exam;
import org.sturgeon.sdial.common.model.forms.ExamCreateForm;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 9/22/2015.
 */
public interface ExamService {

    Optional<Exam> getExambyId(Long exam_id);

    Collection<Exam> getRecentExamsbySchoolId(Long school_id);

    Collection<Exam> getExamsbySchoolId(Long school_id);

    int count(Long schoolid);

    Exam create(ExamCreateForm form);
}
