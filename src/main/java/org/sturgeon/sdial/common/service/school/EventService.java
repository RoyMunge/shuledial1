package org.sturgeon.sdial.common.service.school;

import org.sturgeon.sdial.common.model.Event;
import org.sturgeon.sdial.common.model.EventStudentGroup;
import org.sturgeon.sdial.common.model.forms.EventCreateForm;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 9/22/2015.
 */
public interface EventService {

    Optional<Event> getEventbyId(Long event_id);

    Collection<Event> getEventsbySchoolId(Long schoolid);

    int count(Long schoolid);

    Event create(EventCreateForm form);

    Collection<Event> getUpcomingEvents(Long schoolid);

    Collection<Event> getByName(Long schoolid, String name);

    Collection<Event> getEventsByStudent(Long studentid);

    Collection<Event> getUpcomingEventsByStudent(Long studentid);
}
