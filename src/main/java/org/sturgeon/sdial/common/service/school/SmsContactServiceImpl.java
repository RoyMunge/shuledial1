package org.sturgeon.sdial.common.service.school;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.model.SmsContact;
import org.sturgeon.sdial.common.model.SmsContactGroup;
import org.sturgeon.sdial.common.model.forms.SmsContactCreateForm;
import org.sturgeon.sdial.common.model.forms.SmsContactGroupCreateForm;
import org.sturgeon.sdial.common.repository.SmsContactGroupRepository;
import org.sturgeon.sdial.common.repository.SmsContactRepository;

import java.util.*;

/**
 * Created by User on 01/04/2017.
 */
@Service
public class SmsContactServiceImpl implements SmsContactService {

    SmsContactRepository smsContactRepository;
    SmsContactGroupRepository smsContactGroupRepository;

    @Autowired
    public SmsContactServiceImpl(SmsContactRepository smsContactRepository, SmsContactGroupRepository smsContactGroupRepository) {
        this.smsContactRepository = smsContactRepository;
        this.smsContactGroupRepository = smsContactGroupRepository;
    }

    private String processPhoneNumber(String phoneNumber){
        String intlPhoneNumber;
        if (phoneNumber.startsWith("07")){
            intlPhoneNumber = phoneNumber.replace("07","2547");
            return intlPhoneNumber;
        } else if(phoneNumber.startsWith("+")){
            intlPhoneNumber = phoneNumber.replace("+","");
            return intlPhoneNumber;
        } else {
            return phoneNumber;
        }
    }

    @Override
    public SmsContactGroup createSmsContactGroup(SmsContactGroupCreateForm form) {
        SmsContactGroup smsContactGroup = new SmsContactGroup();
        smsContactGroup.setSchoolid(form.getSchoolid());
        smsContactGroup.setName(form.getName());
        smsContactGroup.setDescription(form.getDescription());
        return smsContactGroupRepository.save(smsContactGroup);
    }

    @Override
    public SmsContact createSmsContact(SmsContactCreateForm form) {
        String phoneNumber = processPhoneNumber(form.getPhoneNumber());
        Optional<SmsContact> smsContact = smsContactRepository.findByPhoneNumber(phoneNumber, form.getSchoolid());
        SmsContactGroup smsContactGroup = smsContactGroupRepository.findOne(form.getSmscontactgroupid());

        if (smsContactGroup != null){
            if(smsContact.isPresent()){
                SmsContact smsContactChange = smsContact.get();
                smsContactChange.getSmsContactGroupSet().add(smsContactGroup);
                return smsContactRepository.save(smsContactChange);
            } else {
                SmsContact newSmsContact = new SmsContact();
                newSmsContact.setSchoolid(form.getSchoolid());
                newSmsContact.setPhoneNumber(form.getPhoneNumber());
                newSmsContact.setName(form.getName());
                Set<SmsContactGroup> smsContactGroupSet = new HashSet<SmsContactGroup>();
                smsContactGroupSet.add(smsContactGroup);
                newSmsContact.setSmsContactGroupSet(smsContactGroupSet);
                return smsContactRepository.save(newSmsContact);
        }
        } else {
            return null;
        }
    }

    @Override
    public int smsContactCount(Long schoolid) {
        return smsContactRepository.CountSmsContact(schoolid);
    }

    @Override
    public int smsContactGroupCount(Long schoolid) {
        return smsContactGroupRepository.CountSmsContactGroup(schoolid);
    }

    @Override
    public Collection<SmsContact> findSmsContactBySchool(Long schoolid) {
        return smsContactRepository.findBySchoolid(schoolid);
    }

    @Override
    public Collection<SmsContactGroup> findSmsContactGroupsBySchool(Long schoolid) {
        return smsContactGroupRepository.findBySchoolid(schoolid);
    }

    @Override
    public SmsContactGroup findSmsContactGroup(Long smscontactgroupid) {
        return smsContactGroupRepository.findOne(smscontactgroupid);
    }


}
