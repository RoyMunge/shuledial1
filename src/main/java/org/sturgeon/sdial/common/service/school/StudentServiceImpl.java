package org.sturgeon.sdial.common.service.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.sturgeon.sdial.common.model.*;
import org.sturgeon.sdial.common.model.forms.*;
import org.sturgeon.sdial.common.repository.*;
import org.sturgeon.sdial.sms.service.SmsService;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by User on 9/22/2015.
 */
@Service
public class StudentServiceImpl implements StudentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolServiceImpl.class);
    StudentRepository studentRepository;
    StudentGroupRepository studentGroupRepository;
    SchoolService schoolService;
    SubscriberStudentRepository subscriberStudentRepository;
    SubscriberRepository subscriberRepository;
    SmsService smsService;
    FeeBalanceRepository feeBalanceRepository;
    ExamResultsRepository examResultsRepository;
    SchoolRepository schoolRepository;

    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository, StudentGroupRepository studentGroupRepository, SchoolService schoolService, SubscriberStudentRepository subscriberStudentRepository, SubscriberRepository subscriberRepository, SmsService smsService, FeeBalanceRepository feeBalanceRepository, ExamResultsRepository examResultsRepository, SchoolRepository schoolRepository) {
        this.studentRepository = studentRepository;
        this.studentGroupRepository = studentGroupRepository;
        this.schoolService = schoolService;
        this.subscriberStudentRepository = subscriberStudentRepository;
        this.subscriberRepository = subscriberRepository;
        this.smsService = smsService;
        this.feeBalanceRepository = feeBalanceRepository;
        this.examResultsRepository = examResultsRepository;
        this.schoolRepository = schoolRepository;
    }

    @PostConstruct
    public void init() {
        smsService.setStudentService(this);
    }

    @Override
    public Optional<Student> getStudentbyId(Long studentid) {
        LOGGER.debug("getting student by id = {}", studentid);
        return Optional.ofNullable(studentRepository.findOne(studentid));
    }

    @Override
    public Optional<Student> getStudentbyAdmission(Long schoolid, String admission) {
        LOGGER.debug("getting student by schoolid = {} and admission number = {}", schoolid, admission);
        return studentRepository.findBySchoolidAndAdmission(schoolid, admission.toUpperCase());
    }

    @Override
    public Student create(StudentCreateForm form) {
        Student student = new Student();
        student.setSchoolid(form.getSchoolid());
        student.setSchool(schoolService.getSchoolById(form.getSchoolid()).get());
        student.setAdmission(form.getAdmission().toUpperCase());
        student.setStudentName(form.getStudentName().toUpperCase());

        //student.setStatus(true);
        return studentRepository.save(student);
    }

    @Override
    public Student editStudent(StudentEditForm form) {
        Student student = studentRepository.findOne(form.getStudentid());
        if (student != null) {
            student.setAdmission(form.getAdmission());
            student.setStudentName(form.getStudentName());

            return studentRepository.save(student);
        } else {
            return null;
        }

    }

    @Override
    @Transactional
    public void deleteStudents(String studentsToDelete) {
        List<String> studentids = Arrays.asList(studentsToDelete.split("\\s*,\\s*"));
        for (String sttudentid : studentids) {
            Long id = Long.parseLong(sttudentid);
            subscriberStudentRepository.deleteByStudentid(id);
            feeBalanceRepository.deleteByStudentid(id);
            examResultsRepository.deleteByStudentid(id);
            studentRepository.delete(id);
        }
    }

    @Override
    public Student AddStudentWithGroup(StudentCreateForm form) {
        Optional<Student> student = studentRepository.findBySchoolidAndAdmission(form.getSchoolid(), form.getAdmission());

        if (form.getStudentgroupid() == null) {
            if (student.isPresent()) {
                return student.get();
            } else {
                Student newStudent = new Student();
                newStudent.setSchoolid(form.getSchoolid());
                newStudent.setAdmission(form.getAdmission().toUpperCase());
                newStudent.setStudentName(form.getStudentName().toUpperCase());
                return studentRepository.save(newStudent);
            }
        } else {
            List<String> groupids = Arrays.asList(form.getStudentgroupid().split("\\s*,\\s*"));
            Student student1;

            if (student.isPresent()) {
                student1 = student.get();
            } else {
                Student newStudent = new Student();
                newStudent.setSchoolid(form.getSchoolid());
                newStudent.setAdmission(form.getAdmission().toUpperCase());
                newStudent.setStudentName(form.getStudentName().toUpperCase());
                Student savedStudent = studentRepository.save(newStudent);
                student1 = savedStudent;
            }

            for (String groupid : groupids) {
                StudentGroup studentGroup = studentGroupRepository.findOne(Long.parseLong(groupid));
                if (studentGroup != null) {
                    studentGroup.getStudentSet().add(student1);
                    studentGroupRepository.save(studentGroup);

                }
            }

            return student1;
        }
    }

    @Override
    public int count(Long schoolid) {

        LOGGER.debug("count is = {}", studentRepository.CountStudent(schoolid));
        return studentRepository.CountStudent(schoolid);
    }

    @Override
    public StudentGroup createStudentGroup(StudentGroupCreateForm form) {
        StudentGroup studentGroup = new StudentGroup();
        studentGroup.setSchoolid(form.getSchoolid());
        studentGroup.setName(form.getName());
        studentGroup.setDescription(form.getDescription());
        studentGroup.setStudentSet(new HashSet<Student>());

        return studentGroupRepository.save(studentGroup);
    }

    @Override
    public Student AddStudentToGroup(StudentGroupAddForm form) {

        StudentGroup studentGroup = studentGroupRepository.findOne(form.getStudentgroupid());
        Optional<Student> student = studentRepository.findBySchoolidAndAdmission(form.getSchoolid(), form.getAdmission());

        if (studentGroup != null && student.isPresent()) {
            studentGroup.getStudentSet().add(student.get());
            studentGroupRepository.save(studentGroup);
            return student.get();
        } else {
            return null;
        }
    }

    @Override
    public void AddStudentsToGroups(String studentsToAdd, String groupsToAddTo) {
        List<String> students = Arrays.asList(studentsToAdd.split("\\s*,\\s*"));
        List<String> groups = Arrays.asList(groupsToAddTo.split("\\s*,\\s*"));

        List<Student> studentList = new ArrayList<Student>();
        for (String student : students) {
            Student toAdd = studentRepository.findOne(Long.parseLong(student));
            if (toAdd != null) {
                studentList.add(toAdd);
            }
        }

        for (String group : groups) {
            StudentGroup studentGroup = studentGroupRepository.findOne(Long.parseLong(group));
            if (studentGroup != null) {
                studentGroup.getStudentSet().addAll(studentList);
                studentGroupRepository.save(studentGroup);
            }
        }

    }

    @Override
    public void AddStudentsToNewGroup(StudentGroupCreateForm studentGroupCreateForm, String studentsToAdd) {
        List<String> students = Arrays.asList(studentsToAdd.split("\\s*,\\s*"));

        StudentGroup studentGroup = createStudentGroup(studentGroupCreateForm);

        List<Student> studentList = new ArrayList<Student>();
        for (String student : students) {
            Student toAdd = studentRepository.findOne(Long.parseLong(student));
            if (toAdd != null) {
                studentList.add(toAdd);
            }
        }

        if (studentGroup != null) {
            studentGroup.getStudentSet().addAll(studentList);
            studentGroupRepository.save(studentGroup);
        }

    }

    @Override
    public void MoveStudentsToGroups(String studentsToMove, String groupsToMoveTo, Long currentGroup) {

        List<String> students = Arrays.asList(studentsToMove.split("\\s*,\\s*"));
        List<String> groups = Arrays.asList(groupsToMoveTo.split("\\s*,\\s*"));

        List<Student> studentList = new ArrayList<Student>();
        for (String student : students) {
            Student toAdd = studentRepository.findOne(Long.parseLong(student));
            if (toAdd != null) {
                studentList.add(toAdd);
            }
        }

        StudentGroup toRemoveFrom = studentGroupRepository.findOne(currentGroup);
        if (toRemoveFrom != null) {
            toRemoveFrom.getStudentSet().removeAll(studentList);
            studentGroupRepository.save(toRemoveFrom);
        }

        for (String group : groups) {
            StudentGroup studentGroup = studentGroupRepository.findOne(Long.parseLong(group));
            if (studentGroup != null) {
                studentGroup.getStudentSet().addAll(studentList);
                studentGroupRepository.save(studentGroup);
            }
        }
    }

    @Override
    public void MoveStudentsToNewGroup(StudentGroupCreateForm studentGroupCreateForm, String studentsToMove, Long currentGroup) {
        List<String> students = Arrays.asList(studentsToMove.split("\\s*,\\s*"));

        StudentGroup studentGroup = createStudentGroup(studentGroupCreateForm);

        List<Student> studentList = new ArrayList<Student>();
        for (String student : students) {
            Student toAdd = studentRepository.findOne(Long.parseLong(student));
            if (toAdd != null) {
                studentList.add(toAdd);
            }
        }

        StudentGroup toRemoveFrom = studentGroupRepository.findOne(currentGroup);
        if (toRemoveFrom != null) {
            toRemoveFrom.getStudentSet().removeAll(studentList);
            studentGroupRepository.save(toRemoveFrom);
        }

        if (studentGroup != null) {
            studentGroup.getStudentSet().addAll(studentList);
            studentGroupRepository.save(studentGroup);
        }

    }

    @Override
    public void RemoveStudentFromGroup(String studentsToRemove, Long currentGroup) {
        List<String> students = Arrays.asList(studentsToRemove.split("\\s*,\\s*"));

        List<Student> studentList = new ArrayList<Student>();
        for (String student : students) {
            Student toAdd = studentRepository.findOne(Long.parseLong(student));
            if (toAdd != null) {
                studentList.add(toAdd);
            }
        }

        StudentGroup toRemoveFrom = studentGroupRepository.findOne(currentGroup);
        if (toRemoveFrom != null) {
            toRemoveFrom.getStudentSet().removeAll(studentList);
            studentGroupRepository.save(toRemoveFrom);
        }
    }

    @Override
    public StudentGroup editStudentGroup(StudentGroupCreateForm form) {
        StudentGroup studentGroup = studentGroupRepository.findOne(form.getStudentgroupid());
        studentGroup.setName(form.getName());
        studentGroup.setDescription(form.getDescription());

        return studentGroupRepository.save(studentGroup);
    }

    @Override
    @Transactional
    public void deleteStudentGroup(Long studentgroupid) {
        StudentGroup studentGroup = studentGroupRepository.findOne(studentgroupid);
        School school = schoolRepository.findOne(studentGroup.getSchoolid());
        if (school != null) {
            school.getStudentGroupSet().remove(studentGroup);
            schoolRepository.save(school);
        }
    }

    @Override
    public Student DeleteStudentFromGroup(StudentGroupAddForm form) {
        StudentGroup studentGroup = studentGroupRepository.findOne(form.getStudentgroupid());
        Optional<Student> student = studentRepository.findBySchoolidAndAdmission(form.getSchoolid(), form.getAdmission());

        if (studentGroup != null && student.isPresent()) {
            studentGroup.getStudentSet().remove(student.get());
            studentGroupRepository.save(studentGroup);
            return student.get();
        } else {
            return null;
        }
    }

    @Override
    public Collection<Student> getStudentsbySchool(Long schoolid) {
        return studentRepository.findBySchoolid(schoolid);
    }

    @Override
    public Set<Student> getStudentsbyGroup(Long studentgroupid) {
        StudentGroup studentGroup = studentGroupRepository.findOne(studentgroupid);
        if (studentGroup != null) {
            Set<Student> students = studentGroup.getStudentSet();
            return students;
        } else {
            return null;
        }

    }

    @Override
    public Subscriber createSubscriber(ParentAddForm form) {
        Optional<Student> student = studentRepository.findBySchoolidAndAdmission(form.getSchoolid(), form.getAdmission());

        if (student.isPresent()) {
            Subscriber subscriber;
            Optional<Subscriber> existingSubscriber = subscriberRepository.findByPhoneNumber(processPhoneNumber(form.getPhoneNumber()));
            if (existingSubscriber.isPresent()) {
                subscriber = existingSubscriber.get();
            } else {
                Subscriber newSubscriber = new Subscriber();
                newSubscriber.setPhoneNumber(processPhoneNumber(form.getPhoneNumber()));
                Long twoWeeks = (new Date().getTime()) + (30L * 24L * 3600L * 1000L);
                Date date = new Date(twoWeeks);
                newSubscriber.setValidUntil(date);
                if (form.getParentName() != null) {
                    newSubscriber.setName(form.getParentName());
                }
                subscriber = subscriberRepository.save(newSubscriber);
            }
            SubscriberStudent subscriberStudent = new SubscriberStudent();
            subscriberStudent.setSubscriberid(subscriber.getSubscriberid());
            subscriberStudent.setStudentid(student.get().getStudentid());
            subscriberStudent.setSchoolid(form.getSchoolid());
            subscriberStudent.setPrimaryContact(form.getIsPrimary());
            subscriberStudentRepository.save(subscriberStudent);

            String recipients = processPhoneNumber(form.getPhoneNumber());
            SmsMessageForm smsMessageForm = new SmsMessageForm();
            smsMessageForm.setOtherRecipients(recipients);
            smsMessageForm.setText("Dear Parent, You can now access Results, Fees, Announcements and School Events with regards to your child " + student.get().getStudentName() + " by dialing *882*91# or by downloading the ShuleDial application from Android Playstore.");
            smsService.processOtherSMS(smsMessageForm);

            return subscriber;
        } else {
            if (form.getStudentName() != null) {
                Student newStudent = new Student();
                newStudent.setSchoolid(form.getSchoolid());
                newStudent.setStudentName(form.getStudentName());
                newStudent.setAdmission(form.getAdmission());
                Student savedStudent = studentRepository.save(newStudent);

                Subscriber subscriber;
                Optional<Subscriber> existingSubscriber = subscriberRepository.findByPhoneNumber(processPhoneNumber(form.getPhoneNumber()));
                if (existingSubscriber.isPresent()) {
                    subscriber = existingSubscriber.get();
                } else {
                    Subscriber newSubscriber = new Subscriber();
                    newSubscriber.setPhoneNumber(processPhoneNumber(form.getPhoneNumber()));
                    Long twoWeeks = (new Date().getTime()) + (30L * 24L * 3600L * 1000L);
                    Date date = new Date(twoWeeks);
                    newSubscriber.setValidUntil(date);
                    if (form.getParentName() != null) {
                        newSubscriber.setName(form.getParentName());
                    }
                    subscriber = subscriberRepository.save(newSubscriber);
                }

                SubscriberStudent subscriberStudent = new SubscriberStudent();
                subscriberStudent.setSubscriberid(subscriber.getSubscriberid());
                subscriberStudent.setStudentid(savedStudent.getStudentid());
                subscriberStudent.setSchoolid(savedStudent.getSchoolid());
                subscriberStudent.setPrimaryContact(form.getIsPrimary());
                subscriberStudentRepository.save(subscriberStudent);

                String recipients = processPhoneNumber(form.getPhoneNumber());
                SmsMessageForm smsMessageForm = new SmsMessageForm();
                smsMessageForm.setOtherRecipients(recipients);
                smsMessageForm.setText("Dear Parent, You can now access Results, Fees, Announcements and School Events with regards to your child " + form.getStudentName() + " by dialing *882*91# or by downloading the ShuleDial application from Android Playstore.");
                smsService.processOtherSMS(smsMessageForm);

                return subscriber;
            } else {
                return null;
            }
        }
    }

    private String processPhoneNumber(String phoneNumber) {
        String intlPhoneNumber;
        if (phoneNumber.startsWith("07")) {
            intlPhoneNumber = phoneNumber.replace("07", "2547");
            return intlPhoneNumber;
        } else if (phoneNumber.startsWith("+")) {
            intlPhoneNumber = phoneNumber.replace("+", "");
            return intlPhoneNumber;
        } else {
            return phoneNumber;
        }
    }

    @Override
    public int SubscriberCount(Long schoolid) {
        return subscriberStudentRepository.CountSubscriber(schoolid);
    }

    @Override
    public Collection<SubscriberStudent> findByStudentid(Long studentid) {

        return subscriberStudentRepository.findByStudentid(studentid);
    }

    @Override
    public Collection<SubscriberStudent> findBySubscriberid(Long subscriberid) {

        return subscriberStudentRepository.findBySubscriberid(subscriberid);
    }

    @Override
    public Collection<SubscriberStudent> findPrimaryByStudentid(Long studentid) {
        return subscriberStudentRepository.findPrimaryByStudentid(studentid);
    }

    @Override
    public Optional<StudentGroup> findStudentGroupById(Long studentgroupid) {
        return Optional.ofNullable(studentGroupRepository.findOne(studentgroupid));
    }

    @Override
    public Collection<Subscriber> findSubscribersbySchool(Long schoolid) {
        return null;
    }

    @Override
    public Collection<Subscriber> findPrimarySubscribersbySchool(Long schoolid) {
        return null;
    }

    @Override
    public Optional<Subscriber> findSubscriberById(Long subscriberid) {
        return null;
    }

    @Override
    public Collection<Subscriber> findSubscribersbyGroup(Long studentgroupid) {
        return null;
    }

    @Override
    public Collection<Subscriber> findPrimarySubscribersbyGroup(Long studentgroupid) {
        return null;
    }

    @Override
    public Optional<SubscriberStudent> findbyPhoneNumberandStudentId(String PhoneNumber, Long studentid) {
        Optional<Subscriber> subscriber = subscriberRepository.findByPhoneNumber(PhoneNumber);
        if (subscriber.isPresent()) {
            return subscriberStudentRepository.findByIds(subscriber.get().getSubscriberid(), studentid);
        } else {
            return null;
        }
    }

    @Override
    public Subscriber editSubscriber(ParentEditForm parentEditForm) {
        Subscriber subscriber = subscriberRepository.findOne(parentEditForm.getSubscriberid());

        if (subscriber != null) {
            subscriber.setName(parentEditForm.getParentName());
            subscriber.setPhoneNumber(parentEditForm.getPhoneNumber());
            return subscriberRepository.save(subscriber);
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public void deleteSubscriber(Long studentid, Long subscriberid) {
        subscriberStudentRepository.deleteByStudentidAndSubscriberid(studentid, subscriberid);
    }

    private String getFirstName(String name) {
        if (name.indexOf(" ") > -1) {
            return name.substring(0, name.indexOf(" "));
        } else {
            return name;
        }
    }
}