package org.sturgeon.sdial.common.service.school;

import org.sturgeon.sdial.common.model.SchoolPeriod;
import org.sturgeon.sdial.common.model.forms.SchoolPeriodForm;

import java.util.Collection;

/**
 * Created by User on 18/11/2016.
 */
public interface SchoolPeriodService {
    int count(Long schoolid);
    SchoolPeriod create(SchoolPeriodForm schoolPeriodForm);
    Collection<SchoolPeriod> getAllSchoolPeriods(Long schoolid);
}
