package org.sturgeon.sdial.common.service.school;

import org.sturgeon.sdial.common.model.SchoolType;

import java.util.Collection;

/**
 * Created by User on 18/11/2016.
 */
public interface SchoolTypeService {
    Collection<SchoolType> getAllSchoolTypes();
}
