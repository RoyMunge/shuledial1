package org.sturgeon.sdial.common.service.school;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.model.FeeBalance;
import org.sturgeon.sdial.common.model.FeeItem;
import org.sturgeon.sdial.common.model.FeeItemAmount;
import org.sturgeon.sdial.common.model.SchoolLevelPeriodTotal;
import org.sturgeon.sdial.common.model.forms.FeeBalanceForm;
import org.sturgeon.sdial.common.model.forms.FeeItemAmountForm;
import org.sturgeon.sdial.common.model.forms.FeeItemForm;
import org.sturgeon.sdial.common.model.forms.SchoolLevelPeriodTotalForm;
import org.sturgeon.sdial.common.repository.FeeBalanceRepository;
import org.sturgeon.sdial.common.repository.FeeItemAmountRepository;
import org.sturgeon.sdial.common.repository.FeeItemRepository;
import org.sturgeon.sdial.common.repository.SchoolLevelPeriodTotalRepository;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 19/11/2016.
 */
@Service
public class SchoolFeesServiceImpl implements SchoolFeesService {

    SchoolLevelPeriodTotalRepository schoolLevelPeriodTotalRepository;
    FeeItemRepository feeItemRepository;
    FeeItemAmountRepository feeItemAmountRepository;
    FeeBalanceRepository feeBalanceRepository;

    @Autowired
    public SchoolFeesServiceImpl(SchoolLevelPeriodTotalRepository schoolLevelPeriodTotalRepository, FeeItemRepository feeItemRepository, FeeItemAmountRepository feeItemAmountRepository, FeeBalanceRepository feeBalanceRepository) {
        this.schoolLevelPeriodTotalRepository = schoolLevelPeriodTotalRepository;
        this.feeItemRepository = feeItemRepository;
        this.feeItemAmountRepository = feeItemAmountRepository;
        this.feeBalanceRepository = feeBalanceRepository;
    }


    @Override
    public SchoolLevelPeriodTotal createSchoolLevelPeriodTotal(SchoolLevelPeriodTotalForm schoolLevelPeriodTotalForm) {
        SchoolLevelPeriodTotal schoolLevelPeriodTotal = new SchoolLevelPeriodTotal();
        schoolLevelPeriodTotal.setAmount(schoolLevelPeriodTotalForm.getAmount());
        schoolLevelPeriodTotal.setSchoolperiodid(schoolLevelPeriodTotalForm.getSchoolperiodid());
        schoolLevelPeriodTotal.setSchoollevelid(schoolLevelPeriodTotalForm.getSchoollevelid());
        return schoolLevelPeriodTotalRepository.save(schoolLevelPeriodTotal);
    }

    @Override
    public Optional<SchoolLevelPeriodTotal> getSchoolLevelPeriodTotal(Long schoolperiodid, Long schoollevelid) {
        return schoolLevelPeriodTotalRepository.getSchoolLevelPeriodTotal(schoolperiodid, schoollevelid);
    }

    @Override
    public void UpdateSchoolLevelPeriodTotal(Long schoollevelperiodtotalid, String amount) {
        schoolLevelPeriodTotalRepository.UpdateAmount(schoollevelperiodtotalid, amount);
    }

    @Override
    public FeeItem createFeeItem(FeeItemForm feeItemForm) {
        FeeItem feeItem = new FeeItem();
        feeItem.setSchoolid(feeItemForm.getSchoolid());
        feeItem.setFeeItemName(feeItemForm.getFeeItemName());
        return feeItemRepository.save(feeItem);
    }

    @Override
    public int countFeeItems(Long schoolid) {
        return feeItemRepository.CountFeeItem(schoolid);
    }

    @Override
    public Collection<FeeItem> getFeeItems(Long schoolid) {
        return feeItemRepository.findBySchoolid(schoolid);
    }

    @Override
    public FeeItemAmount createFeeItemAmount(FeeItemAmountForm feeItemAmountForm) {
        FeeItemAmount feeItemAmount = new FeeItemAmount();
        feeItemAmount.setAmount(feeItemAmountForm.getAmount());
        feeItemAmount.setFeeitemid(feeItemAmountForm.getFeeitemid());
        feeItemAmount.setSchoolperiodid(feeItemAmountForm.getSchoolperiodid());
        feeItemAmount.setSchoollevelid(feeItemAmountForm.getSchoollevelid());
        return feeItemAmountRepository.save(feeItemAmount);
    }

    @Override
    public Optional<FeeItemAmount> getFeeItemAmount(Long feeitemid,Long schoolperiodid, Long schoollevelid) {
        return feeItemAmountRepository.getFeeItemAmount(feeitemid, schoolperiodid,schoollevelid);
    }

    @Override
    public void UpdateFeeItemAmount(Long feeitemamountid, String amount) {
        feeItemAmountRepository.UpdateAmount(feeitemamountid, amount);
    }

    @Override
    public Collection<SchoolLevelPeriodTotal> getPeriodTotalBySchoolLevel(Long schoollevelid) {
        return schoolLevelPeriodTotalRepository.getBySchoolLevel(schoollevelid);
    }

    @Override
    public Collection<FeeItemAmount> getFeeAmountByItemandSchoolLevel(Long feeitemid, Long schoollevelid) {
        return feeItemAmountRepository.getByItemandSchoolLevel(feeitemid,schoollevelid);
    }

    @Override
    public Optional<FeeBalance> getFeeBalance(Long studentid) {
        return feeBalanceRepository.findByStudentid(studentid);
    }

    @Override
    public FeeBalance addFeeBalance(FeeBalanceForm feeBalanceForm) {
        Optional<FeeBalance> check = feeBalanceRepository.findByStudentid(feeBalanceForm.getStudentid());
        if (check.isPresent()){
            FeeBalance feeBalance = check.get();
            if (feeBalanceForm.getBalance() != null){
                feeBalance.setBalance(feeBalanceForm.getBalance());
            }
            if (feeBalanceForm.getPaid() != null){
                feeBalance.setPaid(feeBalanceForm.getPaid());
            }
            FeeBalance savedfeeBalance = feeBalanceRepository.save(feeBalance);
            return savedfeeBalance;
        } else {
            FeeBalance feeBalance = new FeeBalance();
            feeBalance.setStudentid(feeBalanceForm.getStudentid());
            feeBalance.setBalance(feeBalanceForm.getBalance());
            feeBalance.setPaid(feeBalanceForm.getPaid());
            return feeBalanceRepository.save(feeBalance);
        }
    }

    @Override
    public Collection<FeeItemAmount> getBySchoolPeriodandSchoolLevel(Long schoolperiodid, Long schoollevelid) {
        return feeItemAmountRepository.getBySchoolPeriodandSchoolLevel(schoolperiodid, schoollevelid);
    }
}
