package org.sturgeon.sdial.common.service.school;

import org.sturgeon.sdial.common.model.FeeBalance;
import org.sturgeon.sdial.common.model.FeeItem;
import org.sturgeon.sdial.common.model.FeeItemAmount;
import org.sturgeon.sdial.common.model.SchoolLevelPeriodTotal;
import org.sturgeon.sdial.common.model.forms.FeeBalanceForm;
import org.sturgeon.sdial.common.model.forms.FeeItemAmountForm;
import org.sturgeon.sdial.common.model.forms.FeeItemForm;
import org.sturgeon.sdial.common.model.forms.SchoolLevelPeriodTotalForm;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 19/11/2016.
 */
public interface SchoolFeesService {
    SchoolLevelPeriodTotal createSchoolLevelPeriodTotal(SchoolLevelPeriodTotalForm schoolLevelPeriodTotalForm);
    Optional<SchoolLevelPeriodTotal> getSchoolLevelPeriodTotal(Long schoolperiodid, Long schoollevelid);
    int countFeeItems(Long schoolid);
    FeeItem createFeeItem(FeeItemForm feeItemForm);
    Collection<FeeItem> getFeeItems(Long schoolid);
    FeeItemAmount createFeeItemAmount(FeeItemAmountForm feeItemAmountForm);
    Optional<FeeItemAmount> getFeeItemAmount(Long feeitemid, Long schoolperiodid, Long schoollevelid);
    void UpdateSchoolLevelPeriodTotal(Long schoollevelperiodtotalid, String amount);
    void UpdateFeeItemAmount(Long feeitemamountid, String amount);
    Collection<SchoolLevelPeriodTotal> getPeriodTotalBySchoolLevel(Long schoollevelid);
    Collection<FeeItemAmount> getFeeAmountByItemandSchoolLevel(Long feeitemid, Long schoollevelid);
    Optional<FeeBalance> getFeeBalance(Long studentid);
    FeeBalance addFeeBalance(FeeBalanceForm feeBalanceForm);
    Collection<FeeItemAmount> getBySchoolPeriodandSchoolLevel(Long schoolperiodid, Long schoollevelid);
}

