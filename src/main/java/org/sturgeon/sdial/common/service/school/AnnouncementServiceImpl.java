package org.sturgeon.sdial.common.service.school;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.model.*;
import org.sturgeon.sdial.common.model.forms.AnnouncementCreateForm;
import org.sturgeon.sdial.common.model.forms.PushNotificationForm;
import org.sturgeon.sdial.common.model.forms.SmsMessageForm;
import org.sturgeon.sdial.common.repository.AnnouncementRepository;
import org.sturgeon.sdial.common.repository.AnnouncementStudentGroupRepository;
import org.sturgeon.sdial.mobileapp.service.SubscriberService;
import org.sturgeon.sdial.sms.service.SmsService;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by User on 9/22/2015.
 */
@Service
public class AnnouncementServiceImpl implements AnnouncementService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolServiceImpl.class);
    private final AnnouncementRepository announcementRepository;
    private final AnnouncementStudentGroupRepository announcementStudentGroupRepository;
    private final SmsService smsService;
    private final StudentService studentService;
    private final SubscriberService subscriberService;

    @Autowired
    public AnnouncementServiceImpl(AnnouncementRepository announcementRepository, SmsService smsService, AnnouncementStudentGroupRepository announcementStudentGroupRepository, StudentService studentService, SubscriberService subscriberService) {
        this.announcementRepository = announcementRepository;
        this.announcementStudentGroupRepository = announcementStudentGroupRepository;
        this.smsService = smsService;
        this.studentService = studentService;
        this.subscriberService = subscriberService;
    }

    @Override
    public Optional<Announcement> getAnnouncementById(Long announcementid) {
        LOGGER.debug("getting announcement by id = {}", announcementid);
        return Optional.ofNullable(announcementRepository.findOne(announcementid));
    }

    @Override
    public Collection<Announcement> getAnnouncementsbySchoolId(Long schoolid) {
        LOGGER.debug("getting announcements by schoolid = {}", schoolid);
        return announcementRepository.findBySchoolid(schoolid);
    }

    @Override
    public int count(Long schoolid) {
        return announcementRepository.Count(schoolid);
    }

    @Override
    public Announcement create(AnnouncementCreateForm form) {

        if (form.getRecipients() == null || form.getRecipients().isEmpty()){
            Announcement announcement = new Announcement();
            announcement.setSchoolid(form.getSchoolid());
            announcement.setAnnouncementName(form.getAnnouncementName());
            announcement.setDescription(form.getDescription());
            announcement.setPostingDate(new Date());
            Announcement savedAnnouncement = announcementRepository.save(announcement);

            AnnouncementStudentGroup announcementStudentGroup = new AnnouncementStudentGroup();
            announcementStudentGroup.setAnnouncementid(savedAnnouncement.getAnnouncementid());
            announcementStudentGroup.setSchoolid(form.getSchoolid());
            announcementStudentGroup.setStudentgroupid(null);
            announcementStudentGroupRepository.save(announcementStudentGroup);


            if (form.isSendAsSMS()){
                SmsMessageForm smsMessageForm = new SmsMessageForm();
                smsMessageForm.setText(form.getDescription());
                smsMessageForm.setSchoolid(form.getSchoolid());
                smsMessageForm.setSendtoAll(true);
                smsMessageForm.setRecipients(form.getRecipients());
                smsService.processParentSMS(smsMessageForm);
            }

            PushNotificationForm pushNotificationForm = new PushNotificationForm(form.getAnnouncementName(), form.getDescription(),form.getSchoolid(), form.getRecipients(), true);
            subscriberService.processPushNotification(pushNotificationForm);


        } else {
            Announcement announcement = new Announcement();
            announcement.setSchoolid(form.getSchoolid());
            announcement.setAnnouncementName(form.getAnnouncementName());
            announcement.setDescription(form.getDescription());
            announcement.setPostingDate(new Date());
            Announcement savedAnnouncement = announcementRepository.save(announcement);

            for (String recipient : form.getRecipients()){
                AnnouncementStudentGroup announcementStudentGroup = new AnnouncementStudentGroup();
                announcementStudentGroup.setAnnouncementid(savedAnnouncement.getAnnouncementid());
                announcementStudentGroup.setSchoolid(form.getSchoolid());
                announcementStudentGroup.setStudentgroupid(Long.parseLong(recipient));
                announcementStudentGroupRepository.save(announcementStudentGroup);
            }

            if (form.isSendAsSMS()){
                SmsMessageForm smsMessageForm = new SmsMessageForm();
                smsMessageForm.setText(form.getDescription());
                smsMessageForm.setSchoolid(form.getSchoolid());
                smsMessageForm.setSendtoAll(false);
                smsMessageForm.setRecipients(form.getRecipients());
                smsService.processParentSMS(smsMessageForm);
            }

            PushNotificationForm pushNotificationForm = new PushNotificationForm(form.getAnnouncementName(), form.getDescription(),form.getSchoolid(), form.getRecipients(), false);
            subscriberService.processPushNotification(pushNotificationForm);

        }

         return null;
    }

    @Override
    public Collection<Announcement> getAnnouncementsByStudent(Long studentid) {
        Optional<Student> student = studentService.getStudentbyId(studentid);
        if(student.isPresent()){
            Set<StudentGroup> studentGroups = student.get().getStudentGroupSet();
            ArrayList<Long> query = new ArrayList();
            if(studentGroups != null || !studentGroups.isEmpty()){
                for(StudentGroup studentGroup : studentGroups){
                    query.add(studentGroup.getStudentgroupid());
                }
            }
                Collection<Long> announcementids = announcementStudentGroupRepository.findByStudent(student.get().getSchoolid(), query);

            if(!announcementids.isEmpty()){
                return announcementRepository.findByIds(announcementids);
            } else {
                return null;
            }


        } else {
            return null;
        }

    }
}