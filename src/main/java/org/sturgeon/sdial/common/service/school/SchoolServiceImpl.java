package org.sturgeon.sdial.common.service.school;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.sturgeon.sdial.common.model.*;
import org.sturgeon.sdial.common.model.forms.*;
import org.sturgeon.sdial.common.repository.*;

import java.util.*;

@Service
public class SchoolServiceImpl implements SchoolService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolServiceImpl.class);
    private final SchoolRepository schoolRepository;
    private final SchoolTypeRepository schoolTypeRepository;
    private final SchoolBioRepository schoolBioRepository;
    private final AdmissionRepository admissionRepository;
    private final CountyRepository countyRepository;
    private final SubCountyRepository subCountyRepository;
    private final QualificationLevelRepository qualificationLevelRepository;
    private final PhoneNumberRepository phoneNumberRepository;
    private final EmailRepository emailRepository;

    @Autowired
    public SchoolServiceImpl(SchoolRepository schoolRepository, SchoolTypeRepository schoolTypeRepository, SchoolBioRepository schoolBioRepository, AdmissionRepository admissionRepository, CountyRepository countyRepository, SubCountyRepository subCountyRepository, QualificationLevelRepository qualificationLevelRepository, PhoneNumberRepository phoneNumberRepository, EmailRepository emailRepository) {
        this.schoolRepository = schoolRepository;
        this.schoolTypeRepository = schoolTypeRepository;
        this.schoolBioRepository = schoolBioRepository;
        this.admissionRepository = admissionRepository;
        this.countyRepository = countyRepository;
        this.subCountyRepository = subCountyRepository;
        this.qualificationLevelRepository = qualificationLevelRepository;
        this.phoneNumberRepository = phoneNumberRepository;
        this.emailRepository = emailRepository;
    }


    @Override
    public Optional<School> getSchoolById(Long schoolid) {
        LOGGER.debug("getting school by id = {}", schoolid);
        return Optional.ofNullable(schoolRepository.findOne(schoolid));
    }

    @Override
    public Optional<School> getSchoolByShortName(String shortName) {
        LOGGER.debug("getting school by shorName = {}", shortName);
        return schoolRepository.findOneByShortName(shortName.toUpperCase());
    }

    @Override
    public Optional<School> getSchoolByName(String schoolName) {
        return schoolRepository.findOneByName(schoolName.toUpperCase());
    }

    @Override
    public Collection<School> getAllSchools() {
        LOGGER.debug("Getting all schools");
        return schoolRepository.findAll(new Sort("name"));
    }

    @Override
    public School create(SchoolCreateForm schoolForm) {
        School school = new School();
        school.setName(schoolForm.getName().toUpperCase());
        school.setShortName(schoolForm.getShortName().toUpperCase());
        school.setSchoolTypeSet(new HashSet<SchoolType>());
        school.setSubcountyid(schoolForm.getSubcountyid());
        School created = schoolRepository.save(school);

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setPhoneNumber(schoolForm.getSchoolPhone());
        phoneNumber.setSchoolid(created.getSchoolid());
        final PhoneNumber savedNumber = phoneNumberRepository.save(phoneNumber);
        Email email = new Email();
        email.setEmail(schoolForm.getSchoolEmail());
        email.setSchoolid(created.getSchoolid());
        final Email savedEmail = emailRepository.save(email);

        return created;
    }

    @Override
    public Collection<School> searchSchools(String searchstring) {
        LOGGER.debug("getting schools by searchstring = {}", searchstring);
        return schoolRepository.searchSchools("%" + searchstring.toUpperCase() + "%");
    }

    @Override
    public void updateSchoolBio(SchoolBioForm schoolBioForm) {
        schoolRepository.Update(schoolBioForm.getSchoolid(), schoolBioForm.getWebsite(), schoolBioForm.getMotto() ,schoolBioForm.getPhysicalAddress(), schoolBioForm.getPostalAddress(), schoolBioForm.getBio(), schoolBioForm.getAdmissionInfo(), schoolBioForm.getFeesPaymentInfo(), schoolBioForm.getOverview());
    }

    @Override
    public Admission createAdmission(AdmissionForm admissionForm) {
        Admission admission = new Admission();
        admission.setParentName(admissionForm.getParentName());
        admission.setStudentName(admissionForm.getStudentName());
        admission.setPhonenumber(admissionForm.getPhonenumber());
        admission.setEmail(admissionForm.getEmail());
        admission.setPreviousSchool(admissionForm.getPreviousSchool());
        admission.setSchoollevel(admissionForm.getSchoollevel());
        admission.setQualificationlevel(admissionForm.getQualificationlevel());
        admission.setQualificationLevelResult(admissionForm.getQualificationLevelResult());
        admission.setSchoolid(admissionForm.getSchoolid());
        admission.setDescription(admissionForm.getDescription());
        return admissionRepository.save(admission);
    }

    @Override
    public Collection<Admission> getAdmissionsBySchoolId(Long schoolid) {
        return admissionRepository.findBySchoolid(schoolid);
    }

    @Override
    public Set<School> getSchoolsByType(Long schooltypeid) {
        return schoolTypeRepository.findOne(schooltypeid).getSchoolSet();
    }

    @Override
    public Collection<County> getAllCounties() {
        return countyRepository.findAll();
    }

    @Override
    public Collection<SchoolType> findAllSchoolCategories() {
        return schoolTypeRepository.findAll();
    }

    @Override
    public County getCountyById(Long countyid) {
        return countyRepository.findOne(countyid);
    }

    @Override
    public Collection<QualificationLevel> getQualificationLevels() {
        return qualificationLevelRepository.findAll();
    }

    @Override
    public Collection<School> getSchoolsBySubCounty(Long subcountyid) {
        return subCountyRepository.findOne(subcountyid).getSchoolSet();
    }

    @Override
    public Collection<School> getSchoolsByCounty(Long countyid) {

        Collection<School> schools = new ArrayList<School>();

        for(SubCounty subCounty: countyRepository.findOne(countyid).getSubCountySet()){
            schools.addAll(subCounty.getSchoolSet());
        }

        return schools;
    }

    @Override
    public void addPhoneNumber(PhoneNumberAddForm form) {
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setSchoolid(form.getSchoolid());
        phoneNumber.setPhoneNumber(form.getPhonenumber());
        phoneNumberRepository.save(phoneNumber);
    }

    @Override
    public void addEmail(EmailAddForm form) {
        Email email = new Email();
        email.setSchoolid(form.getSchoolid());
        email.setEmail(form.getEmail());
        emailRepository.save(email);
    }

    @Override
    public void addSchoolType(SchoolTypeAddForm form) {
        SchoolType schoolType = schoolTypeRepository.findOne(form.getSchooltypeid());
        School school = schoolRepository.findOne(form.getSchoolid());
        school.getSchoolTypeSet().add(schoolType);

        schoolRepository.save(school);
    }

    @Override
    public Collection<SubCounty> getAllSubCounties() {
        return subCountyRepository.findAll();
    }
}
