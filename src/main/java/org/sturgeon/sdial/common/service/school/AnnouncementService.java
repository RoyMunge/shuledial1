package org.sturgeon.sdial.common.service.school;

import org.sturgeon.sdial.common.model.Announcement;
import org.sturgeon.sdial.common.model.AnnouncementStudentGroup;
import org.sturgeon.sdial.common.model.forms.AnnouncementCreateForm;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by User on 9/22/2015.
 */
public interface AnnouncementService {

    Optional<Announcement> getAnnouncementById(Long announcement_id);

    Collection<Announcement> getAnnouncementsbySchoolId(Long school_id);

    int count(Long schoolid);

    Announcement create(AnnouncementCreateForm form);

    Collection<Announcement> getAnnouncementsByStudent(Long studentid);

}
