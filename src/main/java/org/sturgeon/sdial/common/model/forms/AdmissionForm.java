package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 23/11/2016.
 */
public class AdmissionForm {
    private String parentName;
    private String studentName;
    private String phonenumber;
    private String email;
    private String previousSchool;
    private String schoollevel;
    private String qualificationlevel;
    private String qualificationLevelResult;
    private Long schoolid;
    private String description;

    public AdmissionForm(){}

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPreviousSchool() {
        return previousSchool;
    }

    public void setPreviousSchool(String previousSchool) {
        this.previousSchool = previousSchool;
    }

    public String getSchoollevel() {
        return schoollevel;
    }

    public void setSchoollevel(String schoollevel) {
        this.schoollevel = schoollevel;
    }

    public String getQualificationlevel() {
        return qualificationlevel;
    }

    public void setQualificationlevel(String qualificationlevel) {
        this.qualificationlevel = qualificationlevel;
    }

    public String getQualificationLevelResult() {
        return qualificationLevelResult;
    }

    public void setQualificationLevelResult(String qualificationLevelResult) {
        this.qualificationLevelResult = qualificationLevelResult;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
