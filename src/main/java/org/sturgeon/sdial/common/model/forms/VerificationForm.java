package org.sturgeon.sdial.common.model.forms;

/**
 * Created by Roy on 5/9/2017.
 */
public class VerificationForm {
        private String phoneNumber;
        private String verificationCode;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }
}
