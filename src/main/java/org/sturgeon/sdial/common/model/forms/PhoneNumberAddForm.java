package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 13/02/2017.
 */
public class PhoneNumberAddForm {
    Long schoolid;
    String phonenumber;

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
}
