package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(name = "Event.CountEvent", query = "select count(*) from Event e where e.schoolid = ?1"),
        @NamedQuery(name = "Event.UpcomingEvents", query = "select e from Event e where e.schoolid = ?1 and e.eventDate >= CURDATE()"),
        @NamedQuery(name = "Event.findByIds", query = "select e from Event e where e.eventid IN ?1 order by e.eventDate asc"),
        @NamedQuery(name = "Event.EventByName", query = "select e from Event e where e.schoolid = ?1 and e.eventName like ?2")
})
@Table(name = "event")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "eventid", nullable = false, updatable = false)
    private Long eventid;

    @Column(name = "eventname", nullable = false)
    private String eventName;

    @Column(name = "eventdate", nullable = false)
    private Date eventDate;

    @Column(name = "location", nullable = true)
    private String location;

    @Column(name = "endtime", nullable = true)
    private Date endDate;

    @Column(name = "description")
    private String description;

    @Column(name = "schoolid", nullable = false)
    private Long schoolid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolid", referencedColumnName="schoolid", updatable = false, insertable = false)
    private School school;

    public Long getEventid() {
        return eventid;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public void setEventid(Long eventid) {
        this.eventid = eventid;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }


    @Override
    public String toString() {
        return "Event{" +
                "eventid=" + eventid +
                ", eventName='" + eventName + '\'' +
                ", eventDate=" + eventDate +
                ", description='" + description + '\'' +
                ", school=" + school +
                '}';
    }
}
