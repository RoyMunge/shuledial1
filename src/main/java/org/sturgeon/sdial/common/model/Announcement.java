package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(name = "Announcement.Count", query = "select count(*) from Announcement a where a.schoolid = ?1"),
        @NamedQuery(name = "Announcement.findByIds", query = "select a from Announcement a where a.announcementid IN ?1 order by a.announcementid desc")
})

@Table(name = "announcement")
public class Announcement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "announcementid", nullable = false, updatable = false)
    private Long announcementid;

    @Column(name = "announcementname", nullable = false)
    private String announcementName;

    @Column(name = "postingdate", nullable = false)
    private Date postingDate;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "schoolid", nullable = false)
    private Long schoolid;


    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolid", referencedColumnName="schoolid", updatable = false, insertable = false)
    private School school;

    public Long getAnnouncementid() {
        return announcementid;
    }

    public String getAnnouncementName() {
        return announcementName;
    }

    public void setAnnouncementName(String announcementName) {
        this.announcementName = announcementName;
    }

    public Date getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(Date postingDate) {
        this.postingDate = postingDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    @Override
    public String toString() {
        return "Announcement{" +
                "announcementid=" + announcementid +
                ", announcementName='" + announcementName + '\'' +
                ", postingDate=" + postingDate +
                ", description='" + description + '\'' +
                ", school=" + school +
                '}';
    }
}
