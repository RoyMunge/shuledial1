package org.sturgeon.sdial.common.model;

import org.springframework.security.core.authority.AuthorityUtils;

public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private PortalUser user;

    public CurrentUser(PortalUser user) {
        super(user.getEmail(), user.getPasswordHash(), AuthorityUtils.NO_AUTHORITIES);
        this.user = user;
    }

    public PortalUser getUser() {
        return user;
    }

    public Long getId() {
        return user.getId();
    }

    @Override
    public String toString() {
        return "CurrentUser{" +
                "user=" + user +
                "} " + super.toString();
    }
}