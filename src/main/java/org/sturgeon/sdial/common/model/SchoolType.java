package org.sturgeon.sdial.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "schooltype")
public class SchoolType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schooltypeid", nullable = false, updatable = false)
    private Long schooltypeid;

    @Column(name = "schooltypename", nullable = false)
    private String schoolTypeName;

    @ManyToMany(mappedBy = "schoolTypeSet")
    private Set<School> schoolSet;

    public Long getSchooltypeid() {
        return schooltypeid;
    }

    public String getSchoolTypeName() {
        return schoolTypeName;
    }

    public void setSchoolTypeName(String schoolTypeName) {
        this.schoolTypeName = schoolTypeName;
    }

    public Set<School> getSchoolSet() {
        return schoolSet;
    }

    public void setSchoolSet(Set<School> schoolSet) {
        this.schoolSet = schoolSet;
    }

    @Override
    public String toString() {
        return "SchoolType{" +
                "schooltypeid=" + schooltypeid +
                ", schoolTypeName='" + schoolTypeName + '\'' +
                '}';
    }
}
