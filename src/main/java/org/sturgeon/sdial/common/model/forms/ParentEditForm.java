package org.sturgeon.sdial.common.model.forms;

public class ParentEditForm {
    private String phoneNumber;
    private String parentName;
    private Long subscriberid;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Long getSubscriberid() {
        return subscriberid;
    }

    public void setSubscriberid(Long subscriberid) {
        this.subscriberid = subscriberid;
    }
}
