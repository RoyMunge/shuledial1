package org.sturgeon.sdial.common.model.forms;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by User on 17/05/2016.
 */
public class SubscriberLoginForm {
    @NotEmpty
    private String subscriberPhoneNumber="";
    @NotEmpty
    private String subscriberPin="";

    public SubscriberLoginForm(){}

    public SubscriberLoginForm(String subscriberPhoneNumber, String subscriberPin) {
        this.subscriberPhoneNumber = subscriberPhoneNumber;
        this.subscriberPin = subscriberPin;
    }

    public String getSubscriberPhoneNumber() {
        return subscriberPhoneNumber;
    }

    public void setSubscriberPhoneNumber(String subscriberPhoneNumber) {
        this.subscriberPhoneNumber = subscriberPhoneNumber;
    }

    public String getSubscriberPin() {
        return subscriberPin;
    }

    public void setSubscriberPin(String subscriberPin) {
        this.subscriberPin = subscriberPin;
    }
}
