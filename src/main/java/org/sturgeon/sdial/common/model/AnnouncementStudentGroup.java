package org.sturgeon.sdial.common.model;

import javax.persistence.*;

/**
 * Created by Roy on 5/18/2017.
 */

@Entity
@NamedQueries({
        @NamedQuery(name = "AnnouncementStudentGroup.findByStudentgroupid", query = "select a from AnnouncementStudentGroup a where a.studentgroupid = ?1"),
        @NamedQuery(name = "AnnouncementStudentGroup.findBySchoolid", query = "select a from AnnouncementStudentGroup a where a.schoolid= ?1"),
        @NamedQuery(name = "AnnouncementStudentGroup.findByStudent", query = "select a.announcementid from AnnouncementStudentGroup a where a.schoolid= ?1 and a.studentgroupid IN ?2 or a.studentgroupid IS NULL group by a.announcementid order by a.announcementid desc"),
        @NamedQuery(name = "AnnouncementStudentGroup.findRecentByStudent", query = "select a.announcementid from AnnouncementStudentGroup a where a.schoolid= ?1 and a.studentgroupid IN ?2 or a.studentgroupid IS NULL group by a.announcementid order by a.announcementid desc")
})
@Table(name = "announcement_studentgroup")
public class AnnouncementStudentGroup  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "announcementstudentgroupid", nullable= false, updatable = false)
    private Long announcementstudentgroupid;

    @Column(name = "announcementid", nullable = false)
    private Long announcementid;

    @Column(name = "studentgroupid")
    private Long studentgroupid;

    @Column(name = "schoolid", nullable = false)
    private Long schoolid;

    @ManyToOne(optional = false, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "announcementid", referencedColumnName = "announcementid", updatable = false, insertable = false)
    private Announcement announcement;

    public Long getAnnouncementstudentgroupid() {
        return announcementstudentgroupid;
    }

    public Long getAnnouncementid() {
        return announcementid;
    }

    public void setAnnouncementid(Long announcementid) {
        this.announcementid = announcementid;
    }

    public Long getStudentgroupid() {
        return studentgroupid;
    }

    public void setStudentgroupid(Long studentgroupid) {
        this.studentgroupid = studentgroupid;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }
}
