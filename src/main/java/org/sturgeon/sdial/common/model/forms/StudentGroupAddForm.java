package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 14/03/2017.
 */
public class StudentGroupAddForm {
    private String admission;
    private String studentName;
    private Long schoolid;
    private Long studentgroupid;


    public Long getStudentgroupid() {
        return studentgroupid;
    }

    public void setStudentgroupid(Long studentgroupid) {
        this.studentgroupid = studentgroupid;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getAdmission() {
        return admission;
    }

    public void setAdmission(String admission) {
        this.admission = admission;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }
}
