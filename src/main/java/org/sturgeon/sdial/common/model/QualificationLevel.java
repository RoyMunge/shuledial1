package org.sturgeon.sdial.common.model;

import javax.persistence.*;

/**
 * Created by User on 18/01/2017.
 */
@Entity
@Table(name = "qualificationlevel")
public class QualificationLevel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "qualificationlevelid", nullable = false, updatable = false)
    private Long qualificationlevelid;

    @Column(name = "qualificationlevelname", nullable = false)
    private String qualificationLevelName;

    public String getQualificationLevelName() {
        return qualificationLevelName;
    }

    public void setQualificationLevelName(String qualificationLevelName) {
        this.qualificationLevelName = qualificationLevelName;
    }
}
