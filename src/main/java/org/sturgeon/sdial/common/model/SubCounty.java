package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by User on 18/01/2017.
 */
@Entity
@Table(name = "subcounty")
public class SubCounty {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subcountyid", nullable = false, updatable = false)
    private Long subcountyid;

    @Column(name = "subcountyname", nullable = false)
    private String subcountyname;

    @Column(name = "countyid", nullable = false)
    private Long countyid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="countyid", referencedColumnName="countyid", updatable = false, insertable = false)
    private County county;

    @OneToMany(mappedBy = "subCounty")
    private Set<School> schoolSet;

    public Long getSubcountyid() {
        return subcountyid;
    }

    public String getSubcountyname() {
        return subcountyname;
    }

    public void setSubcountyname(String subcountyname) {
        this.subcountyname = subcountyname;
    }

    public Long getCountyid() {
        return countyid;
    }

    public void setCountyid(Long countyid) {
        this.countyid = countyid;
    }

    public County getCounty() {
        return county;
    }

    public void setCounty(County county) {
        this.county = county;
    }

    public Set<School> getSchoolSet() {
        return schoolSet;
    }

    public void setSchoolSet(Set<School> schoolSet) {
        this.schoolSet = schoolSet;
    }
}
