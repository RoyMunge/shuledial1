package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(name = "Exam.findRecent", query = "select e from Exam e where e.schoolid = ?1 order by e.examDate desc"),
        @NamedQuery(name = "Exam.CountExam", query = "select count(*) from Exam s where s.schoolid = ?1")
})
@Table(name = "exam")
public class Exam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "examid", nullable = false, updatable = false)
    private Long examid;

    @Column(name = "examname", nullable = false)
    private String examName;

    @Column(name = "examDate", nullable = false)
    private Date examDate;

    @Column (name = "schoolid", nullable = false)
    private Long schoolid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolid", referencedColumnName="schoolid", updatable = false, insertable = false)
    private School school;


    public Long getExamid() {
        return examid;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
