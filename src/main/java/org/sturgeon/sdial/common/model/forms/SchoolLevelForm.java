package org.sturgeon.sdial.common.model.forms;

import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Created by User on 18/11/2016.
 */
public class SchoolLevelForm {
    @NotEmpty
    private String schoolLevelName;
    @NotNull
    private Long schoolid;

    public String getSchoolLevelName() {
        return schoolLevelName;
    }

    public void setSchoolLevelName(String schoolLevelName) {
        this.schoolLevelName = schoolLevelName;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }
}
