package org.sturgeon.sdial.common.model;

import javax.persistence.*;

/**
 * Created by User on 18/11/2016.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "FeeItemAmount.UpdateAmount", query = "UPDATE FeeItemAmount f SET f.amount = ?2 WHERE f.feeitemamountid = ?1"),
        @NamedQuery(name= "FeeItemAmount.getFeeItemAmount", query = "SELECT f From FeeItemAmount f WHERE f.feeitemid = ?1 and f.schoolperiodid = ?2 and f.schoollevelid = ?3"),
        @NamedQuery(name= "FeeItemAmount.getBySchoolPeriodandSchoolLevel", query = "SELECT f From FeeItemAmount f WHERE f.schoolperiodid = ?1 and f.schoollevelid = ?2"),
        @NamedQuery(name= "FeeItemAmount.getByItemandSchoolLevel", query = "SELECT f From FeeItemAmount f WHERE f.feeitemid = ?1 and f.schoollevelid = ?2")
})
@Table(name = "feeitemamount")
public class FeeItemAmount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "feeitemamountid", nullable = false, updatable = false)
    private Long feeitemamountid;

    @Column(name = "feeitemid", nullable = false)
    private Long feeitemid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="feeitemid", referencedColumnName="feeitemid", updatable = false, insertable = false)
    private FeeItem feeItem;


    @Column(name = "amount", nullable = true)
    private String amount;

    @Column(name = "schoolperiodid", nullable = false)
    private Long schoolperiodid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolperiodid", referencedColumnName="schoolperiodid", updatable = false, insertable = false)
    private SchoolPeriod schoolPeriod;

    @Column(name = "schoollevelid", nullable = false)
    private Long schoollevelid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoollevelid", referencedColumnName="schoollevelid", updatable = false, insertable = false)
    private SchoolLevel schoolLevel;

    public Long getFeeitemamountid() {
        return feeitemamountid;
    }

    public String getAmount() {
        return amount;
    }

    public Long getFeeitemid() {
        return feeitemid;
    }

    public void setFeeitemid(Long feeitemid) {
        this.feeitemid = feeitemid;
    }

    public FeeItem getFeeItem() {
        return feeItem;
    }

    public void setFeeItem(FeeItem feeItem) {
        this.feeItem = feeItem;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Long getSchoolperiodid() {
        return schoolperiodid;
    }

    public void setSchoolperiodid(Long schoolperiodid) {
        this.schoolperiodid = schoolperiodid;
    }

    public SchoolPeriod getSchoolPeriod() {
        return schoolPeriod;
    }

    public void setSchoolPeriod(SchoolPeriod schoolPeriod) {
        this.schoolPeriod = schoolPeriod;
    }

    public Long getSchoollevelid() {
        return schoollevelid;
    }

    public void setSchoollevelid(Long schoollevelid) {
        this.schoollevelid = schoollevelid;
    }

    public SchoolLevel getSchoolLevel() {
        return schoolLevel;
    }

    public void setSchoolLevel(SchoolLevel schoolLevel) {
        this.schoolLevel = schoolLevel;
    }

    @Override
    public String toString() {
        return "FeeItemAmount{" +
                "feeitemamountid=" + feeitemamountid +
                ", feeitemid=" + feeitemid +
                ", feeItem=" + feeItem +
                ", amount=" + amount +
                ", schoolperiodid=" + schoolperiodid +
                ", schoolPeriod=" + schoolPeriod +
                ", schoollevelid=" + schoollevelid +
                ", schoolLevel=" + schoolLevel +
                '}';
    }
}
