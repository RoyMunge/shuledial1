package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@NamedQueries({
        @NamedQuery(name = "Student.CountStudent", query = "select count(*) from Student s where s.schoolid = ?1"),
        @NamedQuery(name = "Student.findBySchoolidAndAdmission", query = "select s from Student s where s.schoolid = ?1 and s.admission = ?2")
})
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "studentid", nullable = false, updatable = false)
    private Long studentid;

    @Column(name = "admission", nullable = false)
    private String admission;

    @Column(name = "studentname", nullable = false)
    private String studentName;

    @Column(name = "schoolid", nullable = false)
    private Long schoolid;

    @ManyToOne(optional = false, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "schoolid", referencedColumnName = "schoolid", updatable = false, insertable = false)
    private School school;

    @ManyToMany(mappedBy = "studentSet")
    private Set<StudentGroup> studentGroupSet;

    public Long getStudentid() {
        return studentid;
    }

    public String getAdmission() {
        return admission;
    }

    public void setAdmission(String admission) {
        this.admission = admission;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public School getSchool() {
        return school;
    }

    public Set<StudentGroup> getStudentGroupSet() {
        return studentGroupSet;
    }

    public void setStudentGroupSet(Set<StudentGroup> studentGroupSet) {
        this.studentGroupSet = studentGroupSet;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentid=" + studentid +
                ", admission='" + admission + '\'' +
                ", studentName='" + studentName + '\'' +
                ", school=" + school +
                '}';
    }
}