package org.sturgeon.sdial.common.model.forms;

import org.hibernate.validator.constraints.NotEmpty;
import org.sturgeon.sdial.common.model.SchoolPeriod;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;

/**
 * Created by User on 9/22/2015.
 */
public class EventCreateForm {

    @NotEmpty
    private String eventName;

    @NotEmpty
    private String eventDate;

    private String location;

    private String endDate;

    private String description;

    private ArrayList<String> recipients;

    @NotNull
    private Long schoolid;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public ArrayList<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(ArrayList<String> recipients) {
        this.recipients = recipients;
    }

    @Override
    public String toString() {
        return "EventCreateForm{" +
                "eventName='" + eventName + '\'' +
                ", eventDate=" + eventDate +
                ", description='" + description + '\'' +
                ", schoolid=" + schoolid +
                '}';
    }
}
