package org.sturgeon.sdial.common.model.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.sturgeon.sdial.common.model.forms.StudentCreateForm;
import org.sturgeon.sdial.common.service.school.StudentService;

/**
 * Created by Student on 9/20/2015.
 */
@Component
public class StudentCreateFormValidator implements Validator {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentCreateFormValidator.class);
    private final StudentService studentService;

    @Autowired
    public StudentCreateFormValidator(StudentService studentService) {
        this.studentService = studentService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(StudentCreateForm.class);
    }

    @Override
    public void validate(Object target, Errors errors){
        LOGGER.debug("Validating {}", target);
        StudentCreateForm form = (StudentCreateForm) target;
        validateAdmission(errors, form);
    }

    private void validateAdmission(Errors errors, StudentCreateForm form) {
        LOGGER.debug("Validating {}", form);
        if (studentService.getStudentbyAdmission(form.getSchoolid(),form.getAdmission()).isPresent()) {
            errors.reject("student.exists", "Student with this admission number already exists");
        }
    }

}
