package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Set;

//@NamedQuery(name="School.filterSchools", query = "select distinct s from School s, County c, SchoolType t join c.schoolSet cs on cs.schoolid = s.schoolid join t.schoolSet ts on ts.schoolid = s.schoolid where c.countyid = ?1 t.schooltypeid = ?2"),

@Entity
@NamedQueries ({
        @NamedQuery(name = "School.searchSchools", query = "select s from School s where s.name LIKE ?1"),
        @NamedQuery(name = "School.Update", query = "UPDATE School s SET s.website=?2, s.motto=?3, s.physicalAddress=?4, s.postalAddress=?5, s.bio=?6, s.admissionInfo=?7, s.feesPaymentInfo=?8, s.overview=?9 WHERE s.schoolid=?1"),
        })
@Table(name = "school")
public class School {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schoolid", nullable = false, updatable = false)
    private Long schoolid;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "shortname", nullable = false, unique = true)
    private String shortName;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolid", referencedColumnName="schoolid")
    private Set<PhoneNumber> phoneNumberSet;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolid", referencedColumnName="schoolid")
    private Set<Email> emailSet;

    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<SchoolType> schoolTypeSet;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolid", referencedColumnName="schoolid")
    private Set<StudentGroup> studentGroupSet;

    @Column(name = "website")
    private String website;

    @Column(name = "motto")
    private String motto;

    @Column(name = "physicaladdress")
    private String physicalAddress;

    @Column(name = "postaladdress")
    private String postalAddress;

    @Column(name = "bio")
    private String bio;

    @Column(name = "admissioninfo")
    private String admissionInfo;

    @Column(name = "feespaymentinfo")
    private String feesPaymentInfo;

    @Column(name = "subcountyid")
    private Long subcountyid;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subcountyid", referencedColumnName = "subcountyid", updatable = false, insertable = false)
    private SubCounty subCounty;

    @Column(name = "overview")
    private String overview;


    public Long getSchoolid() {
        return schoolid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Set<PhoneNumber> getPhoneNumberSet(){
        return phoneNumberSet;
    }

    public void setPhoneNumberSet(Set<PhoneNumber> phoneNumberSet) {
        this.phoneNumberSet = phoneNumberSet;
    }

   public Set<Email> getEmailSet() {
        return emailSet;
    }

    public void setEmailSet(Set<Email> emailSet) {
        this.emailSet = emailSet;
    }

    public Set<SchoolType> getSchoolTypeSet() {
        return schoolTypeSet;
    }

    public void setSchoolTypeSet(Set<SchoolType> schoolTypeSet) {
        this.schoolTypeSet = schoolTypeSet;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getAdmissionInfo() {
        return admissionInfo;
    }

    public void setAdmissionInfo(String admissionInfo) {
        this.admissionInfo = admissionInfo;
    }

    public String getFeesPaymentInfo() {
        return feesPaymentInfo;
    }

    public void setFeesPaymentInfo(String feesPaymentInfo) {
        this.feesPaymentInfo = feesPaymentInfo;
    }

    public Long getSubcountyid() {
        return subcountyid;
    }

    public void setSubcountyid(Long subcountyid) {
        this.subcountyid = subcountyid;
    }

    public SubCounty getSubCounty() {
        return subCounty;
    }

    public void setSubCounty(SubCounty subCounty) {
        this.subCounty = subCounty;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public Set<StudentGroup> getStudentGroupSet() {
        return studentGroupSet;
    }

    public void setStudentGroupSet(Set<StudentGroup> studentGroupSet) {
        this.studentGroupSet = studentGroupSet;
    }
}