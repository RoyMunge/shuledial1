package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 13/02/2017.
 */
public class SchoolTypeAddForm {
    Long schooltypeid;
    Long schoolid;

    public Long getSchooltypeid() {
        return schooltypeid;
    }

    public void setSchooltypeid(Long schooltypeid) {
        this.schooltypeid = schooltypeid;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }
}
