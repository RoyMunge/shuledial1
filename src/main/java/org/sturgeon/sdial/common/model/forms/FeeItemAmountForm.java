package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 19/11/2016.
 */
public class FeeItemAmountForm {
    private Long feeitemamountid;
    private Long feeitemid;
    private String amount;
    private Long schoolperiodid;
    private Long schoollevelid;

    public Long getFeeitemamountid() {
        return feeitemamountid;
    }

    public void setFeeitemamountid(Long feeitemamountid) {
        this.feeitemamountid = feeitemamountid;
    }

    public Long getFeeitemid() {
        return feeitemid;
    }

    public void setFeeitemid(Long feeitemid) {
        this.feeitemid = feeitemid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Long getSchoollevelid() {
        return schoollevelid;
    }

    public void setSchoollevelid(Long schoollevelid) {
        this.schoollevelid = schoollevelid;
    }

    public Long getSchoolperiodid() {
        return schoolperiodid;
    }

    public void setSchoolperiodid(Long schoolperiodid) {
        this.schoolperiodid = schoolperiodid;
    }


    @Override
    public String toString() {
        return "FeeItemAmountForm{" +
                "feeitemamountid=" + feeitemamountid +
                ", feeitemid=" + feeitemid +
                ", amount=" + amount +
                ", schoolperiodid=" + schoolperiodid +
                ", schoollevelid=" + schoollevelid +
                '}';
    }
}
