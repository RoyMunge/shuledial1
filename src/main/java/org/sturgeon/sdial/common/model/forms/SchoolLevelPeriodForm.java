package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 19/11/2016.
 */
public class SchoolLevelPeriodForm {
    private Long schoolperiodid;
    private Long schoollevelid;

    public Long getSchoolperiodid() {
        return schoolperiodid;
    }

    public void setSchoolperiodid(Long schoolperiodid) {
        this.schoolperiodid = schoolperiodid;
    }

    public Long getSchoollevelid() {
        return schoollevelid;
    }

    public void setSchoollevelid(Long schoollevelid) {
        this.schoollevelid = schoollevelid;
    }
}
