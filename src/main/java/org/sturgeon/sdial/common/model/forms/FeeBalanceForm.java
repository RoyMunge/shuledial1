package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 23/11/2016.
 */
public class FeeBalanceForm {
    private Long studentid;
    private String balance;
    private String paid;

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
