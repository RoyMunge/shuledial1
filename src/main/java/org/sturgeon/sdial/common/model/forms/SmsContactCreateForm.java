package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 01/04/2017.
 */
public class SmsContactCreateForm {
    private Long smscontactgroupid;
    private String phoneNumber;
    private String name;
    private Long schoolid;

    public Long getSmscontactgroupid() {
        return smscontactgroupid;
    }

    public void setSmscontactgroupid(Long smscontactgroupid) {
        this.smscontactgroupid = smscontactgroupid;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }
}
