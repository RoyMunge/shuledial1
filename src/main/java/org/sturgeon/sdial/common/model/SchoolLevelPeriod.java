package org.sturgeon.sdial.common.model;

import javax.persistence.*;

/**
 * Created by User on 19/11/2016.
 */
@Entity
@Table(name = "schoollevelperiod")
public class SchoolLevelPeriod {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schoollevelperiodid", nullable = false, updatable = false)
    private Long schoollevelperiodid;

    @Column(name = "schoolperiodid", nullable = false)
    private Long schoolperiodid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolperiodid", referencedColumnName="schoolperiodid", updatable = false, insertable = false)
    private SchoolPeriod schoolPeriod;

    @Column(name = "schoollevelid", nullable = false)
    private Long schoollevelid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoollevelid", referencedColumnName="schoollevelid", updatable = false, insertable = false)
    private SchoolLevel schoolLevel;

    public Long getSchoollevelperiodid() {
        return schoollevelperiodid;
    }

    public Long getSchoolperiodid() {
        return schoolperiodid;
    }

    public void setSchoolperiodid(Long schoolperiodid) {
        this.schoolperiodid = schoolperiodid;
    }

    public SchoolPeriod getSchoolPeriod() {
        return schoolPeriod;
    }

    public void setSchoolPeriod(SchoolPeriod schoolPeriod) {
        this.schoolPeriod = schoolPeriod;
    }

    public Long getSchoollevelid() {
        return schoollevelid;
    }

    public void setSchoollevelid(Long schoollevelid) {
        this.schoollevelid = schoollevelid;
    }

    public SchoolLevel getSchoolLevel() {
        return schoolLevel;
    }

    public void setSchoolLevel(SchoolLevel schoolLevel) {
        this.schoolLevel = schoolLevel;
    }
}
