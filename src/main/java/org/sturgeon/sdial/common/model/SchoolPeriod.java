package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by User on 9/22/2015.
 */
@Entity
@NamedQuery(name = "SchoolPeriod.CountSchoolPeriod", query = "select count(*) from SchoolPeriod e where e.schoolid = ?1")
@Table(name="schoolperiod")
public class SchoolPeriod {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schoolperiodid", nullable = false, updatable = false)
    private Long schoolperiodid;

    @Column(name = "schoolperiodname", nullable = false)
    private String schoolPeriodName;

    @Column(name = "startdate", nullable = false)
    private Date startDate;

    @Column(name = "enddate", nullable = false)
    private Date endDate;

    @Column(name = "schoolid", nullable = false)
    private Long schoolid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolid", referencedColumnName="schoolid", updatable = false, insertable = false)
    private School school;

    public Long getSchoolperiodid() {
        return schoolperiodid;
    }

    public String getSchoolPeriodName() {
        return schoolPeriodName;
    }

    public void setSchoolPeriodName(String schoolPeriodName) {
        this.schoolPeriodName = schoolPeriodName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
