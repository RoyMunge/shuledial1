package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by User on 18/01/2017.
 */
@Entity
@Table(name = "county")
public class County {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "countyid", nullable = false, updatable = false)
    private Long countyid;

    @Column(name = "countyname", nullable = false)
    private String countyName;

    @OneToMany(mappedBy = "county")
    private Set<SubCounty> subCountySet;

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public Set<SubCounty> getSubCountySet() {
        return subCountySet;
    }

    public void setSubCountySet(Set<SubCounty> subCountySet) {
        this.subCountySet = subCountySet;
    }

    public Long getCountyid() {
        return countyid;
    }
}
