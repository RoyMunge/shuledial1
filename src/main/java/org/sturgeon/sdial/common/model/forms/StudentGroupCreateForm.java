package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 14/03/2017.
 */
public class StudentGroupCreateForm {
    private Long studentgroupid = null;
    private String name;
    private String description;
    private Long schoolid;

    public Long getStudentgroupid() {
        return studentgroupid;
    }

    public void setStudentgroupid(Long studentgroupid) {
        this.studentgroupid = studentgroupid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }
}
