package org.sturgeon.sdial.common.model;

import javax.persistence.*;

/**
 * Created by User on 23/11/2016.
 */
@Entity
@Table(name = "feebalance")
public class FeeBalance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "feebalanceid", nullable = false, updatable = false)
    private Long feebalanceid;

    @Column(name = "studentid", nullable = false)
    private Long studentid;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "studentid", referencedColumnName = "studentid", updatable = false, insertable = false)
    private Student student;

    @Column(name = "balance", nullable = false)
    private String balance;

    @Column(name = "paid", nullable = true)
    private String paid;

    public Long getFeebalanceid() {
        return feebalanceid;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }
}