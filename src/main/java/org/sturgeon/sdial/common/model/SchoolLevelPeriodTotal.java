package org.sturgeon.sdial.common.model;

import javax.persistence.*;

/**
 * Created by User on 19/11/2016.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "SchoolLevelPeriodTotal.UpdateAmount", query = "UPDATE SchoolLevelPeriodTotal f SET f.amount = ?2 WHERE f.schoollevelperiodtotalid = ?1"),
        @NamedQuery(name= "SchoolLevelPeriodTotal.getSchoolLevelPeriodTotal", query = "SELECT f From SchoolLevelPeriodTotal f WHERE f.schoolperiodid = ?1 and f.schoollevelid = ?2"),
        @NamedQuery(name= "SchoolLevelPeriodTotal.getBySchoolLevel", query = "SELECT f From SchoolLevelPeriodTotal f WHERE f.schoollevelid = ?1")
})
@Table(name = "schoollevelperiodtotal")
public class SchoolLevelPeriodTotal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schoollevelperiodtotalid", nullable = false, updatable = false)
    private Long schoollevelperiodtotalid;

    @Column(name = "amount", nullable = true)
    private String amount;

    @Column(name = "schoolperiodid", nullable = false)
    private Long schoolperiodid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolperiodid", referencedColumnName="schoolperiodid", updatable = false, insertable = false)
    private SchoolPeriod schoolPeriod;

    @Column(name = "schoollevelid", nullable = false)
    private Long schoollevelid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoollevelid", referencedColumnName="schoollevelid", updatable = false, insertable = false)
    private SchoolLevel schoolLevel;

    public Long getSchoollevelperiodtotalid() {
        return schoollevelperiodtotalid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Long getSchoolperiodid() {
        return schoolperiodid;
    }

    public void setSchoolperiodid(Long schoolperiodid) {
        this.schoolperiodid = schoolperiodid;
    }

    public SchoolPeriod getSchoolPeriod() {
        return schoolPeriod;
    }

    public void setSchoolPeriod(SchoolPeriod schoolPeriod) {
        this.schoolPeriod = schoolPeriod;
    }

    public Long getSchoollevelid() {
        return schoollevelid;
    }

    public void setSchoollevelid(Long schoollevelid) {
        this.schoollevelid = schoollevelid;
    }

    public SchoolLevel getSchoolLevel() {
        return schoolLevel;
    }

    public void setSchoolLevel(SchoolLevel schoolLevel) {
        this.schoolLevel = schoolLevel;
    }

    @Override
    public String toString() {
        return "SchoolLevelPeriodTotal{" +
                "schoollevelperiodtotalid=" + schoollevelperiodtotalid +
                ", amount=" + amount +
                ", schoolperiodid=" + schoolperiodid +
                ", schoolPeriod=" + schoolPeriod +
                ", schoollevelid=" + schoollevelid +
                ", schoolLevel=" + schoolLevel +
                '}';
    }
}
