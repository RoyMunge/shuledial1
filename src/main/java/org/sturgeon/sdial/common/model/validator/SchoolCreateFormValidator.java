package org.sturgeon.sdial.common.model.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.sturgeon.sdial.common.model.forms.SchoolCreateForm;
import org.sturgeon.sdial.common.service.school.SchoolService;

/**
 * Created by School on 9/20/2015.
 */
@Component
public class SchoolCreateFormValidator implements Validator {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolCreateFormValidator.class);
    private final SchoolService schoolService;

    @Autowired
    public SchoolCreateFormValidator(SchoolService schoolService) {
        this.schoolService = schoolService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(SchoolCreateForm.class);
    }

    @Override
    public void validate(Object target, Errors errors){
        LOGGER.debug("Validating {}", target);
        SchoolCreateForm form = (SchoolCreateForm) target;
        validateShortName(errors, form);
    }

    private void validateShortName(Errors errors, SchoolCreateForm form) {
        if (schoolService.getSchoolByShortName(form.getShortName()).isPresent()) {
            errors.reject("shorname.exists", "School with this shortname already exists");
        }
    }

}
