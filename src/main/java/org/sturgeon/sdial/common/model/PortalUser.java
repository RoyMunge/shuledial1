package org.sturgeon.sdial.common.model;

import javax.persistence.*;

@Entity
@Table(name = "portal_user")
public class PortalUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userid", nullable = false, updatable = false)
    private Long userid;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password_hash", nullable = false)
    private String passwordHash;

    @Column(name = "firstname", nullable = false)
    private String firstName;

    @Column(name = "othernames", nullable = true)
    private String otherNames;

    @Column (name = "schoolid", nullable = false)
    private Long schoolid;

    @Column (name = "phone", nullable = false)
    private String phone;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolid", referencedColumnName="schoolid", updatable = false, insertable = false)
    private School school;

    public Long getId() {
        return userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getFirstName(){ return firstName;}

    public void setFirstName(String firstName){ this.firstName = firstName;}

    public String getOtherNames(){ return otherNames;}

    public void setOtherNames(String otherNames){ this.otherNames = otherNames;}

    public Long getSchoolid() { return schoolid;}

    public void setSchoolid(Long schoolid) { this.schoolid = schoolid;}

    public String getPhone() { return phone;}

    public void setPhone(String phone) { this.phone = phone;}

    public School getSchool() { return school;}

    @Override
    public String toString() {
        return "User{" +
                "id=" + userid +
                ", email='" + email.replaceFirst("@.*", "@***") +
                ", passwordHash='" + passwordHash.substring(0, 10) +
                ", firstName='" + firstName +
                ", otherNames='" + otherNames +
                ", schoolId=" + schoolid +
                ", phone='" + phone +
                '}';
    }
}