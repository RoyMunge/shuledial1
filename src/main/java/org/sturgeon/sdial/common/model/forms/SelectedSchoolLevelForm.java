package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 21/11/2016.
 */
public class SelectedSchoolLevelForm {
    private Long selectedSchoolLevelid;

    public Long getSelectedSchoolLevelid() {
        return selectedSchoolLevelid;
    }

    public void setSelectedSchoolLevelid(Long selectedSchoolLevelid) {
        this.selectedSchoolLevelid = selectedSchoolLevelid;
    }
}
