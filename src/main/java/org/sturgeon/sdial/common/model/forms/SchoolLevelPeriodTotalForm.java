package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 19/11/2016.
 */
public class SchoolLevelPeriodTotalForm {
    private String amount;
    private Long schoolperiodid;
    private Long schoollevelid;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Long getSchoolperiodid() {
        return schoolperiodid;
    }

    public void setSchoolperiodid(Long schoolperiodid) {
        this.schoolperiodid = schoolperiodid;
    }

    public Long getSchoollevelid() {
        return schoollevelid;
    }

    public void setSchoollevelid(Long schoollevelid) {
        this.schoollevelid = schoollevelid;
    }

    @Override
    public String toString() {
        return "SchoolLevelPeriodTotalForm{" +
                "amount=" + amount +
                ", schoolperiodid=" + schoolperiodid +
                ", schoollevelid=" + schoollevelid +
                '}';
    }
}
