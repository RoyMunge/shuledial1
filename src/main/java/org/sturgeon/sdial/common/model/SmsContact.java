package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by User on 01/04/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "SmsContact.CountSmsContact", query = "select count(*) from SmsContact s where s.schoolid = ?1"),
        @NamedQuery(name = "SmsContact.findByPhoneNumber", query = "select s from SmsContact s where s.phoneNumber = ?1 and s.schoolid = ?2")
})
@Table(name = "smscontact")
public class SmsContact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "smscontactid", nullable = false, updatable = false)
    private Long smscontactid;

    @Column(name = "phonenumber", nullable = false)
    private String phoneNumber;

    @Column(name = "name")
    private String name;

    @Column(name = "schoolid")
    private Long schoolid;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name="smscontact_smscontactgroup",
            joinColumns = @JoinColumn(name = "smscontactid",
                    referencedColumnName = "smscontactid"),
            inverseJoinColumns = @JoinColumn(name = "smscontactgroupid",
                    referencedColumnName = "smscontactgroupid"))
    private Set<SmsContactGroup> smsContactGroupSet;

    public Long getSmscontactid() {
        return smscontactid;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public Set<SmsContactGroup> getSmsContactGroupSet() {
        return smsContactGroupSet;
    }

    public void setSmsContactGroupSet(Set<SmsContactGroup> smsContactGroupSet) {
        this.smsContactGroupSet = smsContactGroupSet;
    }
}
