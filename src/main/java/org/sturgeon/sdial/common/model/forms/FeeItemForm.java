package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 19/11/2016.
 */
public class FeeItemForm {
    private String feeItemName;
    private Long schoolid;

    public String getFeeItemName() {
        return feeItemName;
    }

    public void setFeeItemName(String feeItemName) {
        this.feeItemName = feeItemName;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }
}
