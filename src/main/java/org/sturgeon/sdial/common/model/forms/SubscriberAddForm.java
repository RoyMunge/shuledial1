package org.sturgeon.sdial.common.model.forms;

/**
 * Created by Roy on 5/29/2017.
 */
public class SubscriberAddForm {
    private String msisdn;
    private String phoneNumber;
    private String name;
    private String studentid;

    public SubscriberAddForm() {
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }
}
