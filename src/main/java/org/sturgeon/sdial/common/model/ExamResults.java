package org.sturgeon.sdial.common.model;

import javax.persistence.*;

@Entity
@NamedQuery(name = "ExamResults.findByStudentid", query = "select e from ExamResults e where e.studentid = ?1 order by e.examresultid desc")
@Table(name = "examresults")
public class ExamResults {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "examresultid", nullable = false, updatable = false)
    private Long examresultid;

    @Column(name = "studentid", nullable = false)
    private Long studentid;

    @Column(name = "examid", nullable = false)
    private Long examid;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "examid", referencedColumnName = "examid", updatable = false, insertable = false)
    private Exam exam;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "studentid", referencedColumnName = "studentid", updatable = false, insertable = false)
    private Student student;

    @Column(name = "english")
    private String english;

    @Column(name = "kiswahili")
    private String kiswahili;

    @Column(name = "maths")
    private String maths;

    @Column(name = "science")
    private String science;

    @Column(name = "biology")
    private String biology;

    @Column(name = "physics")
    private String physics;

    @Column(name = "chemistry")
    private String chemistry;

    @Column(name = "history")
    private String history;

    @Column(name = "geography")
    private String geography;

    @Column(name = "cre")
    private String cre;

    @Column(name = "ire")
    private String ire;

    @Column(name = "hre")
    private String hre;

    @Column(name = "homescience")
    private String homescience;

    @Column(name = "art")
    private String art;

    @Column(name = "agriculture")
    private String agriculture;

    @Column(name = "conputer")
    private String commputer;

    @Column(name = "aviation")
    private String aviation;

    @Column(name = "french")
    private String french;

    @Column(name = "german")
    private String german;

    @Column(name = "arabic")
    private String arabic;

    @Column(name = "music")
    private String music;

    @Column(name = "businessstudies")
    private String businessstudies;

    @Column(name = "socialstudies")
    private String socialstudies;

    @Column(name = "ssre")
    private String ssre;

    @Column(name = "total")
    private String total;

    @Column(name = "average")
    private String average;

    @Column(name = "grade")
    private String grade;

    @Column(name = "position")
    private String position;

    public Long getExamresultid() {
        return examresultid;
    }

    public void setExamresultid(Long examresultid) {
        this.examresultid = examresultid;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public Long getExamid() {
        return examid;
    }

    public void setExamid(Long examid) {
        this.examid = examid;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getKiswahili() {
        return kiswahili;
    }

    public void setKiswahili(String kiswahili) {
        this.kiswahili = kiswahili;
    }

    public String getMaths() {
        return maths;
    }

    public void setMaths(String maths) {
        this.maths = maths;
    }

    public String getScience() {
        return science;
    }

    public void setScience(String science) {
        this.science = science;
    }

    public String getBiology() {
        return biology;
    }

    public void setBiology(String biology) {
        this.biology = biology;
    }

    public String getPhysics() {
        return physics;
    }

    public void setPhysics(String physics) {
        this.physics = physics;
    }

    public String getChemistry() {
        return chemistry;
    }

    public void setChemistry(String chemistry) {
        this.chemistry = chemistry;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getGeography() {
        return geography;
    }

    public void setGeography(String geography) {
        this.geography = geography;
    }

    public String getCre() {
        return cre;
    }

    public void setCre(String cre) {
        this.cre = cre;
    }

    public String getIre() {
        return ire;
    }

    public void setIre(String ire) {
        this.ire = ire;
    }

    public String getHre() {
        return hre;
    }

    public void setHre(String hre) {
        this.hre = hre;
    }

    public String getHomescience() {
        return homescience;
    }

    public void setHomescience(String homescience) {
        this.homescience = homescience;
    }

    public String getArt() {
        return art;
    }

    public void setArt(String art) {
        this.art = art;
    }

    public String getAgriculture() {
        return agriculture;
    }

    public void setAgriculture(String agriculture) {
        this.agriculture = agriculture;
    }

    public String getCommputer() {
        return commputer;
    }

    public void setCommputer(String commputer) {
        this.commputer = commputer;
    }

    public String getAviation() {
        return aviation;
    }

    public void setAviation(String aviation) {
        this.aviation = aviation;
    }

    public String getFrench() {
        return french;
    }

    public void setFrench(String french) {
        this.french = french;
    }

    public String getGerman() {
        return german;
    }

    public void setGerman(String german) {
        this.german = german;
    }

    public String getArabic() {
        return arabic;
    }

    public void setArabic(String arabic) {
        this.arabic = arabic;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getBusinessstudies() {
        return businessstudies;
    }

    public void setBusinessstudies(String businessstudies) {
        this.businessstudies = businessstudies;
    }

    public String getSocialstudies() {
        return socialstudies;
    }

    public void setSocialstudies(String socialstudies) {
        this.socialstudies = socialstudies;
    }

    public String getSsre() {
        return ssre;
    }

    public void setSsre(String ssre) {
        this.ssre = ssre;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
