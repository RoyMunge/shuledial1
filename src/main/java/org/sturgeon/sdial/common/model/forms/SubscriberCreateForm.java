package org.sturgeon.sdial.common.model.forms;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by User on 13/05/2016.
 */
public class SubscriberCreateForm {

    private String userEmail = "";
    private String firstName = "";
    private String otherNames = "";
    @NotEmpty
    private String userPhone = "";
    @NotEmpty
    private String password = "";

    public SubscriberCreateForm(){}

    public SubscriberCreateForm(String userEmail, String firstName, String otherNames, String userPhone, String password) {
        this.userEmail = userEmail;
        this.firstName = firstName;
        this.otherNames = otherNames;
        this.userPhone = userPhone;
        this.password = password;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "SubscriberCreateForm{" +
                "userEmail='" + userEmail + '\'' +
                ", firstName='" + firstName + '\'' +
                ", otherNames='" + otherNames + '\'' +
                ", userPhone='" + userPhone + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
