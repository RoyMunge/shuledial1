package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by User on 14/03/2017.
 */

@Entity
@NamedQuery(name = "StudentGroup.findBySchoolid", query = "select s from StudentGroup s where s.schoolid = ?1")
@Table(name = "studentgroup")
public class StudentGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "studentgroupid", nullable = false, updatable = false)
    private Long studentgroupid;

    @Column(name ="name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "schoolid")
    private Long schoolid;

    @ManyToOne(optional = false, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "schoolid", referencedColumnName = "schoolid", updatable = false, insertable = false)
    private School school;

    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<Student> studentSet;

    public Long getStudentgroupid() {
        return studentgroupid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public Set<Student> getStudentSet() {
        return studentSet;
    }

    public void setStudentSet(Set<Student> studentSet) {
        this.studentSet = studentSet;
    }
}
