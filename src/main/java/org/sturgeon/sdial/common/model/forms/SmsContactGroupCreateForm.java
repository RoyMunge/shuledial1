package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 01/04/2017.
 */
public class SmsContactGroupCreateForm {
    private String name;
    private String description;
    private Long schoolid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }
}
