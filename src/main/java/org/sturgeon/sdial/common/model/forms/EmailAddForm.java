package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 13/02/2017.
 */
public class EmailAddForm {
    Long schoolid;
    String email;

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
