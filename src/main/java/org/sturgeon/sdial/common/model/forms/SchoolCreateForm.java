package org.sturgeon.sdial.common.model.forms;

import org.hibernate.validator.constraints.NotEmpty;
import org.sturgeon.sdial.common.model.SchoolType;

import javax.validation.constraints.NotNull;

/**
 * Created by User on 9/20/2015.
 */
public class SchoolCreateForm {

    @NotEmpty
    private String name = "";

    @NotEmpty
    private String shortName = "";

    private String schoolPhone = "";

    private String schoolEmail = "";

    private Long subcountyid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getSchoolPhone() {
        return schoolPhone;
    }

    public void setSchoolPhone(String schoolPhone) {
        this.schoolPhone = schoolPhone;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public void setSchoolEmail(String schoolEmail) {
        this.schoolEmail = schoolEmail;
    }

    public Long getSubcountyid() {
        return subcountyid;
    }

    public void setSubcountyid(Long subcountyid) {
        this.subcountyid = subcountyid;
    }

    @Override
    public String toString() {
        return "SchoolCreateForm{" +
                "name='" + name + '\'' +
                ", shortName='" + shortName + '\'' +
                ", schoolPhone='" + schoolPhone + '\'' +
                ", schoolEmail='" + schoolEmail + '\'' +
                '}';
    }
}
