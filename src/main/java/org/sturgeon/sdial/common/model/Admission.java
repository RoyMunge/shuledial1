package org.sturgeon.sdial.common.model;

import javax.persistence.*;

/**
 * Created by User on 23/11/2016.
 */
@Entity
@Table(name = "admission")
public class Admission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "admissionid", nullable = false, updatable = false)
    private Long admissionid;

    @Column(name = "parentname", nullable = false)
    private String parentName;

    @Column(name = "studentname", nullable = false)
    private String studentName;

    @Column(name = "phonenumber", nullable = false)
    private String phonenumber;

    @Column(name = "email", nullable = true)
    private String email;

    @Column(name = "previousschool", nullable = true)
    private String previousSchool;

    @Column(name = "schoollevel", nullable = false)
    private String schoollevel;

    @Column(name = "qualificationlevel")
    private String qualificationlevel;

    @Column(name = "qualificationlevelresult")
    private String qualificationLevelResult;

    @Column(name = "description", nullable = true)
    private String description;

    @Column(name = "schoolid", nullable = false)
    private Long schoolid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolid", referencedColumnName="schoolid", updatable = false, insertable = false)
    private School school;

    public Long getAdmissionid() {
        return admissionid;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPreviousSchool() {
        return previousSchool;
    }

    public void setPreviousSchool(String previousSchool) {
        this.previousSchool = previousSchool;
    }

    public String getSchoollevel() {
        return schoollevel;
    }

    public void setSchoollevel(String schoollevel) {
        this.schoollevel = schoollevel;
    }

    public String getQualificationlevel() {
        return qualificationlevel;
    }

    public void setQualificationlevel(String qualificationlevel) {
        this.qualificationlevel = qualificationlevel;
    }

    public String getQualificationLevelResult() {
        return qualificationLevelResult;
    }

    public void setQualificationLevelResult(String qualificationLevelResult) {
        this.qualificationLevelResult = qualificationLevelResult;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
