package org.sturgeon.sdial.common.model;

        import javax.persistence.*;
        import java.util.Set;

/**
 * Created by User on 23/01/2017.
 */
@Entity
@Table(name = "phonenumber")
public class PhoneNumber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "phonenumberid", nullable = false, updatable = false)
    private Long phonenumberid;

    @Column(name = "phonenumber")
    private String phoneNumber;

    @Column(name = "schoolid")
    private Long schoolid;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }
}
