package org.sturgeon.sdial.common.model.forms;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by User on 9/22/2015.
 */
public class StudentCreateFormApp {

    @NotEmpty
    private String subscriberPhoneNumber="";
    @NotEmpty
    private String admission="";
    @NotEmpty
    private String schoolShortName="";

    public StudentCreateFormApp(){}

    public StudentCreateFormApp(String subscriberPhoneNumber, String admission, String schoolShortName) {
        this.subscriberPhoneNumber = subscriberPhoneNumber;
        this.admission = admission;
        this.schoolShortName = schoolShortName;
    }

    public String getSubscriberPhoneNumber() {
        return subscriberPhoneNumber;
    }

    public void setSubscriberPhoneNumber(String subscriberPhoneNumber) {
        this.subscriberPhoneNumber = subscriberPhoneNumber;
    }

    public String getAdmission() {
        return admission;
    }

    public void setAdmission(String admission) {
        this.admission = admission;
    }

    public String getSchoolShortName() {
        return schoolShortName;
    }

    public void setSchoolShortName(String schoolShortName) {
        this.schoolShortName = schoolShortName;
    }

    @Override
    public String toString() {
        return "StudentCreateFormApp{" +
                "subscriberPhoneNumber='" + subscriberPhoneNumber + '\'' +
                ", admission='" + admission + '\'' +
                ", schoolShortName='" + schoolShortName + '\'' +
                '}';
    }
}