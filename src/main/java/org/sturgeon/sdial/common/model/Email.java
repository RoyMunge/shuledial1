package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by User on 23/01/2017.
 */
@Entity
@Table(name = "email")
public class Email {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "emailid", nullable = false, updatable = false)
    private Long emailid;

    @Column(name = "email")
    private String email;

    @Column(name = "schoolid")
    private Long schoolid;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }
}
