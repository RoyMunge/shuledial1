package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.*;

@Entity
@NamedQueries({
        @NamedQuery(name = "Subscriber.findByPhoneNumber", query = "select e from Subscriber e where e.phoneNumber = ?1"),
        @NamedQuery(name = "Subscriber.findByPhoneNumberAndPin", query = "select e from Subscriber e where e.phoneNumber = ?1 and e.passwordHash = ?2"),
        @NamedQuery(name ="Subscriber.updateDeviceToken", query = "update Subscriber s set s.deviceToken = ?2 where s.phoneNumber = ?1")
})
@Table(name = "subscriber")
public class Subscriber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subscriberid", nullable = false, updatable = false)
    private Long subscriberid;

    @Column(name = "phone_number", nullable = false, unique = true)
    private String phoneNumber;

    @Column(name = "email", nullable = true)
    private String email;

    @Column(name = "password_hash", nullable = true)
    private String passwordHash;

    @Column(name = "name", nullable = true)
    private String name;

    @Column(name = "validuntil")
    private Date validUntil;

    @Column(name = "devicetoken")
    private String deviceToken;

    public Long getSubscriberid() {
        return subscriberid;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    @Override
    public String toString() {
        return "Subscriber{" +
                "subscriberid=" + subscriberid +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                '}';
    }

    /**
     * Created by User on 18/11/2016.
     */
    public static class CustomFeeItem {
    }
}