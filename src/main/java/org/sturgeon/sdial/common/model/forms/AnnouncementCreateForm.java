package org.sturgeon.sdial.common.model.forms;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

;import java.util.ArrayList;

/**
 * Created by User on 9/22/2015.
 */
public class AnnouncementCreateForm {

    private boolean sendAsSMS = false;

    @NotEmpty
    private String announcementName;

    @NotEmpty
    private String description;

    @NotNull
    private Long schoolid;

    private ArrayList<String> recipients;

    public boolean isSendAsSMS() {
        return sendAsSMS;
    }

    public void setSendAsSMS(boolean sendAsSMS) {
        this.sendAsSMS = sendAsSMS;
    }

    public String getAnnouncementName() {
        return announcementName;
    }

    public void setAnnouncementName(String announcementName) {
        this.announcementName = announcementName;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public ArrayList<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(ArrayList<String> recipients) {
        this.recipients = recipients;
    }

    @Override
    public String toString() {
        return "AnnouncementCreateForm{" +
                "announcementName='" + announcementName + '\'' +
                ", description='" + description + '\'' +
                ", schoolid=" + schoolid +
                '}';
    }
}
