package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Roy on 5/18/2017.
 */

@Entity
@NamedQueries({
        @NamedQuery(name = "EventStudentGroup.findByStudentgroupid", query = "select a from EventStudentGroup a where a.studentgroupid = ?1"),
        @NamedQuery(name = "EventStudentGroup.findBySchoolid", query = "select a from EventStudentGroup a where a.schoolid= ?1"),
        @NamedQuery(name = "EventStudentGroup.findByStudent", query = "select a.eventid from EventStudentGroup a where a.schoolid= ?1 and a.studentgroupid IN ?2 or a.studentgroupid IS NULL group by a.eventid"),
        @NamedQuery(name = "EventStudentGroup.findUpcomingByStudent", query = "select a.eventid from EventStudentGroup a where a.schoolid= ?1 and a.eventDate >= CURDATE() and a.studentgroupid IN  ?2  or a.studentgroupid IS NULL group by a.eventid")

})
@Table(name = "event_studentgroup")
public class EventStudentGroup  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "eventstudentgroupid", nullable= false, updatable = false)
    private Long eventstudentgroupid;

    @Column(name = "eventid", nullable = false)
    private Long eventid;

    @Column(name = "studentgroupid")
    private Long studentgroupid;

    @Column(name = "schoolid", nullable = false)
    private Long schoolid;

    @Column(name = "eventdate", nullable = false)
    private Date eventDate;

    @ManyToOne(optional = false, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "eventid", referencedColumnName = "eventid", updatable = false, insertable = false)
    private Event event;

    public Long getEventstudentgroupid() {
        return eventstudentgroupid;
    }

    public Long getEventid() {
        return eventid;
    }

    public void setEventid(Long eventid) {
        this.eventid = eventid;
    }

    public Long getStudentgroupid() {
        return studentgroupid;
    }

    public void setStudentgroupid(Long studentgroupid) {
        this.studentgroupid = studentgroupid;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
