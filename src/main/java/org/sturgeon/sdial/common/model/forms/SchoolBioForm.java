package org.sturgeon.sdial.common.model.forms;

/**
 * Created by User on 23/11/2016.
 */
public class SchoolBioForm {
    private Long schoolid;
    private String website;
    private String motto;
    private String physicalAddress;
    private String postalAddress;
    private String bio;
    private String admissionInfo;
    private String feesPaymentInfo;
    private String overview;

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getAdmissionInfo() {
        return admissionInfo;
    }

    public void setAdmissionInfo(String admissionInfo) {
        this.admissionInfo = admissionInfo;
    }

    public String getFeesPaymentInfo() {
        return feesPaymentInfo;
    }

    public void setFeesPaymentInfo(String feesPaymentInfo) {
        this.feesPaymentInfo = feesPaymentInfo;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }
}
