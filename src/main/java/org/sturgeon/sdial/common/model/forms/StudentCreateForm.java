package org.sturgeon.sdial.common.model.forms;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by User on 9/22/2015.
 */
public class StudentCreateForm {

    @NotEmpty
    private String admission;

    @NotEmpty
    private String studentName;

    private String studentgroupid;

    private String studentGroupName;

    private Long schoolid;

    public String getAdmission() {
        return admission;
    }

    public void setAdmission(String admission) {
        this.admission = admission;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public String getStudentgroupid() {
        return studentgroupid;
    }

    public void setStudentgroupid(String studentgroupid) {
        this.studentgroupid = studentgroupid;
    }

    public String getStudentGroupName() {
        return studentGroupName;
    }

    public void setStudentGroupName(String studentGroupName) {
        this.studentGroupName = studentGroupName;
    }
}
