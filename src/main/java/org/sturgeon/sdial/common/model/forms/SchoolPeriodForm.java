package org.sturgeon.sdial.common.model.forms;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by User on 18/11/2016.
 */
public class SchoolPeriodForm {
    @NotEmpty
    private String schoolPeriodName;
    @NotEmpty
    private String startDate;
    @NotEmpty
    private String endDate;
    @NotNull
    private Long schoolid;

    public String getSchoolPeriodName() {
        return schoolPeriodName;
    }

    public void setSchoolPeriodName(String schoolPeriodName) {
        this.schoolPeriodName = schoolPeriodName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }
}
