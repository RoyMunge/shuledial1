package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by User on 01/04/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "SmsContactGroup.findBySchoolid", query = "select s from SmsContactGroup s where s.schoolid = ?1"),
        @NamedQuery(name = "SmsContactGroup.CountSmsContactGroup", query = "select count(*) from SmsContactGroup s where s.schoolid = ?1")
})
@Table(name = "smscontactgroup")
public class SmsContactGroup {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "smscontactgroupid", nullable = false, updatable = false)
        private Long smscontactgroupid;

        @Column(name ="name", nullable = false)
        private String name;

        @Column(name = "description")
        private String description;

        @Column(name = "schoolid")
        private Long schoolid;

        @ManyToOne(optional = false, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
        @JoinColumn(name = "schoolid", referencedColumnName = "schoolid", updatable = false, insertable = false)
        private School school;

        @ManyToMany(mappedBy = "smsContactGroupSet")
        private Set<SmsContact> smsContactSet;

        public Long getSmscontactgroupid() {
                return smscontactgroupid;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getDescription() {
                return description;
        }

        public void setDescription(String description) {
                this.description = description;
        }

        public Long getSchoolid() {
                return schoolid;
        }

        public void setSchoolid(Long schoolid) {
                this.schoolid = schoolid;
        }

        public School getSchool() {
                return school;
        }

        public void setSchool(School school) {
                this.school = school;
        }

        public Set<SmsContact> getSmsContactSet() {
                return smsContactSet;
        }

        public void setSmsContactSet(Set<SmsContact> smsContactSet) {
                this.smsContactSet = smsContactSet;
        }
}
