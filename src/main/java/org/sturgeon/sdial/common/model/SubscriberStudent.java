package org.sturgeon.sdial.common.model;

import javax.persistence.*;

/**
 * Created by User on 31/03/2017.
 */
@Entity
@NamedQueries ({
        @NamedQuery(name = "SubscriberStudent.CountSubscriber", query = "select count(*) from SubscriberStudent s WHERE s.schoolid = ?1"),
        @NamedQuery(name = "SubscriberStudent.findBySubscriberid", query = "select s from SubscriberStudent s where s.subscriberid = ?1"),
        @NamedQuery(name = "SubscriberStudent.findByStudentid", query = "select s from SubscriberStudent s where s.studentid = ?1"),
        @NamedQuery(name = "SubscriberStudent.findPrimaryByStudentid", query = "select s from SubscriberStudent s where s.studentid = ?1 and primaryContact = 'true'"),
        @NamedQuery(name = "SubscriberStudent.findByIds", query = "select s from SubscriberStudent s where s.subscriberid = ?1 and s.studentid = ?2")
})
@Table(name = "subscriber_student")
public class SubscriberStudent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subscriberstudentid", nullable = false, updatable = false)
    private Long subscriberstudentid;

    @Column(name = "subscriberid", nullable = false, updatable = false)
    private Long subscriberid;

    @Column(name = "studentid", nullable = false, updatable = false)
    private Long studentid;

    @Column(name = "schoolid", nullable = false, updatable = false)
    private Long schoolid;

    @Column(name = "primarycontact")
    private Boolean primaryContact;

    @ManyToOne(optional = false, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "studentid", referencedColumnName = "studentid", updatable = false, insertable = false)
    private Student student;

    @ManyToOne(optional = false, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "subscriberid", referencedColumnName = "subscriberid", updatable = false, insertable = false)
    private Subscriber subscriber;


    public Long getSubscriberstudentid() {
        return subscriberstudentid;
    }

    public Long getSubscriberid() {
        return subscriberid;
    }

    public void setSubscriberid(Long subscriberid) {
        this.subscriberid = subscriberid;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public Boolean getPrimaryContact() {
        return primaryContact;
    }

    public void setPrimaryContact(Boolean primaryContact) {
        this.primaryContact = primaryContact;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Subscriber subscriber) {
        this.subscriber = subscriber;
    }
}
