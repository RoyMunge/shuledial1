package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by User on 10/04/2017.
 */

@Entity
@Table(name = "smsmessages")
public class SmsMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "smsmessageid", nullable = false, updatable = false)
    private Long smsmessageid;

    @Column(name = "messagetype", nullable = false)
    private String messageType;

    @Column(name = "text")
    private String text;

    @Column(name = "recipient")
    private String recipient;

    @Column(name = "recipientgroupid")
    private Long recipientgroupid;

    @Column(name = "date")
    private Date date;

    @Column(name = "status")
    private String status;

    public Long getSmsmessageid() {
        return smsmessageid;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public Long getRecipientgroupid() {
        return recipientgroupid;
    }

    public void setRecipientgroupid(Long recipientgroupid) {
        this.recipientgroupid = recipientgroupid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
