package org.sturgeon.sdial.common.model.forms;

import java.util.ArrayList;

/**
 * Created by Roy on 5/23/2017.
 */
public class PushNotificationForm {
    private String title;
    private String message;
    private Long schoolid;
    private ArrayList<String> recipients;
    private Boolean isSendtoAll;

    public PushNotificationForm(String title, String message, Long schoolid, ArrayList<String> recipients, Boolean isSendtoAll) {
        this.title = title;
        this.message = message;
        this.schoolid = schoolid;
        this.recipients = recipients;
        this.isSendtoAll = isSendtoAll;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public ArrayList<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(ArrayList<String> recpients) {
        this.recipients = recpients;
    }

    public Boolean getSendtoAll() {
        return isSendtoAll;
    }

    public void setSendtoAll(Boolean sendtoAll) {
        isSendtoAll = sendtoAll;
    }
}
