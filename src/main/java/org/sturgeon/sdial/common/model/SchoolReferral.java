package org.sturgeon.sdial.common.model;

import javax.persistence.*;

/**
 * Created by Roy on 5/29/2017.
 */
@Entity
@Table(name = "schoolreferral")
public class SchoolReferral {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schoolreferralid", updatable = false, nullable = false)
    private Long schoolreferralid;

    @Column(name = "schoolname")
    private String schoolName;

    @Column(name = "schoollocation")
    private String schoolLocation;

    @Column(name = "contactperson")
    private String contactPerson;

    @Column(name = "phonenumber")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    public Long getSchoolreferralid() {
        return schoolreferralid;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolLocation() {
        return schoolLocation;
    }

    public void setSchoolLocation(String schoolLocation) {
        this.schoolLocation = schoolLocation;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
