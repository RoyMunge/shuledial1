package org.sturgeon.sdial.common.model;

import javax.persistence.*;

/**
 * Created by User on 23/11/2016.
 */
@Entity
@NamedQuery(name = "SchoolBio.Update", query = "UPDATE SchoolBio s SET s.physicalAddress=?2, s.postalAddress=?3, s.bio=?4, s.vacancy=?5 WHERE s.schoolid=?1")

@Table(name = "schoolbio")
public class SchoolBio {

    @Id
    @Column(name = "schoolid", nullable = false, updatable = false)
    private Long schoolid;

    @OneToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "schoolid", referencedColumnName = "schoolid", updatable = false, insertable = false)
    private School school;

    @Column(name = "physicaladdress")
    private String physicalAddress;

    @Column(name = "postaladdress")
    private String postalAddress;

    @Column(name = "bio")
    private String bio;

    @Column(name = "vacancy")
    private String vacancy;

    @Column(name = "feespaymentinfo")
    private String feesPaymentinfo;


    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getVacancy() {
        return vacancy;
    }

    public void setVacancy(String vacancy) {
        this.vacancy = vacancy;
    }

    public String getFeesPaymentinfo() {
        return feesPaymentinfo;
    }

    public void setFeesPaymentinfo(String feesPaymentinfo) {
        this.feesPaymentinfo = feesPaymentinfo;
    }
}
