package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Roy on 5/9/2017.
 */

@Entity
@Table(name = "subscriptionrequest")
public class SubscriptionRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subscriptionrequestid", nullable = false, updatable = false)
    private Long subscriptionrequestid;

    @Column(name = "phonenumber")
    private String phoneNumber;

    @Column(name = "studentid")
    private Long studentid;

    @Column (name = "schoolid")
    private Long schoolid;

    @Column(name ="date")
    private Date date;

    public Long getSubscriptionrequestid() {
        return subscriptionrequestid;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
