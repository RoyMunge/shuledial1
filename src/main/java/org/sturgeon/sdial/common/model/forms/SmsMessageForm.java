package org.sturgeon.sdial.common.model.forms;

import java.util.ArrayList;

/**
 * Created by User on 10/04/2017.
 */
public class SmsMessageForm {

    private String text;

    private ArrayList<String> recipients;

    private String otherRecipients;

    private Long schoolid;

    private  Boolean sendtoAll = false;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ArrayList<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(ArrayList<String> recipients) {
        this.recipients = recipients;
    }

    public String getOtherRecipients() {
        return otherRecipients;
    }

    public void setOtherRecipients(String otherRecipients) {
        this.otherRecipients = otherRecipients;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public Boolean getSendtoAll() {
        return sendtoAll;
    }

    public void setSendtoAll(Boolean sendtoAll) {
        this.sendtoAll = sendtoAll;
    }
}
