package org.sturgeon.sdial.common.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by User on 18/04/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "findByStudent", query = "select dm from DirectMessage dm where dm.studentid = ?1 and deleteStatus = 'ACTIVE' order by date desc"),
        @NamedQuery(name = "findBySchool", query = "select dm from DirectMessage dm where dm.schoolid = ?1 and deleteStatus = 'ACTIVE' order by date desc"),
        @NamedQuery(name = "findBySubscriber", query = "select dm from DirectMessage dm where dm.subscriberid = ?1 and deleteStatus = 'ACTIVE' order by date desc"),
        @NamedQuery(name = "delete", query = "update DirectMessage dm set dm.deleteStatus = 'DELETED' where dm.directmessageid = ?1"),
        @NamedQuery(name = "updateStatus", query = "update DirectMessage dm set dm.status = ?2 where dm.directmessageid = ?1")
})
@Table(name = "directmessage")
public class DirectMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "directmessageid", nullable = false, updatable = false)
    private Long directmessageid;

    @Column(name = "schoolid")
    private Long schoolid;

    @Column(name = "studentid")
    private Long studentid;

    @Column(name = "subscriberid")
    private Long subscriberid;

    @Column(name = "date")
    private Date date;

    @Column(name = "direction")
    private String direction;

    @Column(name = "text")
    private String text;

    @Column(name = "status")
    private String status;

    @Column(name = "deletestatus")
    private String deleteStatus;

    @ManyToOne(optional = false, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "schoolid", referencedColumnName = "schoolid", updatable = false, insertable = false)
    private School school;

    @ManyToOne(optional = false, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "studentid", referencedColumnName = "studentid", updatable = false, insertable = false)
    private Student student;

    @ManyToOne(optional = false, cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "subscriberid", referencedColumnName = "subscriberid", updatable = false, insertable = false)
    private Subscriber subscriber;


    public Long getDirectmessageid() {
        return directmessageid;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public Long getSubscriberid() {
        return subscriberid;
    }

    public void setSubscriberid(Long subscriberid) {
        this.subscriberid = subscriberid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(String deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public School getSchool() {
        return school;
    }
    public Student getStudent() {
        return student;
    }

    public Subscriber getSubscriber() {
        return subscriber;
    }
}
