package org.sturgeon.sdial.common.model;

import javax.persistence.*;

/**
 * Created by User on 19/11/2016.
 */
@Entity
@NamedQuery(name = "FeeItem.CountFeeItem", query = "select count(*) from FeeItem e where e.schoolid = ?1")
@Table(name = "feeitem")
public class FeeItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "feeitemid", nullable = false, updatable = false)
    private Long feeitemid;

    @Column(name = "feeitemname", nullable = false)
    private String feeItemName;

    @Column (name = "schoolid", nullable = false)
    private Long schoolid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolid", referencedColumnName="schoolid", updatable = false, insertable = false)
    private School school;

    public Long getFeeitemid() {
        return feeitemid;
    }

    public String getFeeItemName() {
        return feeItemName;
    }

    public void setFeeItemName(String feeItemName) {
        this.feeItemName = feeItemName;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
