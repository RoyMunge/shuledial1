package org.sturgeon.sdial.common.model;

import javax.persistence.*;

/**
 * Created by User on 18/11/2016.
 */

@Entity
@NamedQuery(name = "SchoolLevel.CountSchoolLevel", query = "select count(*) from SchoolLevel e where e.schoolid = ?1")
@Table(name = "schoollevel")
public class SchoolLevel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schoollevelid", nullable = false, updatable = false)
    private Long schoollevelid;

    @Column(name = "schoollevelname", nullable = false)
    private String schoolLevelName;

    @Column(name = "schoolid", nullable = false)
    private Long schoolid;

    @ManyToOne(optional=false, fetch = FetchType.EAGER)
    @JoinColumn(name="schoolid", referencedColumnName="schoolid", updatable = false, insertable = false)
    private School school;

    public Long getSchoollevelid() {
        return schoollevelid;
    }

    public String getSchoolLevelName() {
        return schoolLevelName;
    }

    public void setSchoolLevelName(String schoolLevelName) {
        this.schoolLevelName = schoolLevelName;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
