INSERT INTO schooltype(schooltypeid,schooltypename)
VALUES (1,'SECONDARY');

INSERT INTO schooltype(schooltypeid,schooltypename)
VALUES (2,'PRIMARY');

INSERT INTO schooltype(schooltypeid,schooltypename)
VALUES (3,'E.C.D.E');

INSERT INTO schooltype(schooltypeid,schooltypename)
VALUES (4,'DAY');

INSERT INTO schooltype(schooltypeid,schooltypename)
VALUES (5,'BOARDING');

INSERT INTO schooltype(schooltypeid,schooltypename)
VALUES (6,'BOYS');

INSERT INTO schooltype(schooltypeid,schooltypename)
VALUES (7,'GIRLS');

INSERT INTO qualificationlevel (qualificationlevelid,qualificationlevelname)
VALUES (1,'BEGINNER');

INSERT INTO qualificationlevel (qualificationlevelid,qualificationlevelname)
VALUES (2,'PRE SCHOOL');

INSERT INTO qualificationlevel (qualificationlevelid,qualificationlevelname)
VALUES (3,'KCPE');

INSERT INTO qualificationlevel (qualificationlevelid,qualificationlevelname)
VALUES (4,'KCSE');

INSERT INTO qualificationlevel (qualificationlevelid,qualificationlevelname)
VALUES (5,'GCPE');

INSERT INTO qualificationlevel (qualificationlevelid,qualificationlevelname)
VALUES (6,'GSCE');

INSERT INTO county (countyid, countyname)
VALUES (1, 'NAIROBI');

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (1, 'STAREHE', 1);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (2, 'KAMUKUNJI', 1);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (3, 'KASARANI', 1);

INSERT INTO county (countyid, countyname)
VALUES (2, 'MOMBASA');

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (4, 'MVITA', 2);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (5, 'KISAUNI', 2);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (6, 'CHANGAMWE', 2);

INSERT INTO county (countyid, countyname)
VALUES (3, 'KISUMU');

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (7, 'KISUMU EAST', 3);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (8, 'KISUMU WEST', 3);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (9, 'KISUMU NORTH', 3);

INSERT INTO county (countyid, countyname)
VALUES (4, 'NAKURU');

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (10, 'NAIVASHA', 4);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (11, 'GILGIL', 4);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (12, 'NAKURU', 4);

INSERT INTO county (countyid, countyname)
VALUES (5, 'NYERI');

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (13, 'MUKURUWEINI', 5);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (14, 'NYERI SOUTH', 5);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (15, 'MATHIRA', 5);

INSERT INTO county (countyid, countyname)
VALUES (6, 'MERU');

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (16, 'IGEMBE', 6);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (17, 'TIGANIA', 6);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (18, 'IMENTI', 6);

INSERT INTO county (countyid, countyname)
VALUES (7, 'MACHAKOS');

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (19, 'MAKUENI', 7);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (20, 'KILUNGU', 7);

INSERT INTO subcounty(subcountyid, subcountyname, countyid)
VALUES (21, 'KIBWEZI', 7);



INSERT INTO school (schoolid,name, shortname, subcountyid)
VALUES (2315648951 ,'ELIMU GIRLS HIGH SCHOOL', 'EGHS', 12);

INSERT INTO portal_user (email, password_hash, firstname, othernames,phone, schoolid)
VALUES ('carole@gmail.com', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'Carole', 'Waithira', '254722112445', 2315648951);

INSERT INTO schoollevel (schoollevelid, schoolLevelName, schoolid)
VALUES (1, 'Form 1',2315648951);

INSERT INTO schoollevel (schoollevelid, schoolLevelName, schoolid)
VALUES (2, 'Form 2',2315648951);

INSERT INTO schoollevel (schoollevelid, schoolLevelName, schoolid)
VALUES (3, 'Form 3',2315648951);

INSERT INTO schoollevel (schoollevelid, schoolLevelName, schoolid)
VALUES (4, 'Form 4',2315648951);

INSERT INTO schoolperiod (schoolperiodid, schoolPeriodName, startDate, endDate, schoolid)
VALUES (1, 'Term 1 2017', '2017-01-01 00:00:00','2017-04-30 00:00:00',2315648951);

INSERT INTO schoolperiod (schoolperiodid, schoolPeriodName, startDate, endDate, schoolid)
VALUES (2, 'Term 2 2017', '2017-05-01 00:00:00','2017-08-31 00:00:00',2315648951);

INSERT INTO schoolperiod (schoolperiodid, schoolPeriodName, startDate, endDate, schoolid)
VALUES (3, 'Term 3 2017', '2017-09-01 00:00:00','2017-12-31 00:00:00',2315648951);

INSERT INTO feeitem (feeitemid, feeItemName, schoolid)
VALUES (1,'Tuition',2315648951);

INSERT INTO feeitem (feeitemid, feeItemName, schoolid)
VALUES (2,'Boarding',2315648951);

INSERT INTO feeitemamount (feeitemid, schoolperiodid, schoollevelid, amount)
VALUES (1,1,1,500);

INSERT INTO feeitemamount (feeitemid, schoolperiodid, schoollevelid, amount)
VALUES (1,2,1,1500);

INSERT INTO feeitemamount (feeitemid, schoolperiodid, schoollevelid, amount)
VALUES (1,3,1,6000);

INSERT INTO feeitemamount (feeitemid, schoolperiodid, schoollevelid, amount)
VALUES (2,1,1,3000);

INSERT INTO feeitemamount (feeitemid, schoolperiodid, schoollevelid, amount)
VALUES (2,2,1,2000);

INSERT INTO feeitemamount (feeitemid, schoolperiodid, schoollevelid, amount)
VALUES (2,3,1,5000);

INSERT INTO schoollevelperiodtotal (schoolperiodid, schoollevelid, amount)
VALUES (1,1,10000);

INSERT INTO schoollevelperiodtotal (schoolperiodid, schoollevelid, amount)
VALUES (2,1,12000);

INSERT INTO schoollevelperiodtotal (schoolperiodid, schoollevelid, amount)
VALUES (3,1,11000);

INSERT INTO subscriber (phone_number, devicetoken, name)
VALUES ('254712624369', 'f01CJy9BvGg:APA91bEWcq4TwSyb0vyDYirT_dnCEM0ZUiH4CGQ-uEivPIpxA9I1eKzWsxLUuySZgGXfJqFperpHih9sWnksoWIZWaWAYqj6CCIJRnSmkgAlsw4OEKKbW2NT8aqrVtWvdRCvn8dTFkE4', 'Caroline Njama');


